/**
	Nanyang Polytechnic 
	School of Interactive & Digital Media
	DM2241 Multiplayer Games Programming
	For Educational Purpose only
	Do not distribute or copy without permission from NYP
**/

#ifndef CHATBASE_H_
#define CHATBASE_H_

class ChatBase
{
public:
	virtual bool Tick() = 0;
	virtual ~ChatBase() {}
};

#endif