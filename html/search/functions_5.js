var searchData=
[
  ['init',['Init',['../class_c_audio.html#a5496e097b92cf6255b8c8074a6e68808',1,'CAudio::Init()'],['../class_c_map_editor.html#a6b2b640f92e5b7b6a421c0079ac5663c',1,'CMapEditor::Init()'],['../class_c_player_info.html#a8e545c95e46997196d1c10bd2a42c0cd',1,'CPlayerInfo::Init()'],['../class_c_timer.html#a82d078e2b14c26db127357945498409c',1,'CTimer::Init()']]],
  ['inttostring',['intToString',['../classmainmenu.html#a976c0ab6f924c83611f9a2d730c99a26',1,'mainmenu']]],
  ['invulnerability',['Invulnerability',['../class_c_player_info.html#a6f1dfca4651e696cced1ebf6eb88639c',1,'CPlayerInfo']]],
  ['isfreefall',['isFreeFall',['../class_c_player_info.html#a978f5af2f5a0e6f66c12c9314f1bac53',1,'CPlayerInfo']]],
  ['isjumpupwards',['isJumpUpwards',['../class_c_player_info.html#a2473e917149ba609a188c637dddf0d73',1,'CPlayerInfo']]],
  ['isonground',['isOnGround',['../class_c_player_info.html#a990ea014a7d18c162270994988e8e0c0',1,'CPlayerInfo']]]
];
