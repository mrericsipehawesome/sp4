/**
 * @file
 * @author Ryan Tay
 * @version 1.4
 */

#pragma once
#include <time.h>
#include <iostream>

using namespace std;

/**
 * The Timer class allows a timer to be stopped, started, user could set how long the timer lasts and how long.
 */
class CTimer
{
private:
	time_t timestart;
	int m_iCurrentMin, m_iCurrentSec, m_iSec;
	bool m_bTimerStopped;
public:

	/**
	 * Constructor
	 */
	CTimer(void);

	/**
	 * Destructor
	 */
	~CTimer(void);

	/*
	 * Function that starts the timer
	 */
	void StartTimer (void);
	/*
	 * Function that starts the timer for the player
	 */
	void StartInvulTimer (void);

	/*
	 * Function that stops the timer
	 */
	void StopTimer (void);

	/**
	 * Function that gets the value of m_iCurrentMin
	 * @return Returns the value of m_iCurrentMin
	 */
	int GetMin (void);

	/**
	 * Function that gets the value of m_iCurrentSec
	 * @return Returns the value of m_iCurrentSec
	 */
	int GetSec (void);

	/**
	 * Function that sets the value of m_iCurrentMin
	 * @param Min Sets the value of m_iCurrentMin
	 */
	void SetMin (int Min);

	/**
	 * Function that sets the value of m_iCurrentSec
	 * @param Sec Sets the value of m_iCurrentSec
	 */
	void SetSec (int Sec);

	/**
	 * Function that sets the value of m_iCurrentSec
	 * @return Returns true when the timer hits 0 Min and 0 Sec
	 */
	bool Update (void);

	/**
	 * Function that Initiali the value of m_iCurrentSec
	 * @param Min Sets the value of m_iCurrentMin
	 * @param Sec Sets the value of m_iCurrentSec
	 */
	void Init (int Min, int Sec);
};