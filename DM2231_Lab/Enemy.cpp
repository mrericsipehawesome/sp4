#include "Enemy.h"

Enemies::Enemies(float locx_, float locy_, int id, int MobType, int direction, FINITESTATE state)
: curState(state)
, mapOffset_x(0)
, MovingAnimation(0)
, TexCoordX(0.25)
, CounterX(0.5)
, Tile_SizeX(110)
, Tile_SizeY(100)
, speedCounter(0)
, speed(0)
, rest(0)
, health(5)
, hit(0)
, Hit(false)
, Dead(false)
, rage(false)
, moveX(0)
, moveY(0)
, roam(false)
, attack(false)
, TimerStart(false)
, Fire(false)
, MobCounter(7)
, texturetype(0)
, jumpHeight(80.0f)
{
	timer.StartTimer();
	timer.SetSec(10);
	timer.SetMin(0);
	srand(time(NULL));
	LoadTGA( &(EnemyTexture[1]), "images/ghost.tga");
	LoadTGA( &(EnemyTexture[2]), "images/pantherrest.tga");
	LoadTGA( &(EnemyTexture[3]), "images/skull.tga");
	LoadTGA( &(EnemyTexture[4]), "images/bat.tga");
	LoadTGA( &(EnemyTexture[5]), "images/fireball.tga");
	LoadTGA( &(EnemyTexture[6]), "images/panther.tga");
	LoadTGA( &(EnemyTexture[7]), "images/batrest.tga");
	LoadTGA( &(EnemyTexture[8]), "images/watermonster.tga");
	this->direction = direction;
	texturetype = id;
	type = MobType;
	this->id = id;
	Enemy_x = locx_;
	Enemy_y = locy_;
	fireballX = -50;
	fireballY = 90;
	render = true;
	EnemyAnimationCounter = 0;
	EnemyAnimationInvert = false;
}

Enemies::~Enemies(void)
{
}

void Enemies::ConstrainEnemy( const int leftBorder, const int rightBorder,
							 const int topBorder, const int bottomBorder,
							 float timeDiff,
							 int& mapOffset_x, int& mapOffset_y)
{
	if (Enemy_x < leftBorder)
	{
		Enemy_x = leftBorder;
	}
	else if (Enemy_x > rightBorder)
	{
		Enemy_x = rightBorder;
	}
	if (Enemy_y < topBorder)
		Enemy_y = topBorder;
	else if (Enemy_y > bottomBorder)
		Enemy_y = bottomBorder;
}

void Enemies::Update(CPlayerInfo* player, int id, long timelastcall, int mapOffset_x, int player1, int player2)
{
	if (!TimerStart)
	{
		TimerStart = true;
		timer.Init(0, 10);
	}

	switch(type)
	{
	case GHOST:
		//Weakest mob
		{			
			if (curState == IDLE && texturetype == 8)
			{
				jumpHeight -= 3;
				Enemy_y -= jumpHeight/3.5;
				if (jumpHeight <= -40)
					curState = ATTACK;
			}
			//Decide whether to change state
			//Move towards the right if coming from left
			else if (curState == ATTACK)
			{
				jumpHeight = 80.0f;
				if (direction == 2)
				{
					EnemyAnimationInvert = true;
					Enemy_x += 2;
				}
				//Move towards the left if coming from right
				else if (direction == 1)
				{
					EnemyAnimationInvert = false;
					Enemy_x -= 2;
				}
			}
		}
		break;
	case PANTHER:
		{
			//Check which player is nearer and go for the nearer player
			if (curState == IDLE)
			{
				texturetype = 2;
				if (player1 < 20000.0f || player2 < 20000.0f)
				{
					texturetype = 6;
					curState = ATTACK;
				}
			}

			if (Enemy_x > 900)
			{
				EnemyAnimationCounter = 0;
				curState = IDLE;
				texturetype = 2;
			}

			if(curState == ATTACK)
			{
				texturetype = 6;
				//if player 1 is nearer
				if (player1 < player2)
				{
					if (id == 1 && player->GetPos_x() + mapOffset_x < Enemy_x)
					{
						if (Enemy_x != player->GetPos_x())
						{
							EnemyAnimationInvert = false;
							Enemy_x -= 3;
						}
					}
					//if player 1 is nearer
					else if (id == 1 && player->GetPos_x() + mapOffset_x > Enemy_x)
					{
						if (Enemy_x != player->GetPos_x())
						{
							EnemyAnimationInvert = true;
							Enemy_x += 3;
						}
					}
				}
				//If player 2 is nearer
				else if (player2 < player1)
				{
					if (id == 2 && player->GetPos_x() + mapOffset_x < Enemy_x)
					{
						if (Enemy_x != player->GetPos_x())
						{
							EnemyAnimationInvert = false;
							Enemy_x -= 2;
						}
					}
					//If player 2 is nearer
					else if (id == 2 && player->GetPos_x() + mapOffset_x > Enemy_x)
					{
						if (Enemy_x != player->GetPos_x())
						{
							EnemyAnimationInvert = true;
							Enemy_x += 2;
						}
					}
				}
			}
		}
		break;
	case BOSSa: 
		{
			//Attack state 
			if (mapOffset_x == 800)
			{
				if(curState == ATTACK) 
				{ 
					//First boss, start moving off at slow speed, speed increases over time 
					if (direction == 2)
					{ 
						int result = 0;
						//Generate rand number
						result = rand() %100 + 1;
						if(result == 5)
						{
							direction = 1;
						}
						EnemyAnimationInvert = true; 
						speedCounter++;
						if(speedCounter == 10)
						{
							if(rage != true)
							{
								speed += 2;
							}
							else
								speed += 4;
							speedCounter = 0;
						}
						Enemy_x += speed; 
						if(Enemy_x >= 1544)
						{

							curState = REST; 
							speed = 0; 
							direction = 1; 
						} 
					} 
					else if (direction == 1)
					{ 
						int result = 0;
						//Generate rand number
						result = rand() %100 + 1;
						if(result == 5)
						{
							direction = 2;
						}

						EnemyAnimationInvert = false; 
						speedCounter++; 
						if(speedCounter == 10) 
						{
							if(rage != true)
							{
								speed += 2; 
							}
							else 
								speed += 4;
							speedCounter = 0;
						} 

						Enemy_x -= speed;
						if(Enemy_x <= 800) 
						{ 

							curState = REST;
							speed = 0; 
							direction = 2; 
						} 
					} 
				}

				if(curState == REST)
				{
					//speed = 0; 
					rest++; 
					if(rest == 50)
					{ 
						rest = 0;
						curState = ATTACK;
					} 
				} 
				//Rage mode
				if(health <= 3)
				{
					rage = true;
				}
			}
		}
		break;
	case BOSSb:
		{
			if (curState == IDLE)
			{
				texturetype = 7;
				if (mapOffset_x == 800)
				{
					curState = ROAM;
					texturetype = 4;
				}
			}
			
			if(curState == ROAM)
			{
				//For random movement
				if (roam == false)
				{
					roam = true;
					int maxX, minX, maxY, minY;
					maxX = 1500;
					minX = 750;
					maxY = 470;
					minY = 200;
					//Generate rand number
					moveX = rand()%(maxX-minX+1) + minX;
					moveY = rand()%(maxY-minY+1) + minY;

					Destination.x = moveX - Enemy_x;
					Destination.y = moveY - Enemy_y;
					Destination.normalizeVector3D();
				}

				Enemy_x += Destination.x * 3;
				Enemy_y += Destination.y * 3;

				if (CalculateDistance(moveX, moveY, 0) < 1000)
					roam = false;

				//Timer for roaming
				timer.Update();
				//random movement, after a few seconds
				if(timer.GetSec() <= 0 && health >= 4)
				{
					curState = ATTACK;
				}
				else if(timer.GetSec() <= 0 && health < 4)
				{
					curState = REST;
				}
					
			}
			//Attack the player after roaming for a few seconds
			if(curState == ATTACK)
			{
				if (attack == false)
				{
					attack = true;
					Destination.x = (player)->GetPos_x()+mapOffset_x - Enemy_x;
					Destination.y = (player)->GetPos_y() - Enemy_y;
					Destination.normalizeVector3D();
				}
				//Attack, move towards the char "last" position
				Enemy_x += Destination.x * 6;
				Enemy_y += Destination.y * 6;

				//After reaching the destination/limiting it
				if (CalculateDistance((player)->GetPos_x()+mapOffset_x, (player)->GetPos_y(), 0) < 1000 || Enemy_y > 470 || Enemy_x > 1545)
				{
					timer.Init(0, 10);
					roam = false;
					attack = false;
					curState = ROAM;
				}
			}
			// Rest
			if (curState == REST)
			{
				if (attack == false)
				{
					attack = true;
					Destination.x = 0+mapOffset_x - Enemy_x;
					Destination.y = 150 - Enemy_y;
					Destination.normalizeVector3D();
				}

				//After reaching the destination/limiting it
				if (CalculateDistance(0+mapOffset_x, 150, 0) < 1000 && !Fire)
				{
					attack = false;
					Fire = true;
				}
				else if (!Fire)
				{
					//Attack, move towards the top right
					Enemy_x += Destination.x * 6;
					Enemy_y += Destination.y * 6;
				}
			}
			//Fireball attack
			if(Fire == true)
			{
				if (attack == false)
				{
					attack = true;
					Destination2.x = (player)->GetPos_x() - fireballX;
					Destination2.y = (player)->GetPos_y() - fireballY;
					Destination2.normalizeVector3D();
				}
				fireballX += Destination2.x * 12;
				fireballY += Destination2.y * 12;

				cout << fireballX << " " << fireballY << endl;

				if (CalculateDistance2((player)->GetPos_x(), (player)->GetPos_y(), 0) < 1000 || fireballY > 470)
				{
					fireballX = -50;
					fireballY = 90;
					timer.Init(0, 10);
					Fire = false;
					roam = false;
					attack = false;
					curState = ROAM;
				}
			}

			cout << timer.GetSec() << endl;
			cout << "state:" << curState << endl;
			//When HP reaches 5/10, ATTACK state will be replaced by FIRE state
		}
		break;
	default:
		break;
	}
}

// Calculate the distance between this enemy and the hero's position
int Enemies::CalculateDistance(const float hero_x, const float hero_y, const int mapOffset_x)
{
	return (((hero_x + mapOffset_x) - Enemy_x) * ((hero_x + mapOffset_x) - Enemy_x) + (hero_y - Enemy_y) * (hero_y - Enemy_y));
}

// Calculate the distance between firball and the hero's position
int Enemies::CalculateDistance2(const float hero_x, const float hero_y, const int mapOffset_x)
{
	return (((hero_x + mapOffset_x) - fireballX) * ((hero_x + mapOffset_x) - fireballX) + (hero_y - fireballY) * (hero_y - fireballY));
}

// Set the position X of the enemy
void Enemies::SetPos_x(const float Enemy_x)
{
	this->Enemy_x = Enemy_x;
}

// Set the position Y of the enemy
void Enemies::SetPos_y(const float Enemy_y)
{
	this->Enemy_y = Enemy_y;
}

// Get Position X of the Enemy
float Enemies::GetPos_x(void)
{
	return Enemy_x;
}

// Get Position Y of the Enemy
float Enemies::GetPos_y(void)
{
	return Enemy_y;
}

// Get Position X of the fireball
float Enemies::GetFireBallPos_x(void)
{
	return fireballX;
}

// Get Position Y of the fireball
float Enemies::GetFireBallPos_y(void)
{
	return fireballY;
}

void Enemies::RenderEnemies(int mapOffset_x)
{
	if (texturetype != 2 && texturetype != 6 && texturetype != 7)
	{
		if (texturetype != 8)
			MobCounter = 20;
		else
			MobCounter = 70;

		SetMovingAnimation(MovingAnimation + 1);
		if (GetMovingAnimation() > MobCounter)
		{
			SetMovingAnimation(0);
			EnemyAnimationCounter += 1;
		}

		if (texturetype != 8)
		{
			if (EnemyAnimationCounter > 3)
				EnemyAnimationCounter = 0;
		}
		else
		{
			if (EnemyAnimationCounter > 1)
				EnemyAnimationCounter = 0;
		}
	}
	else if (texturetype == 6)
	{
		MobCounter = 15;
		SetMovingAnimation(MovingAnimation + 1);
		if (GetMovingAnimation() > MobCounter)
		{
			SetMovingAnimation(0);
			EnemyAnimationCounter += 1;
		}

		if (EnemyAnimationCounter > 4)
			EnemyAnimationCounter = 0;
	}

	if (texturetype ==	1)
	{
		TexCoordX = 0.5;
		CounterX = 0.5;
	}
	else if (texturetype == 2)
	{
		CounterX = 1;
		Tile_SizeX = 150;
		Tile_SizeY = 50;
		Enemy_y = 480;
	}
	else if (texturetype == 3)
	{
		TexCoordX = 0.35;
		CounterX = 0.3;
	}
	else if (texturetype == 4)
	{
		TexCoordX = 0.1;
		CounterX = 0.1;
		Tile_SizeX = 150;
		Tile_SizeY = 300;
	}
	else if (texturetype == 6)
	{
		TexCoordX = 0.25;
		CounterX = 0.25;
		Tile_SizeX = 200;
		Tile_SizeY = 100;
		Enemy_y = 450;
	}
	else if (texturetype == 7)
	{
		CounterX = 1;
		Tile_SizeX = 60;
		Tile_SizeY = 110;
		Enemy_y = 220;
	}
	else if (texturetype == 8)
	{
		TexCoordX = 0.35;
		CounterX = 0.3;
		Tile_SizeX = 100;
		Tile_SizeY = 140;
	}

	Renderfire();

	//if (id == 4)
	//RenderExplosion(mapOffset_x);

	glPushMatrix();
	if (id != 4)
		glTranslatef(Enemy_x - mapOffset_x, Enemy_y - 33, 0);
	else if (id == 4)
		glTranslatef(Enemy_x - 45 - mapOffset_x, Enemy_y - 120, 0);

	glScalef(0.6, 0.6, 0.6);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glBindTexture(GL_TEXTURE_2D, EnemyTexture[texturetype].texID);
	glBegin(GL_QUADS);
	if (EnemyAnimationInvert)
	{
		glTexCoord2f(TexCoordX * EnemyAnimationCounter,1); glVertex2f(0,0);
		glTexCoord2f(TexCoordX * EnemyAnimationCounter,0); glVertex2f(0,Tile_SizeY);
		glTexCoord2f(TexCoordX * EnemyAnimationCounter + CounterX,0); glVertex2f(Tile_SizeX, Tile_SizeY);
		glTexCoord2f(TexCoordX * EnemyAnimationCounter + CounterX,1); glVertex2f(Tile_SizeX,0);
	}
	else
	{
		glTexCoord2f(TexCoordX * EnemyAnimationCounter + CounterX,1); glVertex2f(0,0);
		glTexCoord2f(TexCoordX * EnemyAnimationCounter + CounterX,0); glVertex2f(0,Tile_SizeY);
		glTexCoord2f(TexCoordX * EnemyAnimationCounter,0); glVertex2f(Tile_SizeX,Tile_SizeY);
		glTexCoord2f(TexCoordX * EnemyAnimationCounter,1); glVertex2f(Tile_SizeX,0);
	}
	glEnd();
	glDisable(GL_BLEND);
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
}

//Render the fireball
void Enemies::Renderfire()
{
	glPushMatrix();
	glTranslatef(fireballX, fireballY , 0);
	glScalef(0.6, 0.6, 0.6);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glBindTexture(GL_TEXTURE_2D, EnemyTexture[5].texID);
	glBegin(GL_QUADS);
				glTexCoord2f(0,1); glVertex2f(0,0);
				glTexCoord2f(0,0); glVertex2f(0,60);
				glTexCoord2f(1,0); glVertex2f(60,60);
				glTexCoord2f(1,1); glVertex2f(60,0);
	glEnd();
	glDisable(GL_BLEND);
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
}

bool Enemies::LoadTGA(TextureImage *texture, char *filename)			// Loads A TGA File Into Memory
{    
	GLubyte		TGAheader[12]={0,0,2,0,0,0,0,0,0,0,0,0};	// Uncompressed TGA Header
	GLubyte		TGAcompare[12];								// Used To Compare TGA Header
	GLubyte		header[6];									// First 6 Useful Bytes From The Header
	GLuint		bytesPerPixel;								// Holds Number Of Bytes Per Pixel Used In The TGA File
	GLuint		imageSize;									// Used To Store The Image Size When Setting Aside Ram
	GLuint		temp;										// Temporary Variable
	GLuint		type=GL_RGBA;								// Set The Default GL Mode To RBGA (32 BPP)

	FILE *file = fopen(filename, "rb");						// Open The TGA File

	if(	file==NULL ||										// Does File Even Exist?
		fread(TGAcompare,1,sizeof(TGAcompare),file)!=sizeof(TGAcompare) ||	// Are There 12 Bytes To Read?
		memcmp(TGAheader,TGAcompare,sizeof(TGAheader))!=0				||	// Does The Header Match What We Want?
		fread(header,1,sizeof(header),file)!=sizeof(header))				// If So Read Next 6 Header Bytes
	{
		if (file == NULL)									// Did The File Even Exist? *Added Jim Strong*
			return false;									// Return False
		else
		{
			fclose(file);									// If Anything Failed, Close The File
			return false;									// Return False
		}
	}

	texture->width  = header[1] * 256 + header[0];			// Determine The TGA Width	(highbyte*256+lowbyte)
	texture->height = header[3] * 256 + header[2];			// Determine The TGA Height	(highbyte*256+lowbyte)

	if(	texture->width	<=0	||								// Is The Width Less Than Or Equal To Zero
		texture->height	<=0	||								// Is The Height Less Than Or Equal To Zero
		(header[4]!=24 && header[4]!=32))					// Is The TGA 24 or 32 Bit?
	{
		fclose(file);										// If Anything Failed, Close The File
		return false;										// Return False
	}

	texture->bpp	= header[4];							// Grab The TGA's Bits Per Pixel (24 or 32)
	bytesPerPixel	= texture->bpp/8;						// Divide By 8 To Get The Bytes Per Pixel
	imageSize		= texture->width*texture->height*bytesPerPixel;	// Calculate The Memory Required For The TGA Data

	texture->imageData=(GLubyte *)malloc(imageSize);		// Reserve Memory To Hold The TGA Data

	if(	texture->imageData==NULL ||							// Does The Storage Memory Exist?
		fread(texture->imageData, 1, imageSize, file)!=imageSize)	// Does The Image Size Match The Memory Reserved?
	{
		if(texture->imageData!=NULL)						// Was Image Data Loaded
			free(texture->imageData);						// If So, Release The Image Data

		fclose(file);										// Close The File
		return false;										// Return False
	}

	for(GLuint i=0; i<int(imageSize); i+=bytesPerPixel)		// Loop Through The Image Data
	{														// Swaps The 1st And 3rd Bytes ('R'ed and 'B'lue)
		temp=texture->imageData[i];							// Temporarily Store The Value At Image Data 'i'
		texture->imageData[i] = texture->imageData[i + 2];	// Set The 1st Byte To The Value Of The 3rd Byte
		texture->imageData[i + 2] = temp;					// Set The 3rd Byte To The Value In 'temp' (1st Byte Value)
	}

	fclose (file);											// Close The File

	// Build A Texture From The Data
	glGenTextures(1, &texture[0].texID);					// Generate OpenGL texture IDs

	glBindTexture(GL_TEXTURE_2D, texture[0].texID);			// Bind Our Texture
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);	// Linear Filtered
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);	// Linear Filtered

	if (texture[0].bpp==24)									// Was The TGA 24 Bits
	{
		type=GL_RGB;										// If So Set The 'type' To GL_RGB
	}

	glTexImage2D(GL_TEXTURE_2D, 0, type, texture[0].width, texture[0].height, 0, type, GL_UNSIGNED_BYTE, texture[0].imageData);

	return true;											// Texture Building Went Ok, Return True
}