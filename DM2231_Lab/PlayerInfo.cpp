#include "PlayerInfo.h"
#include <iostream>

CPlayerInfo::CPlayerInfo(float locx_, float locy_, int id)
:	LoadingLine(0)
,	m_iTileSize(100)
,	jumpspeed(0)
,	hero_inMidAir_Up(false)
,	hero_inMidAir_Down(false)
,	heroAnimationCounter(0)
,	heroAnimationInvert(false)
,	dead(false)
,	Points(0)
,	lives(3)
,	MovingAnimation(0)
,	TexCoordX(0.25)
,	CounterX(0.1)
,	Tile_SizeX(100)
,	Tile_SizeY(100)
,	TextureType(0)
,	TranslateX(0)
,	TranslateY(0)
,	StopAnimation(false)
,	Counter(0)
,	keydown_q(false)
,	keydown_e(false)
,	keydown_u(false)
,	keydown_p(false)
,	Health(5)
,	invulnerable(false)
,	blink(1.0f)
,	Blinking(false)
,	render(true)
,	Animation(false)
{
	this->id = id;
	hero_x = locx_;
	hero_y = locy_;
	LoadTGA( &(HeroTexture[ 0 ]), "images/cop.tga");
	LoadTGA( &(HeroTexture[ 1 ]), "images/Whip.tga");
	LoadTGA( &(HeroTexture[ 2 ]), "images/Knife.tga");
	LoadTGA( &(HeroTexture[ 3 ]), "images/deathsprite.tga");
	theMap = new CMap();
	theMap->Init( 600, 800, 600, 1600, m_iTileSize );
}

CPlayerInfo::CPlayerInfo(void)
{
}

CPlayerInfo::~CPlayerInfo(void)
{
}

// Initialise this class instance
void CPlayerInfo::Init(int PosX, int PosY)
{
	m_iTileSize = 25;
	hero_x = PosX;
	hero_y = PosY;
	jumpspeed = 0;
	hero_inMidAir_Up = false;
	hero_inMidAir_Down = false;
	heroAnimationCounter = 0;
	heroAnimationInvert = false;
	dead = false;
	Points = 0;
	lives = 3;

	theMap = new CMap();
	theMap->Init( 600, 800, 600, 1600, m_iTileSize );
}

/****************************************************************************************************
Draw the hero
****************************************************************************************************/
void CPlayerInfo::RenderHero(void) {
	if (render)
	{
		if (invulnerable)
		{
			if (!Blinking)
				blink-=0.05f;
			else
				blink+=0.05f;

			if (blink <= 0.0f && !Blinking)
				Blinking = true;
			else if (blink >= 1.0f && Blinking)
				Blinking = false;
		}
		else
			blink = 1.0f;

		glPushMatrix();
		glTranslatef(hero_x, hero_y - 31, 0);
		glTranslatef(TranslateX, TranslateY, 0);
		glScalef(0.4, 0.6, 0.0);
		glEnable( GL_TEXTURE_2D );
		glEnable( GL_BLEND );
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glBindTexture( GL_TEXTURE_2D, HeroTexture[TextureType].texID );
		glColor4f(1.0f, 1.0f, 1.0f, blink);
		glBegin(GL_QUADS);
		if (!Animation)
			heroAnimationCounter = 0;

		if (heroAnimationInvert == false)
		{
			glTexCoord2f(TexCoordX * heroAnimationCounter,1); glVertex2f(0,0);
			glTexCoord2f(TexCoordX * heroAnimationCounter,0); glVertex2f(0,Tile_SizeY);
			glTexCoord2f(TexCoordX * heroAnimationCounter + CounterX,0); glVertex2f(Tile_SizeX,Tile_SizeY);
			glTexCoord2f(TexCoordX * heroAnimationCounter + CounterX,1); glVertex2f(Tile_SizeX,0);
		}
		else
		{
			glTexCoord2f(TexCoordX * heroAnimationCounter + CounterX,1); glVertex2f(0,0);
			glTexCoord2f(TexCoordX * heroAnimationCounter + CounterX,0); glVertex2f(0,Tile_SizeY);
			glTexCoord2f(TexCoordX * heroAnimationCounter,0); glVertex2f(Tile_SizeX,Tile_SizeY);
			glTexCoord2f(TexCoordX * heroAnimationCounter,1 ); glVertex2f(Tile_SizeX,0);
		}
		glEnd();
		glDisable( GL_BLEND );
		glDisable( GL_TEXTURE_2D );
		glPopMatrix();
	}
}

// Returns true if the player is on ground
bool CPlayerInfo::isOnGround(void)
{
	if (hero_inMidAir_Up == false && hero_inMidAir_Down == false)
		return true;
	return false;
}

// Returns true if the player is jumping upwards
bool CPlayerInfo::isJumpUpwards(void)
{
	if (hero_inMidAir_Up == true && hero_inMidAir_Down == false)
		return true;
	return false;
}

// Returns true if the player is on freefall
bool CPlayerInfo::isFreeFall(void)
{
	if (hero_inMidAir_Up == false && hero_inMidAir_Down == true)
		return true;
	return false;
}
// Set the player's status to free fall mode
void CPlayerInfo::SetOnFreeFall(bool isOnFreeFall)
{
	if (isOnFreeFall == true)
	{
		hero_inMidAir_Up = false;
		hero_inMidAir_Down = true;
		jumpspeed = 0;
	}
}

// Set the player to jumping upwards
void CPlayerInfo::SetToJumpUpwards(bool isOnJumpUpwards)
{
	if (isOnJumpUpwards == true)
	{
		hero_inMidAir_Up = true;
		hero_inMidAir_Down = false;
		jumpspeed = 13;
	}
}

// Set position x of the player
void CPlayerInfo::SetPos_x(float pos_x)
{
	hero_x = pos_x;
}

// Set position y of the player
void CPlayerInfo::SetPos_y(float pos_y)
{
	hero_y = pos_y;
}

// Set Jumpspeed of the player
void CPlayerInfo::SetJumpspeed(int jumpspeed)
{
	this->jumpspeed = jumpspeed;
}

// Stop the player's movement
void CPlayerInfo::SetToStop(void)
{
	hero_inMidAir_Up = false;
	hero_inMidAir_Down = false;
	jumpspeed = 0;
}

// Get position x of the player
float CPlayerInfo::GetPos_x(void)
{
	return hero_x;
}

// Get position y of the player
float CPlayerInfo::GetPos_y(void)
{
	return hero_y;
}

// Get Jumpspeed of the player
int CPlayerInfo::GetJumpspeed(void)
{
	return jumpspeed;
}

void CPlayerInfo::SetMapOffset(int mapOffset_x)
{
	this->mapOffset_x = mapOffset_x;
}

int CPlayerInfo::GetMapOffset()
{
	return mapOffset_x;
}

// Update Jump Upwards
void CPlayerInfo::UpdateJumpUpwards()
{
	hero_y -= jumpspeed;
	jumpspeed -= 1;
	if (jumpspeed == 0)
	{
		hero_inMidAir_Up = false;
		hero_inMidAir_Down = true;
	}
}

// Update FreeFall
void CPlayerInfo::UpdateFreeFall(int height)
{
	hero_y += jumpspeed;
	jumpspeed += height;
}

// Set Animation Invert status of the player
void CPlayerInfo::SetAnimationInvert(bool heroAnimationInvert)
{
	this->heroAnimationInvert = heroAnimationInvert;
}

// Get Animation Invert status of the player
bool CPlayerInfo::GetAnimationInvert(void)
{
	return heroAnimationInvert;
}

// Set Animation Counter of the player
void CPlayerInfo::SetAnimationCounter(int heroAnimationCounter)
{
	this->heroAnimationCounter = heroAnimationCounter;
}

// Get Animation Counter of the player
int CPlayerInfo::GetAnimationCounter(void)
{
	return heroAnimationCounter;
}

// Constrain the position of the Hero to within the border
void CPlayerInfo::ConstrainHero( const int leftBorder, const int rightBorder,
								const int topBorder, const int bottomBorder,
								float timeDiff,
								int& mapOffset_x, int& mapOffset_y)
{
	if (hero_x < leftBorder)
	{
		hero_x = leftBorder;
		Animation = false;
		/*mapOffset_x = mapOffset_x - (int) (5.0f * timeDiff);
		if (mapOffset_x < 0)
			mapOffset_x = 0;*/
	}
	else if (hero_x > rightBorder)
	{
		hero_x = rightBorder;
		Animation = false;
		//mapOffset_x = mapOffset_x + (int) (5.0f * timeDiff);
		//if (mapOffset_x > theMap->getScreenWidth())
		//	mapOffset_x = theMap->getScreenWidth();
	}
	else
		Animation = true;

	if (hero_y < topBorder)
		hero_y = topBorder;
	else if (hero_y > bottomBorder)
		hero_y = bottomBorder;
}

void CPlayerInfo::ResetAnimation()
{
	TextureType = 0;
	TexCoordX = 0.25;
	CounterX = 0.1;
	Tile_SizeX = 100;
	Tile_SizeY = 100;
	TranslateX = 0;
	TranslateY = 0;
}

void CPlayerInfo::Animate(int type)
{
	TextureType = type;
	
	if (type != 3)
		WeaponAnimation();
	else
		AnimateDeath();
}

void CPlayerInfo::WeaponAnimation()
{
	TexCoordX = 0.2;
	CounterX = 0.15;
	Tile_SizeX = 400;
	Tile_SizeY = 100;

	if (!GetAnimationInvert())
	{
		TranslateX = -43;
		TranslateY = -5;
	}
	else
	{
		TranslateX = -83;
		TranslateY = -5;
	}

	if (GetMovingAnimation() > 5)
	{
		SetMovingAnimation(0);
		SetAnimationCounter( GetAnimationCounter() + 1);
	}
	else
		SetMovingAnimation(GetMovingAnimation() + 1);

	if (GetAnimationCounter()>5)
	{
		ResetAnimation();
		SetAnimationCounter( 0 );
		StopAnimation = true;
	}
}

void CPlayerInfo::AnimateDeath()
{
	if (GetMovingAnimation() > 5)
	{
		SetMovingAnimation(0);
		SetAnimationCounter( GetAnimationCounter() + 1);
	}
	else
		SetMovingAnimation(GetMovingAnimation() + 1);

	if (GetAnimationCounter()>3)
	{
		SetAnimationCounter( 0 );
		StopAnimation = true;
	}
}

void CPlayerInfo::SetKeyDown(char key, bool keydown)
{
	if (key == 'q')
		this->keydown_q = keydown;
	else if (key == 'e')
		this->keydown_e = keydown;
	else if (key == 'u')
		this->keydown_u = keydown;
	else if (key == 'p')
		this->keydown_p = keydown;
}

bool CPlayerInfo::GetKeyDown(char key)
{
	if (key == 'q')
		return keydown_q;
	else if (key == 'e')
		return keydown_e;
	else if (key == 'u')
		return keydown_u;
	else if (key == 'p')
		return keydown_p;
}

int CPlayerInfo::CalculateDistance(const float enemy_x, const float enemy_y, const int mapOffset_x)
{
	return (((enemy_x - mapOffset_x) - hero_x) * ((enemy_x - mapOffset_x) - hero_x) + (enemy_y - hero_y) * (enemy_y - hero_y));
}

void CPlayerInfo::Invulnerability()
{
	if (timer.GetSec() == 0)
		timer.StartInvulTimer();
	timer.SetSec(5);
	timer.SetMin(0);
	timer.Update();
	if (timer.GetSec() <= 0)
		invulnerable = false;
}

void CPlayerInfo::Saving()
{
	ofstream myfile;
	myfile.open ("state.txt");
	myfile << hero_x << "\n";
	myfile << hero_y << "\n";
	myfile << heroAnimationInvert << "\n";
	myfile << Points << "\n";
	myfile << lives;
	myfile.close();
}

void CPlayerInfo::Loading()
{
	string line;
	ifstream myfile ("state.txt");
	if (myfile.is_open())
	{
		while ( myfile.good() )
		{
		  getline (myfile,line);
		  int value = atoi(line.c_str());
			  if (LoadingLine == 0)
			  {
				 hero_x = value;
				 LoadingLine ++;
			  }
			  else if (LoadingLine == 1)
			  {
				 hero_y = value;
				 LoadingLine ++;
			  }
			  else if (LoadingLine == 2)
			  {
				  heroAnimationInvert = value;
				  LoadingLine ++;
			  }
			  else if (LoadingLine == 3)
			  {
				  Points = value;
				  LoadingLine ++;
			  }
			  else if (LoadingLine == 4)
			  {
				  lives = value;
				  LoadingLine = 0;
			  }
	}
	myfile.close();
	}

	else cout << "Unable to open file"; 
}

bool CPlayerInfo::LoadTGA(TextureImage *texture, char *filename)			// Loads A TGA File Into Memory
{    
	GLubyte		TGAheader[12]={0,0,2,0,0,0,0,0,0,0,0,0};	// Uncompressed TGA Header
	GLubyte		TGAcompare[12];								// Used To Compare TGA Header
	GLubyte		header[6];									// First 6 Useful Bytes From The Header
	GLuint		bytesPerPixel;								// Holds Number Of Bytes Per Pixel Used In The TGA File
	GLuint		imageSize;									// Used To Store The Image Size When Setting Aside Ram
	GLuint		temp;										// Temporary Variable
	GLuint		type=GL_RGBA;								// Set The Default GL Mode To RBGA (32 BPP)

	FILE *file = fopen(filename, "rb");						// Open The TGA File

	if(	file==NULL ||										// Does File Even Exist?
		fread(TGAcompare,1,sizeof(TGAcompare),file)!=sizeof(TGAcompare) ||	// Are There 12 Bytes To Read?
		memcmp(TGAheader,TGAcompare,sizeof(TGAheader))!=0				||	// Does The Header Match What We Want?
		fread(header,1,sizeof(header),file)!=sizeof(header))				// If So Read Next 6 Header Bytes
	{
		if (file == NULL)									// Did The File Even Exist? *Added Jim Strong*
			return false;									// Return False
		else
		{
			fclose(file);									// If Anything Failed, Close The File
			return false;									// Return False
		}
	}

	texture->width  = header[1] * 256 + header[0];			// Determine The TGA Width	(highbyte*256+lowbyte)
	texture->height = header[3] * 256 + header[2];			// Determine The TGA Height	(highbyte*256+lowbyte)

	if(	texture->width	<=0	||								// Is The Width Less Than Or Equal To Zero
		texture->height	<=0	||								// Is The Height Less Than Or Equal To Zero
		(header[4]!=24 && header[4]!=32))					// Is The TGA 24 or 32 Bit?
	{
		fclose(file);										// If Anything Failed, Close The File
		return false;										// Return False
	}

	texture->bpp	= header[4];							// Grab The TGA's Bits Per Pixel (24 or 32)
	bytesPerPixel	= texture->bpp/8;						// Divide By 8 To Get The Bytes Per Pixel
	imageSize		= texture->width*texture->height*bytesPerPixel;	// Calculate The Memory Required For The TGA Data

	texture->imageData=(GLubyte *)malloc(imageSize);		// Reserve Memory To Hold The TGA Data

	if(	texture->imageData==NULL ||							// Does The Storage Memory Exist?
		fread(texture->imageData, 1, imageSize, file)!=imageSize)	// Does The Image Size Match The Memory Reserved?
	{
		if(texture->imageData!=NULL)						// Was Image Data Loaded
			free(texture->imageData);						// If So, Release The Image Data

		fclose(file);										// Close The File
		return false;										// Return False
	}

	for(GLuint i=0; i<int(imageSize); i+=bytesPerPixel)		// Loop Through The Image Data
	{														// Swaps The 1st And 3rd Bytes ('R'ed and 'B'lue)
		temp=texture->imageData[i];							// Temporarily Store The Value At Image Data 'i'
		texture->imageData[i] = texture->imageData[i + 2];	// Set The 1st Byte To The Value Of The 3rd Byte
		texture->imageData[i + 2] = temp;					// Set The 3rd Byte To The Value In 'temp' (1st Byte Value)
	}

	fclose (file);											// Close The File

	// Build A Texture From The Data
	glGenTextures(1, &texture[0].texID);					// Generate OpenGL texture IDs

	glBindTexture(GL_TEXTURE_2D, texture[0].texID);			// Bind Our Texture
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);	// Linear Filtered
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);	// Linear Filtered

	if (texture[0].bpp==24)									// Was The TGA 24 Bits
	{
		type=GL_RGB;										// If So Set The 'type' To GL_RGB
	}

	glTexImage2D(GL_TEXTURE_2D, 0, type, texture[0].width, texture[0].height, 0, type, GL_UNSIGNED_BYTE, texture[0].imageData);

	return true;											// Texture Building Went Ok, Return True
}