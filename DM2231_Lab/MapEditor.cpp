#include "MapEditor.h"

CMapEditor::CMapEditor(void)
{
	theEditorMap.clear();
}

CMapEditor::~CMapEditor(void)
{
	theEditorMap.clear();
}

// Initalization the Map Editor variables
void CMapEditor::Init(TextureImage TileTexture[], 
							   int MapTileOffSet_x, int MapOffSet_x,
							   int theNumOfTiles_GridWidth, int theNumOfTiles_GridHeight,
							   int theNumOfTiles_EditorWidth, int theNumOfTiles_EditorHeight,
							   int theNumOfTiles_MapWidth, vector<vector<int>> theGridMap,
							   vector<vector<int>> theEditorMap)
{
	string LevelName;
	m_bSavingMap = false;
	m_bOverWriteConfirmation = false;
	m_iMouseOnButton = 0;
	m_iTextLine = 0;

	LoadTGA( &MapEditorButtonsTexture[0], "images/MapEditor/SaveButton.tga");
	LoadTGA( &MapEditorButtonsTexture[1], "images/MapEditor/BackButton.tga");
	LoadTGA( &MapEditorButtonsTexture[2], "images/MapEditor/DoneButton.tga");
	LoadTGA( &MapEditorButtonsTexture[3], "images/MapEditor/ClearNameButton.tga");
	LoadTGA( &MapEditorButtonsTexture[4], "images/MapEditor/SaveScreen.tga");
	LoadTGA( &MapEditorButtonsTexture[5], "images/MapEditor/NoButton.tga");
	LoadTGA( &MapEditorButtonsTexture[6], "images/MapEditor/YesButton.tga");
	LoadTGA( &MapEditorButtonsTexture[7], "images/MapEditor/OverrideScreen.tga");

	m_bMouseClick = false;
	m_iTileSelected = 0;

	// Set tile offset_x
	this->m_iMapTileOffSet_x = MapTileOffSet_x;

	// Set map offset_x
	this->m_iMapOffSet_x = MapOffSet_x;

	// Set the number of tiles for width of the screen, grid map
	this->m_iTheNumOfTiles_GridWidth = theNumOfTiles_GridWidth;
	// Set the number of tiles for height of the screen, grid map
	this->m_iTheNumOfTiles_GridHeight = theNumOfTiles_GridHeight;
	
	// Set the number of tiles for width of the screen, editor map
	this->m_iTheNumOfTiles_EditorWidth = theNumOfTiles_EditorWidth;
	// Set the number of tiles for height of the screen, editor map
	this->m_iTheNumOfTiles_EditorHeight = theNumOfTiles_EditorHeight;

	// Set the number of tiles for width of the map
	this->m_iTheNumOfTiles_MapWidth = theNumOfTiles_MapWidth;

	// Copy over the textures
	for (int i = 0; i < 32; i ++)
	{
		this->TileTexture[i] = TileTexture[i];
	}

	// Copy the grid map
	this->theGridMap = theGridMap;

	this->theEditorMap = theEditorMap;
}

// Draw the grid on the screen
void CMapEditor::RenderGrid(void)
{
	glPushMatrix();
		for (int i = 4; i < m_iTheNumOfTiles_GridHeight-3; i ++)
		{
			for (int k = 0; k < m_iTheNumOfTiles_GridWidth; k ++)
			{
				glPushMatrix();
					glTranslatef(k * TILE_SIZE, i * TILE_SIZE, 0);
					glEnable(GL_TEXTURE_2D);
					glEnable(GL_BLEND);
					glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
					glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
					glBindTexture( GL_TEXTURE_2D, TileTexture[31].texID );
					glBegin(GL_QUADS);
						glTexCoord2f(0,1); glVertex2f(0,0);
						glTexCoord2f(0,0); glVertex2f(0,TILE_SIZE);
						glTexCoord2f(1,0); glVertex2f(TILE_SIZE,TILE_SIZE);
						glTexCoord2f(1,1); glVertex2f(TILE_SIZE,0);
					glEnd();
					glDisable( GL_BLEND );
					glDisable( GL_TEXTURE_2D );
				glPopMatrix();
			}
		}
	glPopMatrix();
}

// Draw the map on the screen
void CMapEditor::RenderMap(void) 
{
	glPushMatrix();
	for(int i = 0; i < m_iTheNumOfTiles_EditorHeight; i ++)
	{
		for(int k = 0; k < m_iTheNumOfTiles_EditorWidth + 1; k ++)
		{
			// If we have reached the right side of the Map, then do not display the extra column of tiles
			if ((m_iMapTileOffSet_x+k) >= m_iTheNumOfTiles_MapWidth)
				break;

			glPushMatrix();
			glTranslatef(k * TILE_SIZE - m_iMapFineOffSet_x, i * TILE_SIZE, 0);
			glEnable( GL_TEXTURE_2D );
			glEnable( GL_BLEND );
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glBindTexture( GL_TEXTURE_2D, TileTexture[theEditorMap[i][m_iMapTileOffSet_x+k]].texID );
				glBegin(GL_QUADS);
					glTexCoord2f(0,1); glVertex2f(0,0);
					glTexCoord2f(0,0); glVertex2f(0,TILE_SIZE);
					glTexCoord2f(1,0); glVertex2f(TILE_SIZE,TILE_SIZE);
					glTexCoord2f(1,1); glVertex2f(TILE_SIZE,0);
				glEnd();
			glDisable( GL_BLEND );
			glDisable( GL_TEXTURE_2D );
			glPopMatrix();
		}
	}
	glPopMatrix();
}

// Set the tile Offset for x axis
int CMapEditor::GetTileSelected (void)
{
	return m_iTileSelected;
}

bool CMapEditor::GetMouseClick (void)
{
	return m_bMouseClick;
}
void CMapEditor::SetMaptileOffSet(int MapTileOffSet_x)
{
	this->m_iMapTileOffSet_x = MapTileOffSet_x;
}

void CMapEditor::SetMouseLast (void)
{
	this->m_iMouseLastX = m_iMouseX * 800 / glutGet(GLUT_WINDOW_WIDTH);
		
	this->m_iMouseLastY = m_iMouseY * 600 / glutGet(GLUT_WINDOW_HEIGHT);
}

void CMapEditor::SetMouseTilePos (void)
{
	this->m_iMouseTilePosX = (m_iMouseX * (800.0 / glutGet(GLUT_WINDOW_WIDTH)) + m_iMapOffSet_x) / 25;

	this->m_iMouseTilePosY = m_iMouseY * (600.0 / glutGet(GLUT_WINDOW_HEIGHT)) / 25;
}

// Set the map fine Offset for x axis
void CMapEditor::SetMapFineOffSet(int MapFineOffSet_x)
{
	this->m_iMapFineOffSet_x = MapFineOffSet_x;
}

// Set the map Offset for x axis
void CMapEditor::SetMapOffSet(int MapOffSet_x)
{
	this->m_iMapOffSet_x = MapOffSet_x;
}

// Set the textures when changing season
void CMapEditor::SetTileSelected (int TileSelected)
{
	this-> m_iTileSelected = TileSelected;
}

void CMapEditor::SetMouseClick (bool MouseClick)
{
	this->m_bMouseClick = MouseClick;
}
void CMapEditor::Update (int MouseX, int MouseY, int MapOffSet_x)
{
	this->m_iMouseX = MouseX;
	this->m_iMouseY = MouseY;

	SetMouseLast ();
	SetMouseTilePos ();

	MapEditorMenuMouse();

}

//Rendering of Selectable tiles, save and back button
void CMapEditor::RenderMapEditorMenu(void)
{
	glPushMatrix();
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

			//Save Button
			glEnable( GL_TEXTURE_2D );
			glPushMatrix();
				glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
				glTranslatef(16 * TILE_SIZE, (m_iTheNumOfTiles_GridHeight-3) * TILE_SIZE, 0);
				if (m_iMouseOnButton == 4)
				{
					glBindTexture( GL_TEXTURE_2D, MapEditorButtonsTexture[6].texID );
				}
				else if (m_iMouseOnButton == 7)
				{
					glBindTexture( GL_TEXTURE_2D, MapEditorButtonsTexture[0].texID );
				}
				else
					glBindTexture( GL_TEXTURE_2D, MapEditorButtonsTexture[m_iMouseOnButton].texID );
						glBegin(GL_QUADS);
							glTexCoord2f(0,1); glVertex2f(0,0);
							glTexCoord2f(0,0); glVertex2f(0,TILE_SIZE * 3);
							glTexCoord2f(1,0); glVertex2f(TILE_SIZE * 16,TILE_SIZE * 3);
							glTexCoord2f(1,1); glVertex2f(TILE_SIZE * 16,0);
						glEnd();
			glPopMatrix();

			//Back Button
			glPushMatrix();
				glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
				glTranslatef(0, (m_iTheNumOfTiles_GridHeight-3) * TILE_SIZE, 0);
				if (m_iMouseOnButton == 4)
				{
					glBindTexture( GL_TEXTURE_2D, MapEditorButtonsTexture[5].texID );
				}
				else
					glBindTexture( GL_TEXTURE_2D, MapEditorButtonsTexture[1].texID );
						glBegin(GL_QUADS);
							glTexCoord2f(0,1); glVertex2f(0,0);
							glTexCoord2f(0,0); glVertex2f(0,TILE_SIZE * 3);
							glTexCoord2f(1,0); glVertex2f(TILE_SIZE * 16,TILE_SIZE * 3);
							glTexCoord2f(1,1); glVertex2f(TILE_SIZE * 16,0);
						glEnd();
			glPopMatrix();

			for(int k = 1; k < 32; k ++)
			{
				if (k < 12)
				{
				glPushMatrix();
					glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
					glTranslatef((2* (k-1)) * TILE_SIZE, 0, 0);
						glBindTexture( GL_TEXTURE_2D, TileTexture[k - 1].texID );
							glBegin(GL_QUADS);
								glTexCoord2f(0,1); glVertex2f(0,0);
								glTexCoord2f(0,0); glVertex2f(0,TILE_SIZE * 2);
								glTexCoord2f(1,0); glVertex2f(TILE_SIZE * 2,TILE_SIZE * 2);
								glTexCoord2f(1,1); glVertex2f(TILE_SIZE * 2,0);
							glEnd();
				glPopMatrix();
				}
				else if (k > 20)
				{
					glPushMatrix();
						glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
						glTranslatef((2* (k-21)) * TILE_SIZE, 2 * TILE_SIZE, 0);
							glBindTexture( GL_TEXTURE_2D, TileTexture[k - 1].texID );
								glBegin(GL_QUADS);
									glTexCoord2f(0,1); glVertex2f(0,0);
									glTexCoord2f(0,0); glVertex2f(0,TILE_SIZE * 2);
									glTexCoord2f(1,0); glVertex2f(TILE_SIZE * 2,TILE_SIZE * 2);
									glTexCoord2f(1,1); glVertex2f(TILE_SIZE * 2,0);
								glEnd();
					glPopMatrix();

					glPushMatrix();
						glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
						glTranslatef((2* (k-21)) * TILE_SIZE, 2 * TILE_SIZE, 0);
							glBindTexture( GL_TEXTURE_2D, TileTexture[31].texID );
								glBegin(GL_QUADS);
									glTexCoord2f(0,1); glVertex2f(0,0);
									glTexCoord2f(0,0); glVertex2f(0,TILE_SIZE * 2);
									glTexCoord2f(1,0); glVertex2f(TILE_SIZE * 2,TILE_SIZE * 2);
									glTexCoord2f(1,1); glVertex2f(TILE_SIZE * 2,0);
								glEnd();
					glPopMatrix();
				}
				else if (k > 17)
				{
				glPushMatrix();
					glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
					glTranslatef((2* (k-5)) * TILE_SIZE, 0, 0);
						glBindTexture( GL_TEXTURE_2D, TileTexture[k - 1].texID );
							glBegin(GL_QUADS);
								glTexCoord2f(0,1); glVertex2f(0,0);
								glTexCoord2f(0,0); glVertex2f(0,TILE_SIZE * 2);
								glTexCoord2f(1,0); glVertex2f(TILE_SIZE * 2,TILE_SIZE * 2);
								glTexCoord2f(1,1); glVertex2f(TILE_SIZE * 2,0);
							glEnd();
				glPopMatrix();
				}
				else if (k > 14 && k < 17)
				{
				glPushMatrix();
					glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
					glTranslatef((2* (k-4)) * TILE_SIZE, 0, 0);
						glBindTexture( GL_TEXTURE_2D, TileTexture[k - 1].texID );
							glBegin(GL_QUADS);
								glTexCoord2f(0,1); glVertex2f(0,0);
								glTexCoord2f(0,0); glVertex2f(0,TILE_SIZE * 2);
								glTexCoord2f(1,0); glVertex2f(TILE_SIZE * 2,TILE_SIZE * 2);
								glTexCoord2f(1,1); glVertex2f(TILE_SIZE * 2,0);
							glEnd();
				glPopMatrix();
				}
				glPushMatrix();
					glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
					glTranslatef((2* (k-1)) * TILE_SIZE, 0, 0);
						glBindTexture( GL_TEXTURE_2D, TileTexture[31].texID );
							glBegin(GL_QUADS);
								glTexCoord2f(0,1); glVertex2f(0,0);
								glTexCoord2f(0,0); glVertex2f(0,TILE_SIZE * 2);
								glTexCoord2f(1,0); glVertex2f(TILE_SIZE * 2,TILE_SIZE * 2);
								glTexCoord2f(1,1); glVertex2f(TILE_SIZE * 2,0);
							glEnd();
				glPopMatrix();
				
			} 
			glPushMatrix();
				glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
					glBindTexture( GL_TEXTURE_2D, TileTexture[m_iTileSelected].texID );
					glBegin(GL_QUADS);
					glTexCoord2f(0,1); glVertex2f(m_iMouseX, m_iMouseY);
						glTexCoord2f(0,0); glVertex2f(m_iMouseX, m_iMouseY + TILE_SIZE);
						glTexCoord2f(1,0); glVertex2f(m_iMouseX + TILE_SIZE, m_iMouseY + TILE_SIZE);
						glTexCoord2f(1,1); glVertex2f(m_iMouseX + TILE_SIZE, m_iMouseY);
					glEnd();
			glPopMatrix();
			
			if (m_iMouseOnButton == 2 || m_iMouseOnButton == 4)
			{
				glPushMatrix();
					glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
					if (m_iMouseOnButton == 4)
						glBindTexture( GL_TEXTURE_2D, MapEditorButtonsTexture[7].texID );
					else
						glBindTexture( GL_TEXTURE_2D, MapEditorButtonsTexture[4].texID );
							glBegin(GL_QUADS);
								glTexCoord2f(0,1); glVertex2f(0,0);
								glTexCoord2f(0,0); glVertex2f(0,525);
								glTexCoord2f(1,0); glVertex2f(800,525);
								glTexCoord2f(1,1); glVertex2f(800,0);
							glEnd();
				glPopMatrix();

				if (m_iMouseOnButton == 2)
				{
					glPushMatrix();
						glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
						glTranslatef(30 * TILE_SIZE - 5, 14 * TILE_SIZE - 5, 0);
							glBindTexture( GL_TEXTURE_2D, MapEditorButtonsTexture[3].texID );
								glBegin(GL_QUADS);
									glTexCoord2f(0,1); glVertex2f(0, 0);
									glTexCoord2f(0,0); glVertex2f(0, TILE_SIZE);
									glTexCoord2f(1,0); glVertex2f(TILE_SIZE, TILE_SIZE);
									glTexCoord2f(1,1); glVertex2f(TILE_SIZE, 0);
								glEnd();
					glPopMatrix();
				}
			}

			glDisable(GL_BLEND);
			glDisable( GL_TEXTURE_2D );
	glPopMatrix();
}


void CMapEditor::RenderTileMapEditor (int MapFineOffSet_X, int MapOffSet_X)
{
	MapFineOffSet_X = MapOffSet_X % TILE_SIZE;
	SetMapFineOffSet(MapFineOffSet_X);
	RenderMap();
}

//Replacing tiles
void CMapEditor::MapEditorTileMouse(void)
{
	// If the mouse is within the Opengl Window and is not touching the menu
	if ((m_iMouseX < glutGet(GLUT_WINDOW_WIDTH) && m_iMouseX > 0) && (m_iMouseY < glutGet(GLUT_WINDOW_HEIGHT) - 75 && m_iMouseY > 100))
	{
		// If the mouse is within the placeable area
		if ((m_iMouseTilePosX <= 127 && m_iMouseTilePosX >= 0) && (m_iMouseTilePosY < 26 && m_iMouseTilePosY > 0))
		{
			// Changes the map with the selected tile
			if (m_bMouseClick)
				theEditorMap[m_iMouseTilePosY][m_iMouseTilePosX+m_iMapTileOffSet_x] = m_iTileSelected;
		}
	}
}


//Selecting tiles
void CMapEditor::MapEditorMenuMouse(void)
{
	// If the mouse is within the map editor menu bar for the y axis
	if (m_iMouseLastY < 50 && m_iMouseLastY > 0)
	{
		// If the mouse is within the map editor menu bar for the x axis
		if (m_iMouseLastX < (m_iTheNumOfTiles_GridWidth * TILE_SIZE) && m_iMouseLastX > 0)
		{
			// Runs through all the tiles
			for (int i = 0; i < 20; i ++)
			{
				if (i < 11)
				{
					// Check which tile the player selects
					if ((m_iMouseLastX < (2 * (i+1) * TILE_SIZE) && m_iMouseLastX > (i) * TILE_SIZE))
					{
						// Sets the selected tile
						if (m_bMouseClick)
							m_iTileSelected = i;
						break;
					}
				}
				
				else if (i > 16)
				{
					// Check which tile the player selects
					if ((m_iMouseLastX < (2 * (i-3) * TILE_SIZE) && m_iMouseLastX > (i-4) * TILE_SIZE))
					{
						// Sets the selected tile
						if (m_bMouseClick)
							m_iTileSelected = i;
						break;
					}
				}
				else if (i > 13 && i < 16)
				{
					// Check which tile the player selects
					if ((m_iMouseLastX < (2 * (i-2) * TILE_SIZE) && m_iMouseLastX > (i-3) * TILE_SIZE))
					{
						// Sets the selected tile
						if (m_bMouseClick)
							m_iTileSelected = i;
						break;
					}
				}
			}
		}
	}
	if (m_iMouseLastY < 100 && m_iMouseLastY > 50)
	{
		// If the mouse is within the map editor menu bar for the x axis
		if (m_iMouseLastX < (m_iTheNumOfTiles_GridWidth * TILE_SIZE) && m_iMouseLastX > 0)
		{
			// Runs through all the tiles
			for (int i = 19; i < 31; i ++)
			{
				// Check which tile the player selects
				if ((m_iMouseLastX < (2 * (i-19) * TILE_SIZE) && m_iMouseLastX > (i-20) * TILE_SIZE))
				{
					// Sets the selected tile
					if (m_bMouseClick)
						m_iTileSelected = i;
					break;
				}
			}
		}
	}

	if (m_iMouseLastX < (TILE_SIZE * 16) && m_iMouseLastY > (m_iTheNumOfTiles_GridHeight-3) * TILE_SIZE)
	{
		if (m_bMouseClick)
		{
			if (m_bOverWriteConfirmation)
			{
				m_iMouseOnButton = 2;
			}
			else
			{
				if ( m_bSavingMap == false)
					m_iMouseOnButton = 1;
				else
					m_iMouseOnButton = 0;
			}
		}
	}
	else if (m_iMouseLastX > (TILE_SIZE * 16) && m_iMouseLastY > (m_iTheNumOfTiles_GridHeight-3) * TILE_SIZE)
	{
		if (m_bMouseClick)
		{
			if (m_bOverWriteConfirmation)
			{
				m_iMouseOnButton = 7;
			}
			else
			{
				if (m_bSavingMap == false)
					m_iMouseOnButton = 2;
				else
					m_iMouseOnButton = 4;
			}
		}
	}
	else if (m_iMouseLastX > ((30 * TILE_SIZE) - 5) && m_iMouseLastX < ((31 * TILE_SIZE) - 5)  && m_iMouseLastY > ((14 * TILE_SIZE) - 5) && m_iMouseLastY < ((15 * TILE_SIZE) - 5))
	{
		if (m_iMouseOnButton == 2 && m_bMouseClick == true)
			m_iMouseOnButton = 5;
	}
}

void CMapEditor::SaveMap (vector<vector<int>> *SaveTo)
{
	*SaveTo = theEditorMap;
}

int CMapEditor::GetMouseOnButton (void)
{
	return m_iMouseOnButton;
}

void CMapEditor::SetMouseOnButton (int MouseOnButton)
{
	this->m_iMouseOnButton = MouseOnButton;
}

bool CMapEditor::GetSavingMap (void)
{
	return m_bSavingMap;
}

void CMapEditor::SetSavingMap (bool SaveMap)
{
	this->m_bSavingMap = SaveMap;
}

bool CMapEditor::LoadTGA(TextureImage *texture, char *filename)			// Loads A TGA File Into Memory
{    
	GLubyte		TGAheader[12]={0,0,2,0,0,0,0,0,0,0,0,0};	// Uncompressed TGA Header
	GLubyte		TGAcompare[12];								// Used To Compare TGA Header
	GLubyte		header[6];									// First 6 Useful Bytes From The Header
	GLuint		bytesPerPixel;								// Holds Number Of Bytes Per Pixel Used In The TGA File
	GLuint		imageSize;									// Used To Store The Image Size When Setting Aside Ram
	GLuint		temp;										// Temporary Variable
	GLuint		type=GL_RGBA;								// Set The Default GL Mode To RBGA (32 BPP)

	FILE *file = fopen(filename, "rb");						// Open The TGA File

	if(	file==NULL ||										// Does File Even Exist?
		fread(TGAcompare,1,sizeof(TGAcompare),file)!=sizeof(TGAcompare) ||	// Are There 12 Bytes To Read?
		memcmp(TGAheader,TGAcompare,sizeof(TGAheader))!=0				||	// Does The Header Match What We Want?
		fread(header,1,sizeof(header),file)!=sizeof(header))				// If So Read Next 6 Header Bytes
	{
		if (file == NULL)									// Did The File Even Exist? *Added Jim Strong*
			return false;									// Return False
		else
		{
			fclose(file);									// If Anything Failed, Close The File
			return false;									// Return False
		}
	}

	texture->width  = header[1] * 256 + header[0];			// Determine The TGA Width	(highbyte*256+lowbyte)
	texture->height = header[3] * 256 + header[2];			// Determine The TGA Height	(highbyte*256+lowbyte)

	if(	texture->width	<=0	||								// Is The Width Less Than Or Equal To Zero
		texture->height	<=0	||								// Is The Height Less Than Or Equal To Zero
		(header[4]!=24 && header[4]!=32))					// Is The TGA 24 or 32 Bit?
	{
		fclose(file);										// If Anything Failed, Close The File
		return false;										// Return False
	}

	texture->bpp	= header[4];							// Grab The TGA's Bits Per Pixel (24 or 32)
	bytesPerPixel	= texture->bpp/8;						// Divide By 8 To Get The Bytes Per Pixel
	imageSize		= texture->width*texture->height*bytesPerPixel;	// Calculate The Memory Required For The TGA Data

	texture->imageData=(GLubyte *)malloc(imageSize);		// Reserve Memory To Hold The TGA Data

	if(	texture->imageData==NULL ||							// Does The Storage Memory Exist?
		fread(texture->imageData, 1, imageSize, file)!=imageSize)	// Does The Image Size Match The Memory Reserved?
	{
		if(texture->imageData!=NULL)						// Was Image Data Loaded
			free(texture->imageData);						// If So, Release The Image Data

		fclose(file);										// Close The File
		return false;										// Return False
	}

	for(GLuint i=0; i<int(imageSize); i+=bytesPerPixel)		// Loop Through The Image Data
	{														// Swaps The 1st And 3rd Bytes ('R'ed and 'B'lue)
		temp=texture->imageData[i];							// Temporarily Store The Value At Image Data 'i'
		texture->imageData[i] = texture->imageData[i + 2];	// Set The 1st Byte To The Value Of The 3rd Byte
		texture->imageData[i + 2] = temp;					// Set The 3rd Byte To The Value In 'temp' (1st Byte Value)
	}

	fclose (file);											// Close The File

	// Build A Texture From The Data
	glGenTextures(1, &texture[0].texID);					// Generate OpenGL texture IDs

	glBindTexture(GL_TEXTURE_2D, texture[0].texID);			// Bind Our Texture
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);	// Linear Filtered
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);	// Linear Filtered

	if (texture[0].bpp==24)									// Was The TGA 24 Bits
	{
		type=GL_RGB;										// If So Set The 'type' To GL_RGB
	}

	glTexImage2D(GL_TEXTURE_2D, 0, type, texture[0].width, texture[0].height, 0, type, GL_UNSIGNED_BYTE, texture[0].imageData);

	return true;											// Texture Building Went Ok, Return True
}

bool CMapEditor::GetOverWriteConfirmation (void)
{
	return m_bOverWriteConfirmation;
}

void CMapEditor::SetOverWriteConfirmation (bool OverWriteConfirmation)
{
	this->m_bOverWriteConfirmation = OverWriteConfirmation;
}