/**
 * @file
 * @author Winston Yeu
 * @version 1.0
 */

#pragma once
#include <iostream>
#include <fstream>
#include <String>
#include <GL/glut.h>

using namespace std;

#define MAX 5

struct Score
{
	string name;
	int score;
}; 

/**
 * The Highscore class allows user to save, load into a text file & render highscore.
 */
class Highscore
{
private:
	int Textline;
	int arraypos;

	void *font_style;
public:

	/**
	* Constructor
	*/

	Highscore(void);

	/**
	* Destructor
	*/
	~Highscore(void);
	Score score[10];

	/**
	* Function that save name & score to a text file
	* @param name Save name into file
	* @param score Save score into file
	*/
	void Save(string name, int score);

	/**
	* Function that loads name & score into an array
	*/
	void Load();

	/**
	* Function that writes name & score in array to file
	*/
	void Change();

	/**
	* Function to sort score from highest to lowest
	* @param arraypos Set size to array
	*/
	void Sort(int arraypos);

	/**
	* Function to render highscore
	*/
	void Render();

	/**
	* Function to print words
	*/
	void printw (float x, float y, float z, char* format, ...);
};
