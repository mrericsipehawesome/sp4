#include "Button.h"

Button::Button(void)
: mouseX(0)
, mouseY(0)
, TempColor(0.0f)
, ButtonX(0)
, ButtonY(0)
, TextX(0)
, TextY(0)
, ButtonColor(1)
, Page(0)
, Title("Empty")
, PressedState(false)
, Size(1.0f)
{
}

Button::~Button(void)
{
}

void Button::SetMouse(float x, float y)
{
	mouseX = x;
	mouseY = y;
}

void Button::drawButton()
{
	TopLeftX = ButtonX+5;
	TopLeftY = ButtonY+8;

	BottomRightX = ButtonX+95;
	BottomRightY = ButtonY+50;

	glPushMatrix();
		glTranslatef(ButtonX,ButtonY,0);
		glScalef(Size, Size, Size);
		glPushMatrix();
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glColor4f(ButtonColor,ButtonColor,ButtonColor, alpha);
			glBegin(GL_QUADS);
				glVertex2f(5,50);
				glVertex2f(95,50);
				glVertex2f(95,10);
				glVertex2f(5,10);	
			glEnd();
		glPopMatrix();
		glDisable(GL_BLEND);
		glColor3f(0.7,0.3,0.4);
		printw (TextX, TextY, 0, Title);
	glPopMatrix();
	glColor3f(1,1,1);
}

bool Button::Update()
{
	if ((mouseX > TopLeftX && mouseY > TopLeftY) && (mouseX < BottomRightX && mouseY < BottomRightY))
	{
		ButtonColor = TempColor - 0.5f;
		if (PressedState)
			return true;
	}
	else
		ButtonColor = TempColor;

	return false;
}

void Button::SetSize(float size)
{
	Size = size;
}

void Button::SetButtonPos (float ButtonX, float ButtonY)
{
	this->ButtonX = ButtonX;
	this->ButtonY = ButtonY;
}

void Button::SetTextPos (float TextX, float TextY)
{
	this->TextX = TextX;
	this->TextY = TextY;
}

void Button::SetButtonColor(float ButtonColor, float alpha)
{
	this->ButtonColor = ButtonColor;
	this->alpha = alpha;
	TempColor = ButtonColor;
}

void Button::SetTitle(char* Title)
{
	this->Title = Title;
}

void Button::SetState(bool PressedState)
{
	this->PressedState = PressedState;
}

bool Button::GetState()
{
	return PressedState;
}

void Button::SetPage(int Page)
{
	this->Page = Page;
}

int Button::GetPage()
{
	return Page;
}

int Button::GetPosX()
{
	ButtonX += 40;
	return ButtonX;
}

int Button::GetPosY()
{
	ButtonY += 38;
	return ButtonY;
}

void Button::printw (float x, float y, float z, char* format, ...)
{
	font_style = GLUT_BITMAP_TIMES_ROMAN_24;

	va_list args;	//  Variable argument list
	int len;		//	String length
	int i;			//  Iterator
	char * text;	//	Text

	//  Initialize a variable argument list
	va_start(args, format);

	//  Return the number of characters in the string referenced the list of arguments.
	//  _vscprintf doesn't count terminating '\0' (that's why +1)
	len = _vscprintf(format, args) + 1; 

	//  Allocate memory for a string of the specified size
	text = (char *)malloc(len * sizeof(char));

	//  Write formatted output using a pointer to the list of arguments
	vsprintf_s(text, len, format, args);

	//  End using variable argument list 
	va_end(args);

	//  Specify the raster position for pixel operations.
	glRasterPos3f (x, y, z);


	//  Draw the characters one by one
	for (i = 0; text[i] != '\0'; i++)
		glutBitmapCharacter(font_style, text[i]);

	//  Free the allocated memory for the string
	free(text);
}
