#include "Weapon.h"

Weapon::Weapon(CPlayerInfo *player, int Texture)
{
	Invert = player->GetAnimationInvert();
	Weapon_x = player->GetPos_x();
	Weapon_y = player->GetPos_y() - 22;
	TextureType = Texture;
	LoadTGA( &(WeaponTexture[ 2 ]), "images/knifetexture.tga");
	font_style = GLUT_BITMAP_TIMES_ROMAN_24;
	
}

Weapon::Weapon()
: Weapon_x(0)
, Weapon_y(0)
{
	//score = 0;
}

Weapon::~Weapon()
{
}

void Weapon::SetWeapon(string WeaponName)
{
	this->WeaponName = WeaponName;
}

string Weapon::GetWeapon(void)
{
	return WeaponName;
}

void Weapon::renderWeapon(int mapOffset_x) // render the weapon texture(dagger)
{
	glEnable(GL_TEXTURE_2D);
		glPushMatrix();
		glTranslatef(Weapon_x, Weapon_y, 0);
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
				glBindTexture(GL_TEXTURE_2D, WeaponTexture[TextureType].texID );
				glPushMatrix();
				glBegin(GL_QUADS);
				if (!Invert)
				{
					glTexCoord2f(0,1); glVertex2f(0,0);
					glTexCoord2f(0,0); glVertex2f(0,TILE_SIZE/1.5);
					glTexCoord2f(1,0); glVertex2f(TILE_SIZE/1.5,TILE_SIZE/1.5);
					glTexCoord2f(1,1); glVertex2f(TILE_SIZE/1.5,0);			
				}
				else
				{
					glTexCoord2f(1,1); glVertex2f(0,0);
					glTexCoord2f(1,0); glVertex2f(0,TILE_SIZE/1.5);
					glTexCoord2f(0,0); glVertex2f(TILE_SIZE/1.5,TILE_SIZE/1.5);
					glTexCoord2f(0,1); glVertex2f(TILE_SIZE/1.5,0);		
				}
				glEnd();
			glPopMatrix();
			glDisable(GL_BLEND);
		glPopMatrix();
		glDisable(GL_TEXTURE_2D);
}

bool Weapon::Update(std::vector<Enemies*> &enemylist, int mapOffset_x, int &score) // Updates the score, if dagger hits enemy
{
	if (Invert)
		Weapon_x -= 7;
	else
		Weapon_x += 7;

	for (std::vector<Enemies*>::iterator enemy = enemylist.begin();
		enemy != enemylist.end(); enemy++)
	{
		if (CalculateDistance( (*(*enemy)), Weapon_x + mapOffset_x, Weapon_y) < 2000)
		{
			//Ghost mob
 			if ((*enemy)->GetID() == 1)
			{
 				score += 10;
			}

			//Phanter mob
			if ((*enemy)->GetID() == 2)
			{
				score += 20;
			}

			// Boss
			if ((*enemy)->GetID() == 4)
			{
				if ((*enemy)->GetHit() < 1)
					(*enemy)->SetHit((*enemy)->GetHit() + 1);
				else
				{
					(*enemy)->SetHealth((*enemy)->GetHealth() - 1);
					(*enemy)->SetHit(0);
				}

				if ((*enemy)->GetHealth() <= 0)
				{
					(*enemy)->SetDeath(true);
					score += 100;
					return true;
				}
				(*enemy)->SetHitSomething(true);
				return true;
			}
			(*enemy)->SetDeath(true);
			return true;
		}
	}
}

int Weapon::CalculateDistance(Enemies &enemy, int Weapon_x, int Weapon_y)
{
	return (((enemy.GetPos_x()) - Weapon_x) * ((enemy.GetPos_x()) - Weapon_x) + (enemy.GetPos_y() - Weapon_y) * (enemy.GetPos_y() - Weapon_y));
}

void Weapon::WhipAttack(CPlayerInfo *player, std::vector<Enemies*> &enemylist, int type, int mapOffset_x, int &score) // When killed enemy with whip, update score
{
 	if (player->GetAnimationInvert())
		Weapon_x = player->GetPos_x() - 80;
	else
		Weapon_x = player->GetPos_x() + 80;

	Weapon_y = player->GetPos_y();

	for (std::vector<Enemies*>::iterator enemy = enemylist.begin();
		enemy != enemylist.end(); enemy++)
	{
		if (CalculateDistance( (*(*enemy)), Weapon_x + mapOffset_x, Weapon_y) < 2000)
		{
			//Ghost mob
 			if ((*enemy)->GetID() == 1)
			{
 				score += 10;
			}

			//Phanter mob
			if ((*enemy)->GetID() == 2)
			{
				score += 20;
			}

			if ((*enemy)->GetID() == 3 || (*enemy)->GetID() == 4)
			{
				if ((*enemy)->GetHit() < 1)
					(*enemy)->SetHit((*enemy)->GetHit() + 1);
				else
				{
					(*enemy)->SetHealth((*enemy)->GetHealth() - 1);
					(*enemy)->SetHit(0);
				}

				if ((*enemy)->GetHealth() <= 0)
				{
					score += 100;
					(*enemy)->SetDeath(true);
					break;
				}
				Weapon_x = 0;
				Weapon_y = 0;
				(*enemy)->SetHitSomething(true);
			}
			else
			{
				Weapon_x = 0;
				Weapon_y = 0;
				(*enemy)->SetDeath(true);
				break;
			}
		}
	}
}

void Weapon::printw (float x, float y, float z, char* format, ...)
{
	va_list args;	//  Variable argument list
	int len;		//	String length
	int i;			//  Iterator
	char * text;	//	Text

	//  Initialize a variable argument list
	va_start(args, format);

	//  Return the number of characters in the string referenced the list of arguments.
	//  _vscprintf doesn't count terminating '\0' (that's why +1)
	len = _vscprintf(format, args) + 1; 

	//  Allocate memory for a string of the specified size
	text = (char *)malloc(len * sizeof(char));

	//  Write formatted output using a pointer to the list of arguments
	vsprintf_s(text, len, format, args);

	//  End using variable argument list 
	va_end(args);

	//  Specify the raster position for pixel operations.
	glRasterPos3f (x, y, z);


	//  Draw the characters one by one
	for (i = 0; text[i] != '\0'; i++)
		glutBitmapCharacter(font_style, text[i]);

	//  Free the allocated memory for the string
	free(text);
}

bool Weapon::LoadTGA(TextureImage *texture, char *filename)			// Loads A TGA File Into Memory
{    
	GLubyte		TGAheader[12]={0,0,2,0,0,0,0,0,0,0,0,0};	// Uncompressed TGA Header
	GLubyte		TGAcompare[12];								// Used To Compare TGA Header
	GLubyte		header[6];									// First 6 Useful Bytes From The Header
	GLuint		bytesPerPixel;								// Holds Number Of Bytes Per Pixel Used In The TGA File
	GLuint		imageSize;									// Used To Store The Image Size When Setting Aside Ram
	GLuint		temp;										// Temporary Variable
	GLuint		type=GL_RGBA;								// Set The Default GL Mode To RBGA (32 BPP)

	FILE *file = fopen(filename, "rb");						// Open The TGA File

	if(	file==NULL ||										// Does File Even Exist?
		fread(TGAcompare,1,sizeof(TGAcompare),file)!=sizeof(TGAcompare) ||	// Are There 12 Bytes To Read?
		memcmp(TGAheader,TGAcompare,sizeof(TGAheader))!=0				||	// Does The Header Match What We Want?
		fread(header,1,sizeof(header),file)!=sizeof(header))				// If So Read Next 6 Header Bytes
	{
		if (file == NULL)									// Did The File Even Exist? *Added Jim Strong*
			return false;									// Return False
		else
		{
			fclose(file);									// If Anything Failed, Close The File
			return false;									// Return False
		}
	}

	texture->width  = header[1] * 256 + header[0];			// Determine The TGA Width	(highbyte*256+lowbyte)
	texture->height = header[3] * 256 + header[2];			// Determine The TGA Height	(highbyte*256+lowbyte)

	if(	texture->width	<=0	||								// Is The Width Less Than Or Equal To Zero
		texture->height	<=0	||								// Is The Height Less Than Or Equal To Zero
		(header[4]!=24 && header[4]!=32))					// Is The TGA 24 or 32 Bit?
	{
		fclose(file);										// If Anything Failed, Close The File
		return false;										// Return False
	}

	texture->bpp	= header[4];							// Grab The TGA's Bits Per Pixel (24 or 32)
	bytesPerPixel	= texture->bpp/8;						// Divide By 8 To Get The Bytes Per Pixel
	imageSize		= texture->width*texture->height*bytesPerPixel;	// Calculate The Memory Required For The TGA Data

	texture->imageData=(GLubyte *)malloc(imageSize);		// Reserve Memory To Hold The TGA Data

	if(	texture->imageData==NULL ||							// Does The Storage Memory Exist?
		fread(texture->imageData, 1, imageSize, file)!=imageSize)	// Does The Image Size Match The Memory Reserved?
	{
		if(texture->imageData!=NULL)						// Was Image Data Loaded
			free(texture->imageData);						// If So, Release The Image Data

		fclose(file);										// Close The File
		return false;										// Return False
	}

	for(GLuint i=0; i<int(imageSize); i+=bytesPerPixel)		// Loop Through The Image Data
	{														// Swaps The 1st And 3rd Bytes ('R'ed and 'B'lue)
		temp=texture->imageData[i];							// Temporarily Store The Value At Image Data 'i'
		texture->imageData[i] = texture->imageData[i + 2];	// Set The 1st Byte To The Value Of The 3rd Byte
		texture->imageData[i + 2] = temp;					// Set The 3rd Byte To The Value In 'temp' (1st Byte Value)
	}

	fclose (file);											// Close The File

	// Build A Texture From The Data
	glGenTextures(1, &texture[0].texID);					// Generate OpenGL texture IDs

	glBindTexture(GL_TEXTURE_2D, texture[0].texID);			// Bind Our Texture
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);	// Linear Filtered
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);	// Linear Filtered

	if (texture[0].bpp==24)									// Was The TGA 24 Bits
	{
		type=GL_RGB;										// If So Set The 'type' To GL_RGB
	}

	glTexImage2D(GL_TEXTURE_2D, 0, type, texture[0].width, texture[0].height, 0, type, GL_UNSIGNED_BYTE, texture[0].imageData);

	return true;											// Texture Building Went Ok, Return True
}