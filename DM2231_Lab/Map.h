#pragma once

#include <vector>
#include <fstream>
#include <string>
#include <sstream>
using namespace std;

class CMap
{
public:
	CMap(void);
	~CMap(void);

	void Init(const int theScreen_Height, const int theScreen_Width, 
			  const int theMap_Height, const int theMap_Width, 
			  const int theTileSize);
	bool SaveMap(const string mapName);
	bool LoadMap(const string mapName);
	int getNumOfTiles_ScreenHeight(void);
	int getNumOfTiles_ScreenWidth(void);
	int getNumOfTiles_MapHeight(void);
	int getNumOfTiles_MapWidth(void);
	int getScreenWidth(void);
	void setmapName(string mapname);
	string getmapName(void);

	vector<vector<int> > theScreenMap;

private:
	int theScreen_Height;
	int theScreen_Width;
	int theMap_Height;
	int theMap_Width;
	int theNumOfTiles_ScreenHeight;
	int theNumOfTiles_ScreenWidth;
	int theNumOfTiles_MapHeight;
	int theNumOfTiles_MapWidth;
	int theTileSize;
	string mapName;

	bool SaveFile(const string mapName);
	bool LoadFile(const string mapName);
};
