#include "Timer.h"

CTimer::CTimer(void)
{
	m_iCurrentMin = 5;
	m_bTimerStopped = true;
	//timestart = time (0);
	m_iSec = 0;
	m_iCurrentSec = m_iSec;
}

CTimer::~CTimer(void)
{
	
}

void CTimer::StartTimer ()
{
	m_iSec = m_iCurrentSec + difftime(time(0), timestart);
	m_bTimerStopped = false;
}

void CTimer::StartInvulTimer ()
{
	m_iSec = m_iCurrentSec + difftime(time(0), timestart);
	timestart = time(0);
	m_bTimerStopped = false;
}

void CTimer::StopTimer ()
{
	m_bTimerStopped = true;
}

void CTimer::SetMin (int Min)
{
	this->m_iCurrentMin = Min;
}

void CTimer::SetSec (int Sec)
{
	//this->m_iCurrentSec = m_iSec = Sec;
	m_iSec = Sec;
}

int CTimer::GetMin (void)
{
	return m_iCurrentMin;
}

int CTimer::GetSec (void)
{
	return m_iCurrentSec;
}

void CTimer::Init (int Min, int Sec)
{
	this->m_iCurrentMin = Min;
	m_iSec = Sec;
	m_iCurrentSec = m_iSec;
	timestart = time(0);
}

// Update is called once per frame
bool CTimer::Update ()
{
	if (!m_bTimerStopped)
	{
		m_iCurrentSec = m_iSec - difftime(time(0), timestart);

		if (m_iCurrentSec < 0)
		{
			if(m_iCurrentMin > 0)
			{
				m_iCurrentMin--;
				timestart = time(0);
				m_iSec = 59;
				m_iCurrentSec = m_iSec;
			}
		}
		else if ((m_iCurrentMin < 1) && (m_iCurrentSec < 1))
		{
			return true;
		}
	}

	return false;
}