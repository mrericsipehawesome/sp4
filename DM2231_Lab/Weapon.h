/**
 * @file
 * @author Sherman Lim
 * @version 1.0
 */

#pragma once
#include "Audio.h"
#include "Enemy.h"
#include "PlayerInfo.h"
#include <iostream>
#include <vector>
#include "TextureImage.h"
#include <GL/glut.h>

using namespace std;

#define TILE_SIZE 25

/**
 * The Weapon class allows user to create & use weapon
 */
class Weapon
{
private:
	int Bullets, Magazines;
	string WeaponName;
	void *font_style;

	TextureImage WeaponTexture[3];

	float Weapon_x, Weapon_y;
	float Knife_x, Knife_y;
	bool Invert;
	int TextureType;
	bool Hit;

public:
	/**
	* Constructor
	*/

	Weapon(CPlayerInfo *player, int Texture);

	/**
	* Default Constructor
	*/
	Weapon();

	/**
	* Destructor
	*/
	~Weapon();

	void SetPosX(float Weapon_x) { this->Weapon_x = Weapon_x; }

	void SetPosY(float Weapon_y) { this->Weapon_y = Weapon_y; }
	/**
	* Function to get weapon position x
	* @param Weapon_x Gets weapon position x
	*/
	float GetPosX() { return Weapon_x; }

	/**
	* Function to get weapon position y
	* @param Weapon_y Gets weapon position y
	*/
	float GetPosY() { return Weapon_y; }

	/**
	* Function to set weapon name
	* @param WeaponName Sets weapon name
	*/
	void SetWeapon(string WeaponName);

	/**
	* Function to get weapon name
	*/
	string GetWeapon(void);

	/**
	* Function to update weapon
	* @param enemylist Sets enemy vector list
	* @param mapOffset_x Sets map offset x
	* @param score Update score
	*/
	bool Update(std::vector<Enemies*> &enemylist, int mapOffset_x, int &score);

	/**
	* Function to calculate distance between enemy & weapon
	* @param enemy Sets enemy pos
	* @param Weapon_x Sets weapon position x
	* @param Weapon_y Sets weapon position y
	*/
	int CalculateDistance(Enemies &enemy, int Weapon_x, int Weapon_y);

	/**
	* Function to attack enemy with whip
	* @param player Sets player position
	* @param enemylist Sets enemy vector list
	* @param type Sets weapon type
	* @param mapOffset_x Sets map offset x
	* @param score Update score
	*/
	void WhipAttack(CPlayerInfo *player, std::vector<Enemies*> &enemylist, int type, int mapOffset_x, int &score);

	/**
	* Function to render weapon
	* @param mapOffset_x Sets map offset x
	*/
	void renderWeapon(int mapOffset_x);

	/**
	* Function to print words
	*/
	void printw (float x, float y, float z, char* format, ...);

	/**
	* Function to load TGA file
	*/
	bool LoadTGA(TextureImage *texture, char *filename)	;
};
