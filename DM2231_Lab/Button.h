/**
 * @file
 * @author Winston Yeu
 * @version 1.0
 */

#pragma once
#include <stdio.h>
#include <GL/glut.h>
#include "vector3D.h"
#include <iostream>

using namespace std;
/**
 * The Button class allows player to create and click button.
 */

class Button
{
private:
	float mouseX, mouseY;
	float ButtonX, ButtonY;
	float TopLeftX, TopLeftY, BottomRightX,BottomRightY;
	float TextX, TextY;
	float TempColor, ButtonColor, alpha;
	int Page;
	char* Title;
	bool PressedState;
	float Size;

	void *font_style;

public:
	/**
	* Constructor
	*/
	Button(void);

	/**
	* Destructor
	*/
	~Button(void);

	/**
	* Function to set mouse pos x & y.
	* @param x mouse pos x
	* @param y mouse pos y
	*/
	void SetMouse(float x, float y);

	/**
	* Function to render button.
	*/
	void drawButton();

	/**
	* Function to set button pos.
	* @param ButtonX pos X
	* @param ButtonY Pos Y
	*/
	void SetButtonPos (float ButtonX, float ButtonY);

	/**
	* Function to set text pos.
	* @param TextX pos X
	* @param TextY Pos Y
	*/
	void SetTextPos (float TextX, float TextY);

	/**
	* Function to set button color.
	* @param ButtonColor color from 0.0f to 1.0f
	*/
	void SetButtonColor(float ButtonColor, float alpha);

	/**
	* Function to set title.
	* @param Title title to show
	*/
	void SetTitle (char* Title);

	/**
	* Function to set bool when clicked.
	* @param PressedState bool to check if clicked is true or false
	*/
	void SetState(bool PressedState);

	/**
	* Function to get mouse click bool.
	*/
	bool GetState();

	/**
	* Function to set page.
	* @param Page page to set
	*/
	void SetPage(int Page);

	/**
	* Function to get page.
	*/
	int GetPage();
	
	/**
	* Function to move mouse.
	*/
	bool Update();

	void SetSize(float size);

	int GetPosX();
	int GetPosY();
	void printw (float x, float y, float z, char* format, ...);
};
