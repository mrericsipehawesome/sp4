/**
 * @file
 * @author Ryan Tay
 * @version 1.4
 */

#pragma once
#include <stdio.h>
#include "../include/irrKlang.h"
#include <ik_ISoundStopEventReceiver.h>
#include <vector>
#include <iostream>
#include <string>

using namespace std;
using namespace irrklang;

/**
 * The Audio class allows audios like background music or sound effects to be played.
 */

class CAudio
{
private:
	bool m_bPauseAudio;

	static CAudio *s_pInstance;

	int m_iAudioToBePlayed;

	ISound* Sound_BGM;
	ISound* Sound_SFX;

	string BGM[5];
	string SFX[6];

	
public:

	ISoundEngine* theSoundEngine;

	/**
	* Constructor
	*/
	CAudio(void);
	
	/**
	* Destructor
	*/
	~CAudio(void);

	static CAudio* getInstance();

	/**
	* Function that Starts all the Audio
	*/
	void StartAudio (void);

	/**
	* Function that pauses all the Audio
	*/
	void PauseAudio (void);

	/**
	* Function that Stops all the Audio
	*/
	void StopAudio (void);

	/**
	* Function that Initialises all the varibles
	*/
	bool Init (void);

	/**
	* Function that Plays the Background Music
	*/
	void PlayBGM (int WhatBGMToPlay);

	void PlaySFX (int WhatSFXToPlay);
};