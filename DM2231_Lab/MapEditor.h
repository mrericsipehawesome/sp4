/**
 * @file
 * @author Ryan Tay
 * @version 1.4
 */

 /**
 * The Map Editor class allows users to modify the csv files that creats the levels.
 */
#pragma once
#include "TextureImage.h"
#include "vector3D.h"
#include "Enemy.h"
#include <vector>
#include <iostream>
#include <fstream>

using namespace std;

class CMapEditor
{
private:
	TextureImage TileTexture[32];
	TextureImage MapEditorButtonsTexture[10];

	// if mouse is pressed
	bool m_bMouseClick;

	// Which tile is selected
	int m_iTileSelected;

	// Get the number of tiles for width of the screen and height of the screen, grid map
	int m_iTheNumOfTiles_GridWidth, m_iTheNumOfTiles_GridHeight;

	// Get the number of tiles for width and height of the screen, editor map
	int m_iTheNumOfTiles_EditorWidth, m_iTheNumOfTiles_EditorHeight;

	// Get the number of tiles for width of the map
	int m_iTheNumOfTiles_MapWidth;

	// Tile Offset, Map Fine OffSet and Map OffSet for x axis
	int m_iMapTileOffSet_x, m_iMapFineOffSet_x, m_iMapOffSet_x;

	// Variables to contain the mouse position
	int m_iMouseX, m_iMouseY;

	// Variables to contain the mouse position using ratio
	int m_iMouseLastX, m_iMouseLastY;

	// Variables to contain the mouse Position in tile form
	int m_iMouseTilePosX, m_iMouseTilePosY;

	// Grid map
	vector<vector<int>> theGridMap;
	// Map being edited
	vector<vector<int>> theEditorMap;

	int m_iTextLine;

	int m_iMouseOnButton;

	bool m_bSavingMap;

	bool m_bOverWriteConfirmation;

public:

	/**
	 * Constructor
	 */
	CMapEditor(void);

	/**
	 * Destructor
	 */
	~CMapEditor(void);

	/**
	 * Function that renders grids
	 */
	void RenderGrid (void);

	/**
	 * Function that renders the map
	 */
	void RenderMap (void);

	/**
	 * Function that renders map editor menu
	 */
	void RenderMapEditorMenu (void);

	/**
	 * Function that gets which Tile has already been selected for placement
	 *
	 * @return Returns the value of TileSelected
	 */
	int GetTileSelected (void);

	/**
	 * Function that returns the value of m_bMouseClick
	 *
	 * @return Returns the value of m_bMouseClick
	 */
	bool GetMouseClick (void);

	/**
	 * Function that returns an integer value to see where the mouse is at
	 * @return Returns the value of m_iMouseOnButton
	 */
	int GetMouseOnButton (void);

	/**
	 * Function
	 * @param MouseOnButton Sets the value of m_iMouseOnButton
	 */
	void SetMouseOnButton (int MouseOnButton);

	/**
	 * Function that returns the value of m_bSavingMap
	 * @return Returns the value of m_bSavingMap
	 */
	bool GetSavingMap (void);

	/**
	 * Function that Sets m_iMouseLastX and m_iMouseLastY;
	 *
	 */
	void SetMouseLast (void);

	/**
	 * Function that Sets the mouse position in tile form
	 *
	 */
	void SetMouseTilePos (void);

	/**
	 * Function that sets the Tile offset on the x axis by a value
	 *
	 * @param MapTileOffSet_x Sets the value of MapTileOffSet_x
	 */
	void SetMaptileOffSet(int MapTileOffSet_x);

	/**
	 * Function that sets the Fine offset on the x axis by a value
	 *
	 * @param MapFineOffSet_x Sets the value of MapFineOffSet_x
	 */
	void SetMapFineOffSet(int MapFineOffSet_x);
	
	/**
	 * Function that sets the map offset on the x axis by a value
	 *
	 * @param MapOffSet_x Sets the value of MapOffSet_x
	 */
	void SetMapOffSet (int MapOffSet_x);

	/**
	 * Function that Initialises all the varibles
	 *
	 * @param TileTexture Sets the values of TileTexture
	 * @param MaptileOffSet_x Sets the value of m_iMapTileOffSet_x
	 * @param MapOffSet_x Sets the value of m_iMapOffSet_x
	 * @param theNumOfTiles_GridWidth Sets the value of m_iTheNumOfTiles_GridWidth
	 * @param theNumOfTiles_GridHeight Sets the value of m_iTheNumOfTiles_GridHeight
	 * @param theNumOfTiles_EditorWidth Sets the value of m_iTheNumOfTiles_EditorWidth
	 * @param theNumOfTiles_EditorHeight Sets the value of m_iTheNumOfTiles_EditorHeight
	 * @param theNumOfTiles_MapEditorWidth Sets the value of m_iTheNumOfTiles_MapWidth
	 * @param theGridMap Sets the value of theGridMap
	 */
	void Init(TextureImage TileTexture[], 
					   int MaptileOffSet_x, int MapOffSet_x,
					   int theNumOfTiles_GridWidth, int theNumOfTiles_GridHeight,
					   int theNumOfTiles_EditorWidth, int theNumOfTiles_EditorHeight,
					   int theNumOfTiles_MapEditorWidth, vector<vector<int>> theGridMap,
					   vector<vector<int>> theEditorMap);

	/**
	 * Function that chooses what m_iTileSelected is
	 *
	 * @param TileSelected Sets the value of m_iTileSelected
	 */
	void SetTileSelected (int TileSelected);

	/**
	 * Function that sets m_bMouseClick
	 *
	 * @param MouseClick Sets the value of m_bMouseClick
	 */
	void SetMouseClick (bool MouseClick);

	/**
	 * Function that sets m_bSavingMap
	 * @param SaveMap Sets the value of m_bSavingMap
	 */
	void SetSavingMap (bool SaveMap);

	/**
	 * Function that Updates the location of the mouse
	 *
	 * @param MouseX Sets the value of m_iMouseX
	 * @param MouseY Sets the value of m_iMouseY
	 * @param MapOffSet_x Sets the value of m_iMapOffSet_x
	 */
	void Update (int MouseX, int MouseY, int MapOffSet_x);

	/**
	 * Function that chooses what m_iTileSelected is
	 *
	 * @param MapFineOffSet_X Sets the value of m_iMapFineOffSet_X
	 * @param MapOffSet_X Sets the value of m_iMapOffSet_X
	 */
	void RenderTileMapEditor (int MapFineOffSet_X, int MapOffSet_X);

	/**
	 * Function that sets the tile to what tile the user has selected for m_iTileSelected
	 */
	void MapEditorTileMouse(void);

	/**
	 * Function that sets m_iTileSelected to what the user wants
	 */
	void MapEditorMenuMouse(void);

	/**
	 * Function that saves the map by overriding the csv using pass by reference
	 * @param SaveTo Is passed in a vector to be changed
	 */
	void SaveMap (vector<vector<int>> *SaveTo);

	/**
	 * Function that Sets the textures by using pass by reference
	 * @return Returns a boolean
	 * @param texture Pass by reference to take in 
	 * @param filename Takes in the location and name of the Texture
	 */
	bool LoadTGA(TextureImage *texture, char *filename);

	/**
	 * Function that Returns the value of m_bOverWriteConfirmation
	 * @return Returns the boolean value of m_bOverWriteConfirmation
	 */
	bool GetOverWriteConfirmation (void);

	/**
	 * Function that sets the value of m_bOverWriteConfirmation
	 * @param OverWriteConfirmation Sets boolean value of m_bOverWriteConfirmation
	 */
	void SetOverWriteConfirmation (bool OverWriteConfirmation);
};