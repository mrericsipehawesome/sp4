/**
 * @file
 * @author Sherman Lim
 * @version 1.0
 */

#pragma once
#include "TextureImage.h"
#include "Timer.h"
#include "Map.h"
#include <iostream>
#include <string>
#include <fstream>
#include <strstream>

using namespace std;

/**
 * The PlayInfo class allows the character to render, have animation and have healthpoints
 */
class CPlayerInfo
{
public:
		/**
		* Constructor
		*/
		CPlayerInfo(float locx_, float locy_, int id);

		/**
		* Default Constructor
		*/
		CPlayerInfo(void);

		/**
		* Destructor
		*/
		~CPlayerInfo(void);

		// Hero's information
		TextureImage HeroTexture[4];
		TextureImage EnemyTexture[1];

		/**
		* Function that render the Hero onto the screen
		*/
		void RenderHero(void);

		/**
		* Function that initialise this class instance
		* @param PosX sets the position x of hero
		* @param PosY sets the position y of hero
		*/
		void Init(int PosX, int PosY);

		/**
		* Function that returns true if the player is on ground
		*/
		bool isOnGround(void);

		/**
		* Function that returns true if the player is jumping upwards
		*/
		bool isJumpUpwards(void);

		/**
		* Function that returns true if the player is on freefall
		*/	
		bool isFreeFall(void);

		/**
		* Function that set the player's status to free fall mode
		*/
		void SetOnFreeFall(bool isOnFreeFall);

		/**
		* Function that set the player to jumping upwards
		*/
		void SetToJumpUpwards(bool isOnJumpUpwards);

		/**
		* Function that stop the player's movement
		*/
		void SetToStop(void);

		/**
		* Function that set position x of the player
		* @param pos_x 
		*/
		void SetPos_x(float pos_x);

		/**
		* Function that set position y of the player
		* @param pos_y
		*/
		void SetPos_y(float pos_y);


		/**
		* Function that set jumpspeed of the player
		* @param jumpspeed sets the value of speed
		*/
		void SetJumpspeed(int jumpspeed);

		/**
		* Function that get position x of the player
		* @return Returns the value of position x
		*/
		float GetPos_x(void);
		
		/**
		* Function that get position y of the player
		* @return Returns the value of position y
		*/
		float GetPos_y(void);
		
		/**
		* Function that get jumpspeed of the player
		* @return Returns the value of jump speed
		*/
		int GetJumpspeed(void);

		/**
		* Function that set Map offset x
		* @return sets the value of map offset x
		*/
		void SetMapOffset(int mapOffset_x);

		/**
		* Function that get map offset x
		* @return Returns the value of map offset x
		*/
		int GetMapOffset();

		/**
		* Function that updates character jump upwards
		*/
		void UpdateJumpUpwards();

		/**
		* Function that update free fall of character
		* param height sets the height
		*/
		void UpdateFreeFall(int height);

		/**
		* Function that set Animation Invert status of the player
		*/
		void SetAnimationInvert(bool heroAnimationInvert);

		/**
		* Function that get Animation Invert status of the player
		* @return Returns the animation invert
		*/
		bool GetAnimationInvert(void);

		
		/**
		* Function that set Animation Counter of the player
		*/
		void SetAnimationCounter(int heroAnimationCounter);
	
		/**
		* Function that get Animation Counter of the player
		*/
		int GetAnimationCounter(void);

		void SetMovingAnimation(int Animation) { MovingAnimation = Animation; }
		int GetMovingAnimation() { return MovingAnimation; }

		void SetTexCoordX(float TexCoord) { TexCoordX = TexCoord; }
		float GetTexCoordX() { return TexCoordX; }

		void SetCounterX(float CounterX) { this->CounterX = CounterX; }
		float GetCounterX() { return CounterX; }

		/**
		* Function that set tilesize x 
		* @param Tile_Size sets the value of size X
		*/
		void SetTileSizeX(int Tile_Size) { Tile_SizeX = Tile_Size; }

		/**
		* Function that set tilesize y
		* @param Tile_Size sets the value of size Y
		*/
		void SetTileSizeY(int Tile_Size) { Tile_SizeY = Tile_Size; }

		/**
		* Function that get tilesize x
		* @param Tile_Sizex get the value of size x
		*/
		float GetTileSizeX() { return Tile_SizeX; }

		/**
		* Function that get tilesize y
		* @param Tile_Size get the value of size Y
		*/
		float GetTileSizeY() { return Tile_SizeY; }

		void Animate(int type);

		void SetTextureType(int TextureType) { this->TextureType = TextureType; }
		int GetTextureType() { return TextureType; }

		void SetAnimationStop(bool stop) { StopAnimation = stop; }
		bool GetAnimationStop() { return StopAnimation; }

		void SetAnimation(bool StartAnimation) { this->StartAnimation = StartAnimation; }
		bool GetAnimation() { return StartAnimation; }

		void SetCounter(int counter) { Counter = counter; }
		int GetCounter() { return Counter; }

		void SetKeyDown(char key, bool keydown);
		bool GetKeyDown(char key);
		
		/**
		* Function that calculate distance of player and enemy
		* @param enemy_x gets x position of enemy
		* @param enemy_y gets y position of enemy
		* @param mapOffset_x gets the map offset x
		*/
		int CalculateDistance(const float enemy_x, const float enemy_y, const int mapOffset_x);
		
		/**
		* Function that give player invulnerability for 5 second when attacked by enemy
		*/
		void Invulnerability();

		/**
		* Function that set the health of the character
		* @param lives sets the value of health
		*/
		void SetHealth(int health) { this->Health = health; }

		/**
		* Function that get the health of the character
		* @param lives gets the value of health
		*/
		int GetHealth() { return Health; }

		
		void SetHit(bool hit) { Hit = hit; }
		bool GetHit() { return Hit; }

		void SetInvulnerable(bool invulnerable) { this->invulnerable = invulnerable; }
		bool GetInvulnerable() { return invulnerable; }

		void WeaponAnimation();
		void AnimateDeath();


		void ResetAnimation();

		/**
		* Function that constrain hero from all border
		@param leftBorder sets the border at the left side
		@param rightBorder sets the border at the right side
		@param topBorder sets the border at the top side
		@param bottomBorder sets the border at the bottom side
		@param timeDiff sets the border at the left side
		@param mapOffset_x sets map offset x
		@param mapOffset_y sets map offset y
		*/
		void ConstrainHero( const int leftBorder, const int rightBorder,
							const int topBorder, const int bottomBorder,
							float timeDiff,
							int& mapOffset_x, int& mapOffset_y);

		/**
		* Function that save
		*/
		void Saving();

		/**
		* Function that load
		*/
		void Loading();

		/**
		* Function that load tga file
		*/
		bool LoadTGA(TextureImage *texture, char *filename);

		// RakNet
		void setID(unsigned int id) { this->id = id; }
		unsigned int GetID() { return id; }
		void SetName(const char * text) { Playername = text; };
		const char * GetName() { return Playername.c_str(); };

		bool move;
		bool render;
		CTimer timer;

	private:
		int m_iTileSize;
		int LoadingLine;
		int mapOffset_x;

		CMap* theMap;

		// Hero's information
		float hero_x, hero_y;
		int jumpspeed;
		bool hero_inMidAir_Up;
		bool hero_inMidAir_Down;
		bool heroAnimationInvert;
		int heroAnimationCounter;
		bool dead;
		int lives;
		int Points;
		
		// NEW

		unsigned int id;
		string Playername;
		bool Animation;
		int MovingAnimation;

		float TexCoordX, CounterX;
		int Tile_SizeX, Tile_SizeY;
		int weapontype, TextureType;
		float TranslateX, TranslateY;
		bool StopAnimation, StartAnimation;
		int Counter;

		bool keydown_q, keydown_p;
		bool keydown_e, keydown_u;

		int Health;
		bool Hit, invulnerable;
		float blink;
		bool Blinking;
};
