#include "mainmenu.h"
#include "gl/freeglut.h"
#include <sstream>


mainmenu::mainmenu(void)
: blink(1.0f)
, Blinking(false)
, oldTimeSinceStart(0)
, timeSinceStart(0)
, deltaTime(0)
{
}

mainmenu::~mainmenu(void)
{
}

void mainmenu::orthogonalStart()
{
	glMatrixMode(GL_PROJECTION);		// Switch to projection mode.

	glPushMatrix();		// Save previous matrix which contains the settings for the perspective projection.
	glLoadIdentity();		// Reset matrix.
	gluOrtho2D(0, 800, 0, 600);		// Set a 2D orthographic projection.
	glScalef(1, -1, 1);		// Invert the y axis, down is positive.
	glTranslatef(0, -600, 0);		// Move the origin from the bottom left corner to the upper left corner.
	glMatrixMode(GL_MODELVIEW);		// Switch back to modelview mode.
	glPushMatrix();

	glLoadIdentity();
}

// end the projection mode
void mainmenu::orthogonalEnd()
{
	glPopMatrix();
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
}

// change int to string
string mainmenu::intToString(int a)
{
	ostringstream s;
	s << a;
	return s.str();
}

void mainmenu::menulogo()
{
	glPushMatrix();
	glEnable( GL_TEXTURE_2D );
	glBindTexture(GL_TEXTURE_2D,menu[1].texID);
	glColor3f(1.0,1.0,1.0);
	glBegin (GL_QUADS);
	glTexCoord2f (0, 0); glVertex2f (0, 600);
	glTexCoord2f (0, 1); glVertex2f (0, 0);
	glTexCoord2f (1, 1); glVertex2f (800, 0);
	glTexCoord2f (1, 0); glVertex2f (800, 600);
	glEnd ();
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
}


void mainmenu::menustart()
{
	timeSinceStart = glutGet(GLUT_ELAPSED_TIME);
    deltaTime = timeSinceStart - oldTimeSinceStart;
    oldTimeSinceStart = timeSinceStart;

	if (!Blinking)
		blink-=0.0001f * deltaTime;
	else
		blink+=0.0001f * deltaTime;

	if (blink <= 0.0f && !Blinking)
		Blinking = true;
	else if (blink >= 1.0f && Blinking)
		Blinking = false;

	glPushMatrix();
	glEnable( GL_TEXTURE_2D );
	glEnable( GL_BLEND );
	glBindTexture(GL_TEXTURE_2D,menu[0].texID);
	glTranslatef(0,100,0);
	glColor4f(1.0f, 1.0f, 1.0f, blink); 
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glBegin (GL_QUADS);
	glTexCoord2f (0, 0); glVertex2f (275, 300);
	glTexCoord2f (0, 1); glVertex2f (275, 250);
	glTexCoord2f (1, 1); glVertex2f (525, 250);
	glTexCoord2f (1, 0); glVertex2f (525, 300);
	glEnd ();
	glDisable( GL_BLEND ); 
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
}

void mainmenu::menuscore()
{
	if (!Blinking)
		blink-=0.0001f * deltaTime;
	else
		blink+=0.0001f * deltaTime;

	if (blink <= 0.0f && !Blinking)
		Blinking = true;
	else if (blink >= 1.0f && Blinking)
		Blinking = false;

	glPushMatrix();
	glEnable( GL_TEXTURE_2D );
	glEnable( GL_BLEND );
	glBindTexture(GL_TEXTURE_2D,menu[4].texID);
	glTranslatef(0, 40, 0);
	glColor4f(1.0f, 1.0f, 1.0f, blink); 
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glBegin (GL_QUADS);
	glTexCoord2f (0, 0); glVertex2f (275, 420);
	glTexCoord2f (0, 1); glVertex2f (275, 370);
	glTexCoord2f (1, 1); glVertex2f (525, 370);
	glTexCoord2f (1, 0); glVertex2f (525, 420);
	glEnd ();
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
	glColor3f(1,1,1);
}

void mainmenu::singleplayer()
{
	glPushMatrix();
	glEnable( GL_TEXTURE_2D );
	glBindTexture(GL_TEXTURE_2D,menu[6].texID);
	glColor3f(1.0,1.0,1.0);
	glBegin (GL_QUADS);
	glTexCoord2f (0, 0); glVertex2f (275, 420);
	glTexCoord2f (0, 1); glVertex2f (275, 370);
	glTexCoord2f (1, 1); glVertex2f (525, 370);
	glTexCoord2f (1, 0); glVertex2f (525, 420);
	glEnd ();
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
}

void mainmenu::multiplayer()
{
	glPushMatrix();
	glEnable( GL_TEXTURE_2D );
	glBindTexture(GL_TEXTURE_2D,menu[7].texID);
	glColor3f(1.0,1.0,1.0);
	glBegin (GL_QUADS);
	glTexCoord2f (0, 0); glVertex2f (275, 480);
	glTexCoord2f (0, 1); glVertex2f (275, 430);
	glTexCoord2f (1, 1); glVertex2f (525, 430);
	glTexCoord2f (1, 0); glVertex2f (525, 480);
	glEnd ();
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
}

void mainmenu::load()
{
	glPushMatrix();
	glEnable( GL_TEXTURE_2D );
	glBindTexture(GL_TEXTURE_2D,menu[8].texID);
	glTranslatef(0,60,0);
	glColor3f(1.0,1.0,1.0);
	glBegin (GL_QUADS);
	glTexCoord2f (0, 0); glVertex2f (275, 480);
	glTexCoord2f (0, 1); glVertex2f (275, 430);
	glTexCoord2f (1, 1); glVertex2f (525, 430);
	glTexCoord2f (1, 0); glVertex2f (525, 480);
	glEnd ();
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
}

void mainmenu::mapeditor()
{
	if (!Blinking)
		blink-=0.01f;
	else
		blink+=0.01f;

	if (blink <= 0.0f && !Blinking)
		Blinking = true;
	else if (blink >= 1.0f && Blinking)
		Blinking = false;

	glPushMatrix();
	glEnable( GL_TEXTURE_2D );
	glEnable( GL_BLEND );
	glBindTexture(GL_TEXTURE_2D,menu[9].texID);
	glTranslatef(-5,40,0);
	glColor4f(1.0f, 1.0f, 1.0f, blink); 
	glBegin (GL_QUADS);
	glTexCoord2f (0, 0); glVertex2f (275, 480);
	glTexCoord2f (0, 1); glVertex2f (275, 430);
	glTexCoord2f (1, 1); glVertex2f (525, 430);
	glTexCoord2f (1, 0); glVertex2f (525, 480);
	glEnd ();
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
	glColor3f(1,1,1);
}
