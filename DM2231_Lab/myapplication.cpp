#include "myapplication.h"
#include <mmsystem.h>
#include <irrKlang.h>
#include "mainmenu.h"

using namespace irrklang;

#define LEVEL "level1.csv"

#define MMENU 0
#define INGAME 1
#define HIGHSCORE 2
#define LOADGAME 3
#define DEATHSCREEN 4
#define START 5 
#define ENDING 6
#define CONTROL 7
#define MAPEDITOR 8
#define LEVELSELECT 9
#define WIN 10
#define EXITWINDOW 11

#define RIGHT 0
#define LEFT 1
#define DOWN 2
#define LEFTMOVE 3
#define RIGHTMOVE 4

#define ONEPLAYER 1
#define TWOPLAYER 2

int gamemode = MMENU;

#pragma comment(lib, "irrKlang.lib")
myApplication * myApplication::s_pInstance = NULL;

// you can use constructor to initialise variables
myApplication::myApplication()
: theCamera(NULL)
, theMap(NULL)
, theRear(NULL)
, theMiddle(NULL)
, theHero(NULL)
, timer_( 0 )
, Delay( 0 )
, Counter( 0 )
, Spawn( false )
, keydown_q(false)
, keydown_p(false)
, keydown_e(false)
, keydown_u(false)
, counter (0)
, playerno(0)
, BossSpawn(false)
, score(0)
, Animation(false)
, players(ONEPLAYER)
, SetPlayer(false)
, MouseUp(true)
, Distance(0)
, Load(false)
, blink(1.0f)
, Blinking(false)
, AutoSave(false)
, AutoSaveTime(0.0f)
, StageClear(false)
, BallAnimationCounter(0)
, BallMovingAnimation(0)
, KnifeX(395)
, KnifeY(150)
, Loaded(false)
, DoorMove(false)
, DoorTexture(10)
, DoorWait(0.0f)
, coin(0)
, TILESIZE(25)
, timeSinceStart(0)
, deltaTime(0)
, oldTimeSinceStart(0)
, keypressed(false)
, scroll(0.0f)
, startpressed(false)
{
	srand(time(NULL));
}

myApplication::~myApplication()
{
	delete theArrayOfGoodies;
	theArrayOfGoodies = NULL;

	if (theMap != NULL)
	{
		delete theMap;
		theMap = NULL;
	}
	if (theRear != NULL)
	{
		delete theRear;
		theRear = NULL;
	}
	if (theMiddle != NULL)
	{
		delete theMiddle;
		theMiddle = NULL;
	}
	if (theCamera != NULL)
	{
		delete theCamera;
		theCamera = NULL;
	}
	if (Mainmenu != NULL)
	{
		delete Mainmenu;
		Mainmenu = NULL;
	}
}

myApplication* myApplication::getInstance()
{
	if(s_pInstance == NULL)
	{
		s_pInstance = new myApplication();
	}

	return s_pInstance;
}


bool myApplication::Init(void)
{
	Audio = new CAudio();
	Audio->Init();

	Audio->PlayBGM(0);

	// Set camera position
	theCamera = new Camera( Camera::LAND_CAM );
	theCamera->SetPosition( 0.0, 2.0, -5.0 );
	theCamera->SetDirection( 0.0, 0.0, 1.0 );

	//glEnable(GL_TEXTURE_2D);							// Enable Texture Mapping ( NEW )
	LoadTGA( &BackgroundTexture[0], "images/level1.tga");
	LoadTGA( &BackgroundTexture[1], "images/death.tga");
	LoadTGA( &BackgroundTexture[2], "images/Background2.tga");
	LoadTGA( &BackgroundTexture[3], "images/HighscoreBackground.tga");
	LoadTGA( &BackgroundTexture[4], "images/level2.tga");
	LoadTGA( &BackgroundTexture[5], "images/level3.tga");
	LoadTGA( &BackgroundTexture[6], "images/level4.tga");
	LoadTGA( &BackgroundTexture[7], "images/gamecontrols.tga");
	LoadTGA( &BackgroundTexture[8], "images/autosave.tga");
	LoadTGA( &BackgroundTexture[9], "images/Ball.tga");
	LoadTGA( &BackgroundTexture[10], "images/door1.tga");
	LoadTGA( &BackgroundTexture[11], "images/door2.tga");
	LoadTGA( &BackgroundTexture[12], "images/sea.tga");
	LoadTGA( &BackgroundTexture[13], "images/end.tga");
	LoadTGA( &BackgroundTexture[14], "images/castle.tga");
	LoadTGA( &TileMapTexture[ 0 ], "images/tile0_blank.tga");
	LoadTGA( &UITexture[0], "images/bullet.tga");
	LoadTGA( &UITexture[1], "images/coin.tga");
	LoadTGA( &UITexture[2], "images/shotgun.tga");
	LoadTGA( &UITexture[3], "images/knifetexture.tga");
	LoadTGA( &TileMapTexture[ 1 ], "images/dirt.tga");
	LoadTGA( &TileMapTexture[ 2 ], "images/level1tile.tga");
	LoadTGA( &TileMapTexture[ 3 ], "images/statuehead.tga");
	LoadTGA( &TileMapTexture[ 4 ], "images/statuebody.tga");
	LoadTGA( &TileMapTexture[ 5 ], "images/fence.tga");
	LoadTGA( &TileMapTexture[ 6 ], "images/treetopleft.tga");
	LoadTGA( &TileMapTexture[ 7 ], "images/treetopright.tga");
	LoadTGA( &TileMapTexture[ 8 ], "images/treebottomleft.tga");
	LoadTGA( &TileMapTexture[ 9 ], "images/treebottomright.tga");
	LoadTGA( &TileMapTexture[ 10 ], "images/firestatue.tga");
	LoadTGA( &TileMapTexture[ 11 ], "images/towertop.tga");
	LoadTGA( &TileMapTexture[ 12 ], "images/towermid.tga");
	LoadTGA( &TileMapTexture[ 13 ], "images/towerbtm.tga");
	LoadTGA( &TileMapTexture[ 14 ], "images/stairs.tga");
	LoadTGA( &TileMapTexture[ 15 ], "images/firestatue1.tga");
	//
	LoadTGA( &TileMapTexture[ 17 ], "images/candle.tga");
	LoadTGA( &TileMapTexture[ 18 ], "images/candle1.tga");
	LoadTGA( &TileMapTexture[ 19 ], "images/stairs1.tga");
	LoadTGA( &TileMapTexture[ 20 ], "images/halffence.tga");
	LoadTGA( &TileMapTexture[ 21 ], "images/halffence1.tga");
	LoadTGA( &TileMapTexture[ 22 ], "images/black.tga");
	LoadTGA( &TileMapTexture[ 23 ], "images/black1.tga");
	LoadTGA( &TileMapTexture[ 24 ], "images/brick.tga");
	LoadTGA( &TileMapTexture[ 25 ], "images/waterfence.tga");
	LoadTGA( &TileMapTexture[ 26 ], "images/grasssmall.tga");
	LoadTGA( &TileMapTexture[ 27 ], "images/grassbig.tga");
	LoadTGA( &TileMapTexture[ 28 ], "images/grasssmalldown.tga");
	LoadTGA( &TileMapTexture[ 29 ], "images/grassbigdown.tga");
	LoadTGA( &TileMapTexture[ 30 ], "images/portal.tga");
	LoadTGA( &TileMapTexture[ 31 ], "images/grid.tga");
	LoadTGA( &TileMapTexture[ 32 ], "images/coin.tga");
	LoadTGA( &TileMapTexture[ 33 ], "images/heart.tga");
	//Smoke
	LoadTGA( &smokeTexture[ 0 ], "images/smoke.tga");

	Mainmenu = new mainmenu();
	LoadTGA( &(Mainmenu->menu[0]), "images/start.tga");
	LoadTGA( &(Mainmenu->menu[1]), "images/gamelogo.tga");
	LoadTGA( &(Mainmenu->menu[2]), "images/start.tga");
	LoadTGA( &(Mainmenu->menu[3]), "images/loadgame.tga");
	LoadTGA( &(Mainmenu->menu[4]), "images/highscore.tga");
	LoadTGA( &(Mainmenu->menu[5]), "images/quitgame.tga");
	LoadTGA( &(Mainmenu->menu[6]), "images/single.tga");
	LoadTGA( &(Mainmenu->menu[7]), "images/multiplayer.tga");
	LoadTGA( &(Mainmenu->menu[8]), "images/load.tga");
	LoadTGA( &(Mainmenu->menu[9]), "images/map.tga");

	// Set up the map
	theMap = new CMap(); 
	theMap->Init( 600, 800, 600, 1600, TILE_SIZE );
	theMap->LoadMap( LEVEL );
	theMap->setmapName( LEVEL );
	theNumOfTiles_Height = theMap->getNumOfTiles_ScreenHeight();
	theNumOfTiles_Width = theMap->getNumOfTiles_ScreenWidth();
	
	theRear = new CMap();
	theRear->Init( 600, 800, 600, 1600, TILE_SIZE );
	theRear->LoadMap( "RearDesign.csv" );
	theNumOfTiles_RearHeight = theRear->getNumOfTiles_ScreenHeight();
	theNumOfTiles_RearWidth = theRear->getNumOfTiles_ScreenWidth();

	theMiddle = new CMap();
	theMiddle->Init( 600, 800, 600, 1600, TILE_SIZE );
	theMiddle->LoadMap( "MiddleDesign.csv" );
	theNumOfTiles_MiddleHeight = theMiddle->getNumOfTiles_ScreenHeight();
	theNumOfTiles_MiddleWidth = theMiddle->getNumOfTiles_ScreenWidth();

	theGrid = new CMap();
	theGrid->Init(600, 800, 600, 1600, TILE_SIZE);

	//boolean
	coindisappear = false;
	disappear = false;
	stop = false;
	NextLevel = false;
	shootfire = false;
	firepos = false;
	holdkey = false;
	turn = false;
	smoke = false;

	//  The number of frames
	frameCount = 0;
	//  Number of frames per second
	fps = 0;
	//  currentTime - previousTime is the time elapsed
	//  between every call of the Idle function
	currentTime = 0, previousTime = 0;
	//  Pointer to a font style..
	//  Fonts supported by GLUT are: GLUT_BITMAP_8_BY_13, 
	//  GLUT_BITMAP_9_BY_15, GLUT_BITMAP_TIMES_ROMAN_10, 
	//  GLUT_BITMAP_TIMES_ROMAN_24, GLUT_BITMAP_HELVETICA_10,
	//  GLUT_BITMAP_HELVETICA_12, and GLUT_BITMAP_HELVETICA_18.
	//GLvoid *font_style = GLUT_BITMAP_TIMES_ROMAN_24;
	font_style = GLUT_BITMAP_TIMES_ROMAN_24;

	// realtime loop control
	timelastcall=timeGetTime();

	frequency = 50.0f;

	for(int i=0; i<255; i++){
		myKeys[i] = false;
	}
	angle = 0.0f;
	mouseInfo.lastX = 800 >> 1;
	mouseInfo.lastY = 600 >> 1;

	mapOffset_x = 0;
	mapOffset_y = 0;
	tileOffset_x = 0;
	tileOffset_y = 0;
	mapFineOffset_x = 0;
	mapFineOffset_y = 0;


	//Bullet
	theArrayOfGoodies = new CGoodies;
		theArrayOfGoodies = theGoodiesFactory.Create( COIN );
		theArrayOfGoodies->SetPos( 10, -50 );
		LoadTGA( &(theArrayOfGoodies->GoodiesTexture[0]), "images/tile4_coin.tga");

	//Health
	theArrayOfHealth = new CGoodies*[10];
	for (int i = 0; i < 10; i++)
	{
		theArrayOfHealth[i] = theGoodiesFactory.Create(HEALTH);
		theArrayOfHealth[i]->SetPos(150 + i*25, 0);
		LoadTGA( &(theArrayOfHealth[i]->GoodiesTexture[1]), "images/health.tga");
	}

	// Enemy
	life = 0;

	index = 0;

	GetOut = false;
	ComeIn = false;
	Size = 0;
	for (int i = 0; i < 20; i ++)
		SName[i] = -1;

	move = false;
	timer = new CTimer();
	timer->StartTimer();
	timer->Init(5,0);

	spawntimer.StartTimer();
	spawntimer.SetMin(0);
	spawntimer.SetSec(5);

	players_.push_back(new CPlayerInfo(300, 450, 1));
	Whip = new Weapon();

	m_iNumberOfMaps = 0;
	string TempLevelName;

	ifstream myfile ("levelselect.txt");
	if (myfile.is_open())
	{
		while ( getline (myfile, TempLevelName) )
		{
			AllTheMaps[m_iNumberOfMaps] = TempLevelName.c_str();
			m_iNumberOfMaps++;
		}
	  
		myfile.close();
	}
	else cout << "Unable to open Map.txt file"; 


	MapEditor = new CMapEditor();
	MapEditor->Init(TileMapTexture, tileOffset_x, mapOffset_x, 
		theGrid->getNumOfTiles_ScreenWidth(), theGrid->getNumOfTiles_ScreenHeight(),
		theMap->getNumOfTiles_ScreenWidth(), theMap->getNumOfTiles_ScreenHeight(),
		theMap->getNumOfTiles_MapWidth(), theGrid->theScreenMap, theMap->theScreenMap);

	LevelSelect = new CLevelSelect();

	theButton[0].SetButtonPos(282.5, 400);
	theButton[0].SetButtonColor(0.8, 1.0f);
	theButton[0].SetTextPos(27, 38);
	theButton[0].SetTitle("Play");

	theButton[1].SetButtonPos(427.5, 400);
	theButton[1].SetButtonColor(0.8, 1.0f);
	theButton[1].SetTextPos(27, 38);
	theButton[1].SetTitle("Edit");

	theButton[2].SetButtonPos(700.0, 405);
	theButton[2].SetButtonColor(0.8, 1.0f);
	theButton[2].SetTextPos(20, 38);
	theButton[2].SetSize(0.8f);
	theButton[2].SetTitle("Back");

	theButton[3].SetButtonPos(280, 350);
	theButton[3].SetButtonColor(0.8, 1.0f);
	theButton[3].SetTextPos(32, 38);
	theButton[3].SetTitle("No");

	theButton[4].SetButtonPos(420, 350);
	theButton[4].SetButtonColor(0.8, 1.0f);
	theButton[4].SetTextPos(32, 38);
	theButton[4].SetTitle("Yes");

	return true;
}

bool myApplication::Update(void)
{
	timeSinceStart = glutGet(GLUT_ELAPSED_TIME);
    deltaTime = timeSinceStart - oldTimeSinceStart;
    oldTimeSinceStart = timeSinceStart;

	int RandomItem = rand()%(33-32 + 1) + 32;

	if (shootfire)
		theBulletPosition = theBulletPosition + DirectionVector * 25;

	if (theBulletPosition.x < 0 || theBulletPosition.x > 750 || theBulletPosition.y < 0 || theBulletPosition.y > 600)
	{
		theBulletPosition.x = 0;
		theBulletPosition.y = 500;
		shootfire = false;
	}

	// Update Hero's info
	PlayerList::iterator Players;
	for (Players = players_.begin(); Players != players_.end(); Players++)
	{
		if ((*Players)->isOnGround())
		{
			int checkPosition_X = (int) ceil((float)(mapOffset_x+(*Players)->GetPos_x()) / TILE_SIZE);
			int checkPosition_X_1 = (int) ceil((float)(mapOffset_x+(*Players)->GetPos_x()) / TILE_SIZE);
			int checkPosition_Y = (int) floor( ((float)(*Players)->GetPos_y()-(*Players)->GetJumpspeed()) / TILE_SIZE);
		
			// Don't jump
			// Check if the Hero is standing on air, if yes, then fall down.
			if (CheckCollision( (*Players)->GetPos_x(), (*Players)->GetPos_y(), false, true, false, false) == false)
			{
				(*Players)->SetOnFreeFall(true);
			}

			if (Whip->GetPosX() > 20 && Whip->GetPosX() < 780)
			{
 				int checkWhipPosition_X = (int) ceil((float)(Whip->GetPosX() + mapOffset_x) / TILE_SIZE);
				int checkWhipPosition_Y = (int) floor( ((float)Whip->GetPosY()) / TILE_SIZE);
			
				if (theMap->theScreenMap[checkWhipPosition_Y][checkWhipPosition_X] == 10 || theMap->theScreenMap[checkWhipPosition_Y][checkWhipPosition_X] == 15 ||
					theMap->theScreenMap[checkWhipPosition_Y][checkWhipPosition_X] == 17 || theMap->theScreenMap[checkWhipPosition_Y][checkWhipPosition_X] == 18)
				{
					if (theMap->theScreenMap[checkWhipPosition_Y][checkWhipPosition_X] == 10)
					{
						theMap->theScreenMap[checkWhipPosition_Y][checkWhipPosition_X] = RandomItem;
						theMap->theScreenMap[checkWhipPosition_Y-1][checkWhipPosition_X] = 0;
					}
					else if (theMap->theScreenMap[checkWhipPosition_Y][checkWhipPosition_X] == 11)
					{
						theMap->theScreenMap[checkWhipPosition_Y][checkWhipPosition_X] = 0;
						theMap->theScreenMap[checkWhipPosition_Y+1][checkWhipPosition_X] = RandomItem;
					}

					if (theMap->theScreenMap[checkWhipPosition_Y][checkWhipPosition_X] == 17)
					{
						theMap->theScreenMap[checkWhipPosition_Y][checkWhipPosition_X] = RandomItem;
						theMap->theScreenMap[checkWhipPosition_Y-1][checkWhipPosition_X] = 0;
					}
					else if (theMap->theScreenMap[checkWhipPosition_Y][checkWhipPosition_X] == 18)
					{
						theMap->theScreenMap[checkWhipPosition_Y][checkWhipPosition_X] = 0;
						theMap->theScreenMap[checkWhipPosition_Y+1][checkWhipPosition_X] = RandomItem;
					}
				}
			}

			if (theMap->theScreenMap[checkPosition_Y][checkPosition_X] == 14)
			{
		 		if (myKeys['w'] == true)
					TILESIZE = 100;
			}
			else
			{
				if (myKeys['a'] == true || myKeys['d'] == true)
					TILESIZE = 25;
			}

			if (theMap->theScreenMap[checkPosition_Y][checkPosition_X] == 20 || theMap->theScreenMap[checkPosition_Y][checkPosition_X] == 21)
			{
				if (myKeys['s'] == true && theMap->getmapName() == "level2.csv")
				{
					Loaded = false;
					theMap->setmapName("level3.csv");
					theMap->LoadMap(theMap->getmapName());
					(*Players)->SetPos_x(10);
					(*Players)->SetPos_y(125);
					mapOffset_x = 0;
				}
			}

			if (theMap->theScreenMap[checkPosition_Y][checkPosition_X] == 30)
			{
				EnemyList::iterator enemies;
				for (enemies = enemies_.begin(); enemies != enemies_.end(); enemies++)
				{
					delete *enemies;
					enemies_.erase(enemies);
					break;
				}

				Loaded = false;
				theMap->setmapName("level4.csv");
				theMap->LoadMap(theMap->getmapName());
				(*Players)->SetPos_x(180);
				(*Players)->SetPos_y(450);
				mapOffset_x = 0;
			}

			if (theMap->theScreenMap[checkPosition_Y][checkPosition_X] == 32)
			{
				Audio->PlaySFX(4);
				theMap->theScreenMap[checkPosition_Y][checkPosition_X] = 0;
				coin+=1;
			}
			if (theMap->theScreenMap[checkPosition_Y][checkPosition_X] == 33)
			{
				if ((*Players)->GetHealth() < 5)
				{
					Audio->PlaySFX(2);
					theMap->theScreenMap[checkPosition_Y][checkPosition_X] = 0;
					(*Players)->SetHealth((*Players)->GetHealth()+1);
				}
			}
		}
		else if ((*Players)->isJumpUpwards())
		{
			// Check if the hero can move up into mid air...
			int checkBulletPosition_X = (int) ceil((float)(mapOffset_x+theBulletPosition.x) / TILE_SIZE);
			int checkBulletPosition_Y = (int) floor( ((float)theBulletPosition.y) / TILE_SIZE);

			int checkPosition_X = (int) ceil((float)(mapOffset_x+(*Players)->GetPos_x()) / TILE_SIZE);
			int checkPosition_X_floor = (int) floor((float)(mapOffset_x+(*Players)->GetPos_x()) / TILE_SIZE);
			int checkPosition_Y = (int) floor( ((float)(*Players)->GetPos_y()-(*Players)->GetJumpspeed()) / TILE_SIZE);
			
			if (theMap->theScreenMap[checkPosition_Y][checkPosition_X] == 1 || theMap->theScreenMap[checkPosition_Y][checkPosition_X_floor ] == 1 ||
				theMap->theScreenMap[checkPosition_Y][checkPosition_X] == 2 || theMap->theScreenMap[checkPosition_Y][checkPosition_X_floor ] == 2 ||
				theMap->theScreenMap[checkPosition_Y][checkPosition_X] == 14 || theMap->theScreenMap[checkPosition_Y][checkPosition_X_floor ] == 14)
			{
				// Since the new position does not allow the hero to move into, then go back to the old position
				(*Players)->SetPos_y( (checkPosition_Y+1) * TILE_SIZE );
				(*Players)->SetOnFreeFall( true );
			}
			else
			{
				(*Players)->UpdateJumpUpwards();
			}
		}
		else if ((*Players)->isFreeFall())
		{
			// Check if the hero is still in mid air...
			int checkBulletPosition_X = (int) ceil((float)(mapOffset_x+theBulletPosition.x) / TILE_SIZE);
			int checkBulletPosition_Y = (int) floor( ((float)theBulletPosition.y) / TILE_SIZE);

			int checkPosition_X_floor = (int) floor((float)(mapOffset_x+(*Players)->GetPos_x()) / TILE_SIZE);
			int checkPosition_X = (int) ceil(((float)(mapOffset_x+(*Players)->GetPos_x()) / TILE_SIZE));
			int checkPosition_Y = (int) ceil( ((float)(*Players)->GetPos_y()+(*Players)->GetJumpspeed()) / TILE_SIZE);
			if (theMap->theScreenMap[checkPosition_Y][checkPosition_X] == 1 || theMap->theScreenMap[checkPosition_Y][checkPosition_X_floor] == 1 ||
				theMap->theScreenMap[checkPosition_Y][checkPosition_X] == 2 || theMap->theScreenMap[checkPosition_Y][checkPosition_X_floor] == 2 ||
				theMap->theScreenMap[checkPosition_Y][checkPosition_X] == 14 || theMap->theScreenMap[checkPosition_Y][checkPosition_X_floor] == 14 ||
				theMap->theScreenMap[checkPosition_Y][checkPosition_X] == 19 || theMap->theScreenMap[checkPosition_Y][checkPosition_X_floor] == 19)
			{ 
				// Since the new position does not allow the hero to move into, then go back to the old position
				(*Players)->SetPos_y( (checkPosition_Y-1) * TILE_SIZE );
				(*Players)->SetToStop();
			}
			else
			{
				(*Players)->UpdateFreeFall(1);
			}
		}

		if (theMap->getmapName() == "level1.csv")
		{
			if (!BossSpawn)
			{
				spawntimer.Update();
				SpawnEnemies(3, 1, 15);	
			}
		}
			
		if ((*Players)->GetInvulnerable())
			(*Players)->Invulnerability();

		if ((*Players)->GetPos_y() > 520)
		{
			if ((*Players)->GetHealth() > 0) // spawn player when health = 0
			{
				(*Players)->SetPos_x(170);
				(*Players)->SetPos_y(250);
				(*Players)->SetInvulnerable(true);
				(*Players)->SetHealth((*Players)->GetHealth() - 1);
			}
		}

		EnemyList::iterator enemies;
		for (enemies = enemies_.begin(); enemies != enemies_.end(); enemies++)
		{
			int player1, player2;
			player1 = (*enemies)->CalculateDistance(players_.at(0)->GetPos_x(), players_.at(0)->GetPos_y(), mapOffset_x);
			if (players == TWOPLAYER)
			{
				player2 = (*enemies)->CalculateDistance(players_.at(1)->GetPos_x(), players_.at(1)->GetPos_y(), mapOffset_x);
				(*enemies)->Update((*Players), (*Players)->GetID(), timelastcall, mapOffset_x, player1, player2);
			}
			else
				(*enemies)->Update((*Players), (*Players)->GetID(), timelastcall, mapOffset_x, player1, 1000000);

			if ((*enemies)->GetPos_x() - mapOffset_x < -50 || (*enemies)->GetPos_x() - mapOffset_x > 840)
			{
				if ((*enemies)->GetID() == 1 || (*enemies)->GetID() == 2)
				{
					delete *enemies;
					enemies_.erase(enemies);
					break;
				}
			}

			if (!(*Players)->GetInvulnerable())
			{
				if ((*Players)->CalculateDistance((*enemies)->GetPos_x(), (*enemies)->GetPos_y(), mapOffset_x) < 1000 ||
					(*Players)->CalculateDistance((*enemies)->GetFireBallPos_x(), (*enemies)->GetFireBallPos_y(), 0) < 2000)
				{	
					if ((*Players)->GetHealth() > 0) // spawn player when health = 0
					{
						(*Players)->SetInvulnerable(true);
						
						if ((*Players)->GetPos_x() > 100 && (*Players)->GetPos_x() < 700)
						{
							if ((*Players)->GetPos_x() < (*enemies)->GetPos_x())
								(*Players)->SetPos_x((*Players)->GetPos_x() + 60);
							else
								(*Players)->SetPos_x((*Players)->GetPos_x() - 60);
							(*Players)->SetPos_y((*Players)->GetPos_y() - 60);
						}

						(*Players)->SetHealth((*Players)->GetHealth() - 1);
					}
				}
			}

			if (theMap->getmapName() == "level2.csv" && !Loaded)
			{
				if ((*enemies)->GetID() == 3)
				{
					delete *enemies;
					enemies_.erase(enemies);
					break;
				}
			}
		}

		// Panther
		if (theMap->getmapName() == "level2.csv" && !Loaded)
		{
			Loaded = true;
			enemies_.push_back(new Enemies(550, 450, 2, 2, 1, Enemies::IDLE));
		}

		// Panther
		if (theMap->getmapName() == "level2.csv" &&(*Players)->GetPos_x() >= 300 && (*Players)->GetPos_x() < 301)
		{
			if (enemies_.size() < 4)
				enemies_.push_back(new Enemies(-10, 450, 2, 2, 2, Enemies::ATTACK));
		}

		// Water monster
		if (theMap->getmapName() == "level3.csv" && (*Players)->GetPos_x() >= 290 && (*Players)->GetPos_x() <= 292)
		{
			Audio->PlaySFX(5);
			enemies_.push_back(new Enemies(350, 550, 8, 1, 1, Enemies::IDLE));
		}

		// Water monster
		if (theMap->getmapName() == "level3.csv" && ((*Players)->GetPos_x() + mapOffset_x) >= 726 && ((*Players)->GetPos_x() + mapOffset_x) <= 728)
		{
			Audio->PlaySFX(5);
			enemies_.push_back(new Enemies(750, 550, 8, 1, 2, Enemies::IDLE));
		}

		if ((*Players)->CalculateDistance(KnifeX, KnifeY, 0) < 100)
		{
			Audio->PlayBGM(0);
			Loaded = false;
			BossSpawn = false;
			KnifeX = -100;
			KnifeY = -100;
			StageClear = false;
			if (theMap->getmapName() == "level1.csv")
				theMap->setmapName("level2.csv");
			theMap->LoadMap(theMap->getmapName());
			mapOffset_x = 0;
			spawntimer.Init(0, 15);
			if (players == TWOPLAYER)
				players_.at(1)->SetPos_x(350);
			players_.at(0)->SetPos_x(300);
		}

		if (theMap->getmapName() == "level2.csv" && (*Players)->GetPos_x() >= 750 && mapOffset_x <= 620 && !DoorMove ||
			theMap->getmapName() == "level4.csv" && (*Players)->GetPos_x() >= 520 && (*Players)->GetPos_x() < 521 && (*Players)->GetPos_y() <= 250 && !DoorMove)
			DoorMove = true;

		if (!DoorMove && (mapOffset_x == 620 || mapOffset_x == 136))
		{
			if ((*Players)->GetPos_x() <= 370 || (*Players)->GetPos_x() <= 384)
			{
				DoorTexture = 11;
				DoorWait+=1.0f;
			}
			else if ((*Players)->GetPos_x() >= 460)
			{
				DoorWait = 0.0f;
				DoorTexture = 10;
				DoorMove = true;
			}
			if (DoorWait > 10)
				(*Players)->SetPos_x((*Players)->GetPos_x() + 4);
		}

		if (DoorMove)
		{
			(*Players)->SetPos_x((*Players)->GetPos_x() - 4);
			mapOffset_x = mapOffset_x + (int) (4.0f * 1.0f);
			
			for (int i = 0; i < 255; i ++)
				myKeys[i] = false;

			if (theMap->getmapName() == "level2.csv")
			{
				if (mapOffset_x > theMap->getScreenWidth())
				{
					DoorMove = false;
					mapOffset_x = theMap->getScreenWidth();
				}

				if (mapOffset_x == 620)
					DoorMove = false;
			}
			else
			{
				if (mapOffset_x > 575)
				{
					DoorMove = false;
					mapOffset_x = 575;
				}

				if (mapOffset_x == 136)
					DoorMove = false;
			}
		}

		if (myKeys['9'] == true)
			gamemode = LEVELSELECT;

		if (myKeys['0'] == true)
		{
			MapEditor->Init(TileMapTexture, tileOffset_x, mapOffset_x, 
			theGrid->getNumOfTiles_ScreenWidth(), theGrid->getNumOfTiles_ScreenHeight(),
			theMap->getNumOfTiles_ScreenWidth(), theMap->getNumOfTiles_ScreenHeight(),
			theMap->getNumOfTiles_MapWidth(), theGrid->theScreenMap, theMap->theScreenMap);
			gamemode = MAPEDITOR;
		}

		if (myKeys['1'] == true)
			gamemode = DEATHSCREEN;

		if (mapOffset_x == 300 && players == ONEPLAYER && theMap->getmapName() != "level4.csv")
		{
			AutoSave = true;
			Save_Load.Saving(players_.at(0), theMap, score, mapOffset_x, timer);
		}

		if (myKeys['2'] == true)
			Save_Load.Saving(players_.at(0), theMap, score, mapOffset_x, timer);

		if (myKeys['3'] == true)
		{
			Save_Load.Loading(players_.at(0), theMap, &score, &mapOffset_x, timer);
		}

		if ((*Players)->GetHealth() <= 0 || timer->Update())
		{
			if (players == TWOPLAYER)
			{
				(*Players)->SetPos_y(200);
				(*Players)->SetHealth(5);
			}
			else
			{
				Audio->PlayBGM(3);
				gamemode = DEATHSCREEN;
			}
		}

		if (CheckCollision( (*Players)->GetPos_x(), (*Players)->GetPos_y(), false, false, true, false) == false)
		{
			if(myKeys['a'] == true || myKeys['A'] == true || myKeys['j'] || myKeys['J'] == true)
			{
				if (players == TWOPLAYER)
				{
					if (players_.at(0)->GetPos_x() < 405 && players_.at(1)->GetPos_x() < 405 && 
						myKeys['a'] == true && myKeys['j'] == true && mapOffset_x != 800)
					{	
						players_.at(0)->SetPos_x(players_.at(0)->GetPos_x() + 5);
						players_.at(1)->SetPos_x(players_.at(1)->GetPos_x() + 5);
						mapOffset_x = mapOffset_x - (int) (4.0f * 1.0f);
						if (mapOffset_x < 0)
							mapOffset_x = 0;
						if (mapOffset_x == 0)
						{
							players_.at(0)->SetPos_x(players_.at(0)->GetPos_x() - 5);
							players_.at(1)->SetPos_x(players_.at(1)->GetPos_x() - 5);
							players_.at(0)->ConstrainHero(20, 750, 50, 550, 1.0f, mapOffset_x, mapOffset_y);
							players_.at(1)->ConstrainHero(20, 750, 50, 550, 1.0f, mapOffset_x, mapOffset_y);
						}
					}
					else if (myKeys['a'] == true)
					{
						players_.at(0)->ConstrainHero(20, 750, 50, 550, 1.0f, mapOffset_x, mapOffset_y);
						players_.at(1)->ConstrainHero(20, 750, 50, 550, 1.0f, mapOffset_x, mapOffset_y);
					}

					if (myKeys['j'] == true)
					{
						moveMeLeftRight(true, 1.0f, "player2");			
					}
					else
						players_.at(1)->SetMovingAnimation(0);
				}
				else if (players == ONEPLAYER)
				{
					if (players_.at(0)->GetPos_x() < 405 && myKeys['a'] == true && mapOffset_x != 800 && theMap->getmapName() != "level4.csv")
					{
						players_.at(0)->SetPos_x(players_.at(0)->GetPos_x() + 5);
						mapOffset_x = mapOffset_x - (int) (4.0f * 1.0f);
						if (mapOffset_x < 0)
							mapOffset_x = 0;
						if (mapOffset_x == 0)
						{
							players_.at(0)->SetPos_x(players_.at(0)->GetPos_x() - 5);
							players_.at(0)->ConstrainHero(20, 750, 50, 550, 1.0f, mapOffset_x, mapOffset_y);
						}
					}
					else if (myKeys['a'] == true)
					{
						if (theMap->getmapName() != "level2.csv" && theMap->getmapName() != "level4.csv")
						{
							players_.at(0)->ConstrainHero(20, 750, 50, 550, 1.0f, mapOffset_x, mapOffset_y);
						}
						else if (theMap->getmapName() == "level4.csv" && mapOffset_x >= 575 && mapOffset_x != 800)
						{
 							if (players_.at(0)->GetPos_x() < 405)
							{	
								if (mapOffset_x <= 575)
									players_.at(0)->ConstrainHero(20, 750, 50, 550, 1.0f, mapOffset_x, mapOffset_y);
								else
								{
									mapOffset_x = mapOffset_x - (int) (4.0f * 1.0f);
									players_.at(0)->SetPos_x(players_.at(0)->GetPos_x() + 5);
								}
							}
						}
						else
						{
							if (theMap->getmapName() == "level4.csv")
								players_.at(0)->ConstrainHero(20, 750, 50, 550, 1.0f, mapOffset_x, mapOffset_y);
							else
								players_.at(0)->ConstrainHero(220, 750, 50, 550, 1.0f, mapOffset_x, mapOffset_y);
						}
					}
				}
				if(myKeys['a'] == true || myKeys['A'] == true)
				{
					moveMeLeftRight(true, 1.0f, "player1");	
				}
				else
					players_.at(0)->SetMovingAnimation(0);
			}
		}

		if(myKeys['s'] == true || myKeys['S'] == true)
		{
			moveMeUpDown(false, 1.0f);
		}

		if (CheckCollision( (*Players)->GetPos_x(), (*Players)->GetPos_y(), false, false, false, true) == false)
		{
			if(myKeys['d'] == true || myKeys['D'] == true || myKeys['l'] || myKeys['L'] == true)
			{
				if (players == TWOPLAYER)
				{
					if (players_.at(0)->GetPos_x() > 405 && players_.at(1)->GetPos_x() > 405 && 
						myKeys['d'] == true && myKeys['l'] == true)
					{	
						players_.at(0)->SetPos_x(players_.at(0)->GetPos_x() - 5);
						players_.at(1)->SetPos_x(players_.at(1)->GetPos_x() - 5);
						mapOffset_x = mapOffset_x + (int) (4.0f * 1.0f);
						if (mapOffset_x > theMap->getScreenWidth())
							mapOffset_x = theMap->getScreenWidth();
						if (mapOffset_x == theMap->getScreenWidth())
						{
							players_.at(0)->SetPos_x(players_.at(0)->GetPos_x() + 5);
							players_.at(1)->SetPos_x(players_.at(1)->GetPos_x() + 5);
							players_.at(0)->ConstrainHero(50, 750, 50, 550, 1.0f, mapOffset_x, mapOffset_y);
							players_.at(1)->ConstrainHero(50, 750, 50, 550, 1.0f, mapOffset_x, mapOffset_y);
						}
					}
					else
					{
						players_.at(0)->ConstrainHero(50, 750, 50, 550, 1.0f, mapOffset_x, mapOffset_y);
						players_.at(1)->ConstrainHero(50, 750, 50, 550, 1.0f, mapOffset_x, mapOffset_y);
					}
					
					if (myKeys['l'] == true || myKeys['L'] == true)
					{
						moveMeLeftRight(false, 1.0f, "player2");			
					}
					else
						players_.at(1)->SetMovingAnimation(0);
				}
				else if (players == ONEPLAYER)
				{
					if (players_.at(0)->GetPos_x() > 405 && myKeys['d'] == true)
					{
						if (theMap->getmapName() != "level2.csv" && theMap->getmapName() != "level3.csv" && theMap->getmapName() != "level4.csv")
						{
							players_.at(0)->SetPos_x(players_.at(0)->GetPos_x() - 5);
							mapOffset_x = mapOffset_x + (int) (4.0f * 1.0f);
					 		if (mapOffset_x > theMap->getScreenWidth())
								mapOffset_x = theMap->getScreenWidth();
							if (mapOffset_x == theMap->getScreenWidth())
							{
								players_.at(0)->SetPos_x(players_.at(0)->GetPos_x() + 5);
								players_.at(0)->ConstrainHero(50, 750, 50, 550, 1.0f, mapOffset_x, mapOffset_y);
							}
						}
						else if (theMap->getmapName() == "level2.csv")
						{ 
							if (mapOffset_x >= 224)
							{
								players_.at(0)->ConstrainHero(50, 750, 50, 550, 1.0f, mapOffset_x, mapOffset_y);
							}
							else
							{
								players_.at(0)->SetPos_x(players_.at(0)->GetPos_x() - 5);
								mapOffset_x = mapOffset_x + (int) (4.0f * 1.0f);
							}
						}
						else if (theMap->getmapName() == "level3.csv")
						{
							players_.at(0)->SetPos_x(players_.at(0)->GetPos_x() - 5);
					 		if (mapOffset_x >= 370)
							{
								players_.at(0)->SetPos_x(players_.at(0)->GetPos_x() + 5);
								players_.at(0)->ConstrainHero(50, 750, 50, 550, 1.0f, mapOffset_x, mapOffset_y);
							}
							else
								mapOffset_x = mapOffset_x + (int) (4.0f * 1.0f);
						}
						else if (theMap->getmapName() == "level4.csv" && mapOffset_x >= 575)
						{
							players_.at(0)->SetPos_x(players_.at(0)->GetPos_x() - 5);
							mapOffset_x = mapOffset_x + (int) (4.0f * 1.0f);
					 		if (mapOffset_x > theMap->getScreenWidth())
								mapOffset_x = theMap->getScreenWidth();
							if (mapOffset_x == theMap->getScreenWidth())
							{
								players_.at(0)->SetPos_x(players_.at(0)->GetPos_x() + 5);
								players_.at(0)->ConstrainHero(50, 750, 50, 550, 1.0f, mapOffset_x, mapOffset_y);
							}
						}
					}
					else if (players_.at(0)->GetPos_x() < 405 && myKeys['d'] == true)
					{
						if (theMap->getmapName() != "level4.csv")
							players_.at(0)->SetPos_x(players_.at(0)->GetPos_x() + 2);

						players_.at(0)->ConstrainHero(50, 750, 50, 550, 1.0f, mapOffset_x, mapOffset_y);
					}
				}
				if(myKeys['d'] == true || myKeys['D'] == true)
				{
					moveMeLeftRight(false, 1.0f, "player1");	
				}
				else
					players_.at(0)->SetMovingAnimation(0);
				
			}
		}

		if(myKeys['w'] == true || myKeys['W'] == true || myKeys['i'] == true ||myKeys['I'] == true)
		{
			if ((*Players)->isOnGround())
			{
				if (myKeys['w'] == true || myKeys['W'] == true)
					moveMeJump("player1");
				
				if(players == TWOPLAYER)
				{
					if (myKeys['i'] == true || myKeys['I'] == true)
						moveMeJump("player2");
				}
			}
		}

		AnimationAttack();
		if (players == TWOPLAYER)
			AnimationAttack2();

		KnifeList::iterator Knife;
		for (Knife = knife_.begin(); Knife != knife_.end(); Knife++)
		{
			if ((*Knife)->Update(enemies_, mapOffset_x, score))
			{	
				delete *Knife;
				knife_.erase(Knife);
				break;
			}
		}

		for (EnemyList::iterator Enemies = enemies_.begin(); Enemies != enemies_.end(); Enemies++)
		{
			if ((*Enemies)->GetHitSomething())
			{
				(*Enemies)->SetHitSomething(false);
				if ((*Enemies)->GetID() == 3 || (*Enemies)->GetID() == 4)
					Audio->PlaySFX(1);
			}

			if ((*Enemies)->GetDeath())
			{
				if ((*Enemies)->GetID() == 3 || (*Enemies)->GetID() == 4)
				{
					if ((*Enemies)->GetID() == 3 || (*Enemies)->GetID() == 4)
						StageClear = true;
					Audio->PlayBGM(2);
				}
				else
					Audio->PlaySFX(1);

				delete *Enemies;
				enemies_.erase(Enemies);
				break;
			}
		}

		if (theMap->getmapName() == "level1.csv" ||  theMap->getmapName() == "level4.csv" && (mapOffset_x == 800 || mapOffset_x == 68) && !BossSpawn)
		{
			if (theMap->getmapName() == "level4.csv" && mapOffset_x >= 68 && mapOffset_x < 69)
			{
 				enemies_.push_back(new Enemies(1200, 200, 4, 4, 1, Enemies::IDLE));
			}
			if (mapOffset_x == 800 && !BossSpawn)
			{
				Audio->PlayBGM(1);

				if (theMap->getmapName() == "level1.csv")
					enemies_.push_back(new Enemies(1545, 450, 3, 3, 1, Enemies::ATTACK));

				EnemyList::iterator enemies;
				for (enemies = enemies_.begin(); enemies != enemies_.end(); enemies++)
				{
					if((*enemies)->GetID() == 3)
						(*enemies)->SetHealth(5);
				}
				BossSpawn = true;
			}	
		}
	}

	//For potion use
	if((myKeys['e'] == true || myKeys['E'] == true) && hp != 5 && potion != 0 && !holdkey)
	{
		potion--;
		hp++;
		holdkey = true;
	}

	if(myKeys[27] == true)
		exit(0);


	//For the smoke particle effect
	if(smoke == true)
	{
		Scount += 0.5;
	}
	if(Scount >= 5)
	{
		smoke = false;
	}


	tileOffset_x = (int) (mapOffset_x / TILE_SIZE);
	if (tileOffset_x+theMap->getNumOfTiles_ScreenWidth() > theMap->getNumOfTiles_MapWidth())
		tileOffset_x = theMap->getNumOfTiles_MapWidth() - theMap->getNumOfTiles_ScreenWidth();

	return false;
}

void myApplication::UpdateMapEditor(void)
{
	//cout << "OverRide: " << MapEditor->GetOverWriteConfirmation() << endl;
	//cout << MapEditor->GetMouseOnButton() << endl;
		if (myKeys['a'] == true || myKeys['A'] == true)
		{
			// Scrolling the screen
			if (tileOffset_x != 0)
			tileOffset_x = tileOffset_x - 1;
		}

		if(myKeys['d'] == true || myKeys['D'] == true)
		{
			if (tileOffset_x <= 31)
				tileOffset_x = tileOffset_x + 1;
		}
		if (MapEditor->GetMouseOnButton() == 0)
		{
			MapEditor->SetSavingMap(false);
			Name = "";
			Size = 0;
		}
		else if (MapEditor->GetMouseOnButton() == 1)
		{
			gamemode = LEVELSELECT;
			tileOffset_x = 0;
			LevelSelect->Reset();
			LevelSelect->LoadEditedLevel();
			MapEditor->SetMouseClick(false);
			MapEditor->SetMouseOnButton(0);
		}
		else if (MapEditor->GetMouseOnButton() == 2)
		{
			MapEditor->SetOverWriteConfirmation(false);
			MapEditor->SetSavingMap(true);
		}
		else if (MapEditor->GetMouseOnButton() == 3)
		{
			MapEditor->SetSavingMap(false);
			Name = "";
			Size = 0;
		}
		else if (MapEditor->GetMouseOnButton() == 4)
		{
			
		}
		else if (MapEditor->GetMouseOnButton() == 5)
		{
			Name = "";
			Size = 0;
		}
		else if (MapEditor->GetMouseOnButton() == 6)
		{

		}
		else if (MapEditor->GetMouseOnButton() == 7)
		{
			if (!MapEditor->GetOverWriteConfirmation())
			{
				ofstream myfile ("levelselect.txt", ios::app);
				if (myfile.is_open())
				{
					myfile << Name << endl;

					AllTheMaps[m_iNumberOfMaps] = Name.c_str();
					m_iNumberOfMaps++;

					myfile.close();
				}
			else cout << "Unable to open file";
			}
			MapEditor->SaveMap(&theMap->theScreenMap);
			//cout << "MAPsaved\nMAPsaved\nMAPsaved\nMAPsaved\nMAPsaved\nMAPsaved\nMAPsaved\nMAPsaved\nMAPsaved\nMAPsaved\nMAPsaved\nMAPsaved\n" << endl;
			Name = Name + ".csv";
			if (!theMap->SaveMap(Name.c_str()))
				cout << "File not found" << endl;
			Name = "";
			Size = 0;
			MapEditor->SetOverWriteConfirmation(false);
			MapEditor->SetSavingMap(false);
			MapEditor->SetMouseOnButton(0);
		}
}
void myApplication::CreateCircle(int radius)
{
	float xcos, ysin, x, y, tx, ty;
	circle = glGenLists(1);
	glNewList(circle,GL_COMPILE); //compile the new list
	glPushMatrix();
	glColor3f(1.0f, 1.0f, 1.0f);
	glBegin(GL_POLYGON);
	for(int i=0; i<=360; i++)
	{
		const float angle = i * 3.14/180.0f;
		xcos = (float)cos(angle);
		ysin = (float)sin(angle);

		// Get the Position coordinates
		x = xcos * radius;
		y = ysin * radius;

		// Get the Texture coordinates
		tx = xcos * 0.5 + 0.5;
		ty = ysin * 0.5 + 0.5;

		// Draw the circle
		glTexCoord2f(tx, ty);
		glVertex2f(x, y);
	}
	glEnd();
	glPopMatrix();
	glEndList(); //end the list
}

void myApplication::collisioncircle()
{		
	glPushMatrix();
	glTranslatef(300, 250, 0);
		CreateCircle(10.0f);
		glCallList(circle);
	glPopMatrix();
}

void myApplication::drawbullets()
{
	glPushMatrix();
	glEnable( GL_TEXTURE_2D );
	glEnable( GL_BLEND );
	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glBindTexture(GL_TEXTURE_2D, UITexture[0].texID );
	glPushMatrix();
	glBegin(GL_QUADS);
	glTexCoord2f(0,0);glVertex3f(30,135,0);
	glTexCoord2f(1,0);glVertex3f(55,135,0);
	glTexCoord2f(1,1);glVertex3f(55,110,0);
	glTexCoord2f(0,1);glVertex3f(30,110,0);		

	glEnd();
	glDisable( GL_BLEND );
	glDisable( GL_TEXTURE_2D );
	glPopMatrix();
	glPopMatrix();
}

void myApplication::drawpotions()
{
	glPushMatrix();
	glEnable( GL_TEXTURE_2D );
	glEnable( GL_BLEND );
	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glBindTexture(GL_TEXTURE_2D, TileMapTexture[ 41 ].texID );
	glPushMatrix();
	glBegin(GL_QUADS);
	glTexCoord2f(0,0);glVertex3f(30,165,0);
	glTexCoord2f(1,0);glVertex3f(55,165,0);
	glTexCoord2f(1,1);glVertex3f(55,140,0);
	glTexCoord2f(0,1);glVertex3f(30,140,0);		

	glEnd();
	glDisable( GL_BLEND );
	glDisable( GL_TEXTURE_2D );
	glPopMatrix();
	glPopMatrix();
}

void myApplication::drawkeys()
{
	glPushMatrix();
	glEnable( GL_TEXTURE_2D );
	glEnable( GL_BLEND );
	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glBindTexture(GL_TEXTURE_2D, TileMapTexture[43].texID );
	glPushMatrix();
	glBegin(GL_QUADS);
	glTexCoord2f(0,0);glVertex3f(30,195,0);
	glTexCoord2f(1,0);glVertex3f(55,195,0);
	glTexCoord2f(1,1);glVertex3f(55,170,0);
	glTexCoord2f(0,1);glVertex3f(30,170,0);		

	glEnd();
	glDisable( GL_BLEND );
	glDisable( GL_TEXTURE_2D );
	glPopMatrix();
	glPopMatrix();
}


void myApplication::drawDeathScreen()
{
	glEnable(GL_TEXTURE_2D);

	// Draw Background image
	glPushMatrix();
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glBindTexture(GL_TEXTURE_2D, BackgroundTexture[1].texID );
		glPushMatrix();
			glBegin(GL_QUADS);
				glTexCoord2f(0,0); glVertex2f(0,600);
				glTexCoord2f(1,0); glVertex2f(800,600);
				glTexCoord2f(1,1); glVertex2f(800,0);
				glTexCoord2f(0,1); glVertex2f(0,0);				
			glEnd();
		glPopMatrix();
		glDisable(GL_BLEND);
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);
}

void myApplication::renderScene(void) {
	// Clear the buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glLoadIdentity();
	theCamera->Update();

	if ((timeGetTime()-timelastcall)>1000/frequency)
	{
		// Calculate the framerate
		calculateFPS();

		timelastcall=timeGetTime();

		if (gamemode == INGAME)
			Update();
	}

	if (players == TWOPLAYER && !SetPlayer) // function for single and multiplayer
	{
		frequency = 30.0f;
		SetPlayer = true;
		players_.push_back(new CPlayerInfo(350, 450, 2));
	}

	// Enable 2D text display and HUD
	theCamera->SetHUD( true );

	if (gamemode == INGAME || gamemode == EXITWINDOW)
	{
		// Display them!
		//MAP
		RenderBackground();
		if (AutoSave)
			RenderAutosave();

		RenderRearWalls();
		RenderMiddleWalls();
		RenderTileMap();

		if (theMap->getmapName() == "level2.csv" || theMap->getmapName() == "level4.csv")
			RenderDoor();
	
		PlayerList::iterator Players;
		for (Players = players_.begin(); Players != players_.end(); Players++)
		{
			(*Players)->RenderHero();
		}

		EnemyList::iterator Enemies;
		for (Enemies = enemies_.begin(); Enemies != enemies_.end(); Enemies++)
		{
			(*Enemies)->RenderEnemies(mapOffset_x);
		}

		KnifeList::iterator Knife;
		for (Knife = knife_.begin(); Knife != knife_.end(); Knife++)
		{
			(*Knife)->renderWeapon(mapOffset_x);
		}

		//UI
		glPushMatrix();
		glTranslatef(0, -15, 0);
			Renderhealth();
			RenderUI();
		glPopMatrix();

		if (StageClear)
		{
			if (coin != 0)
				score += coin * 10;

			coin = 0;
			if (theMap->getmapName() == "level1.csv")
				BallAnimation();
			else
			{
				if (players_.at(0)->GetPos_x() == 750)
				{
					Audio->PlayBGM(4);
					gamemode = WIN;
				}
			}
		}

		if(smoke)
			Rendersmoke();
	}

	//render menu
	if (gamemode == MMENU) //main menu
	{
		Reset();
		LevelSelect->Reset();
		Mainmenu->orthogonalStart ();
		Mainmenu->menulogo();
		Mainmenu->menustart();
		Mainmenu->menuscore();
		Mainmenu->mapeditor();
		Mainmenu->orthogonalEnd(); 
	}

	if (gamemode == HIGHSCORE)
	{
		RenderHighscore();
		if (!Load)
		{
			Load = true;
			highscore.Load();
		}
		highscore.Render();
	}

	if (gamemode == CONTROL)
	{
		RenderControl();
		Mainmenu->singleplayer();
		Mainmenu->multiplayer();
		Mainmenu->load();
	}

	if (gamemode == DEATHSCREEN)
	{
		drawDeathScreen();
		printw (420.0, 265.0, 0, "%i", score);
		if (!keypressed)
			EnterName();
	}

	if (gamemode == MAPEDITOR)
	{
		RenderBackground();

		MapEditor->RenderTileMapEditor(mapFineOffset_x, mapOffset_x);

		MapEditor->RenderGrid();

		MapEditor->SetMaptileOffSet(tileOffset_x);
		MapEditor->SetMapOffSet(mapOffset_x);

		MapEditor->RenderMapEditorMenu();
		
		UpdateMapEditor();

		if (MapEditor->GetSavingMap())
			EnterName();
	}

	if (gamemode == LEVELSELECT)
	{
		LevelSelect->LoadLevels();
		LevelSelect->RenderLevelSelect();
		LevelSelect->RenderTileMap(TileMapTexture, tileOffset_x, mapOffset_x);
		LevelSelect->Selection();
		LevelSelect->GetButton();
		for (int i = 0; i < 3; i ++)
			theButton[i].drawButton();
		if (theButton[0].Update())
		{
			theMap->setmapName(LevelSelect->GetLevel());
			theMap->LoadMap(theMap->getmapName());
			players_.at(0)->SetPos_x(170);
			if (theMap->getmapName() == "level3.csv")
				players_.at(0)->SetPos_y(250);
			gamemode = INGAME;
		}
		if (theButton[1].Update())
		{
			theMap->setmapName(LevelSelect->GetLevel());
			theMap->LoadMap(theMap->getmapName());
			MapEditor->Init(TileMapTexture, tileOffset_x, mapOffset_x, 
			theGrid->getNumOfTiles_ScreenWidth(), theGrid->getNumOfTiles_ScreenHeight(),
			theMap->getNumOfTiles_ScreenWidth(), theMap->getNumOfTiles_ScreenHeight(),
			theMap->getNumOfTiles_MapWidth(), theGrid->theScreenMap, theMap->theScreenMap);
			gamemode = MAPEDITOR;
		}
		if (theButton[2].Update())
			gamemode = MMENU;
	}

	if (gamemode == WIN)
		RenderEnding();

	if (gamemode == EXITWINDOW)
	{
		timer->StopTimer();
		players_.at(0)->timer.StopTimer();
		RenderExitWindow();
		for (int i = 3; i < 5; i ++)
			theButton[i].drawButton();

		if (theButton[3].Update())
		{
			timer->StartTimer();
			players_.at(0)->timer.StartInvulTimer();
			gamemode = INGAME;
		}

		if (theButton[4].Update())
		{
			AutoSave = false;
			AutoSaveTime = 0.0f;
			gamemode = MMENU;
		}
	}

		// Display framerate

	theCamera->SetHUD( false );

	// Flush off any entity which is not drawn yet, so that we maintain the frame rate.
	glFlush();

	// swapping the buffers causes the rendering above to be shown
	glutSwapBuffers();
}

void myApplication::changeSize(int w, int h) {

	// Prevent a divide by zero, when window is too short
	// (you cant make a window of zero width).
	if(h == 0)
		h = 1;

	float ratio = (float) (1.0f* w / h);

	// Reset the coordinate system before modifying
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// Set the viewport to be the entire window
	glViewport(0, 0, w, h);

	// Set the correct perspective.
	gluPerspective(45,ratio,1,1000);
	glMatrixMode(GL_MODELVIEW);
}

void myApplication::inputKey(int key, int x, int y) {

}

void myApplication::KeyboardDown(unsigned char key, int x, int y){

	myKeys[key]= true;
	keys = key;
	ComeIn = true;
	if (gamemode != DEATHSCREEN)
		keypressed = true;

	switch(key)
	{
		//Key for escape
		case 27:
			{
				if (gamemode == INGAME)
					gamemode = EXITWINDOW;
				else if (gamemode != MMENU && gamemode != WIN)
				{
					tileOffset_x = 0;
					gamemode = MMENU;
				}
				else if (gamemode == WIN)
					gamemode = DEATHSCREEN;
				else
					exit(0);
			}
			break;
	}
}

void myApplication::KeyboardUp(unsigned char key, int x, int y){

	myKeys[key]= false;
	holdkey = false;
	if (ComeIn)
	{
		GetOut = false;
		ComeIn = false;
	}
	keypressed = false;
}

void myApplication::gamepad (unsigned int buttonID, int x, int y, int z)
{
	if (buttonID == 512 && gamemode != EXITWINDOW && !startpressed)
	{
		startpressed = true;
		gamemode = EXITWINDOW;
	}
	else if (buttonID == 512 && gamemode == EXITWINDOW && !startpressed)
	{
		startpressed = true;
		gamemode = INGAME;
	}
	else if (buttonID == 0)
		startpressed = false;

	if (x < 0)
		myKeys['a'] = true;		// Left
	else if (x > 0)
		myKeys['d'] = true;		// Right
	else
	{
		myKeys['a'] = false;
		myKeys['d'] = false;
	}

	if(buttonID == 4)
		myKeys['w'] = true;		// Jump
	else if(buttonID == 32)
		myKeys['q'] = true;		// Whip
	else if(buttonID == 128)
		myKeys['e'] = true;		// Knife
	else
	{
		myKeys['w'] = false;
		myKeys['e'] = false;
		myKeys['q'] = false;
	}
}

void myApplication::MouseMove (int x, int y) {
	mouseInfo.lastX = x;
	mouseInfo.lastY = y;

	if (gamemode == MAPEDITOR)
	{
		// Placement of the tiles
		MapEditor->Update(mouseInfo.lastX, mouseInfo.lastY, mapOffset_x);
		MapEditor->MapEditorTileMouse();
	}

	if (gamemode == LEVELSELECT)
	{
		LevelSelect->Update(mouseInfo.lastX, mouseInfo.lastY);
	}

	for (int i = 0; i < NUMBUTTON; i ++)
		theButton[i].SetMouse(mouseInfo.lastX, mouseInfo.lastY);
}

void myApplication::MouseClick(int button, int state, int x, int y) {
	switch (button) {

		case GLUT_LEFT_BUTTON:
			mouseInfo.mLButtonUp = state;
			mouseInfo.lastX = x;
			mouseInfo.lastY = y;

			if (state == GLUT_DOWN)
			{
				LevelSelect->SetClick(true);
				for (int i = 0; i < NUMBUTTON; i ++)
					theButton[i].SetState(true);
				if (gamemode == MMENU && MouseUp) 
				{
					switch ( button )
					{
						case GLUT_LEFT_BUTTON : 
							if (x <= 525 && x >= 275 && y >= 330 && y <= 390) // Start button
							{ 
								gamemode = CONTROL;
								MouseUp = false;
							}
							if (x <= 525 && x >= 275 && y >= 390 && y <= 450) // Highscore button
							{ 
								gamemode = HIGHSCORE;
								MouseUp = false;
							}
							if (x <= 525 && x >= 275 && y >= 460 && y <= 520) // MapEditor button
							{ 
								gamemode = LEVELSELECT;
								MouseUp = false;
							}
					}
					
				}

				if (gamemode == HIGHSCORE && MouseUp)
				{
					if (x <= 585 && x >= 225 && y >= 490 && y <= 580) // Main menu button
					{
 						gamemode = MMENU;
						MouseUp = false;
					}
				}

				if (gamemode == CONTROL && MouseUp) // control = choice between single and multiplayer
				{
					if (x <= 525 && x >= 275 && y >= 370 && y <= 420) // singleplayer
					{
						gamemode = INGAME;
						players = ONEPLAYER;
						MouseUp = false;
					}
					if (x <= 525 && x >= 275 && y >= 430 && y <= 480) // multiplayer
					{
						gamemode = INGAME;
						players = TWOPLAYER;
						MouseUp = false;
					}

					if (x <= 525 && x >= 275 && y >= 490 && y <= 540) // load
					{
						Save_Load.Loading(players_.at(0), theMap, &score, &mapOffset_x, timer); 
						gamemode = INGAME;
						MouseUp = false;
					}
				}

				if (gamemode == DEATHSCREEN && MouseUp)
				{
					if (x <= 545 && x >= 270 && y >= 465 && y <= 530) // Enter button
					{ 
						Load = false;
						highscore.Save(Name, score);
						gamemode = HIGHSCORE;
						MouseUp = false;
					}
				}

				if (gamemode == MAPEDITOR)
				{
					MapEditor->SetMouseClick(true);

					MapEditor->MapEditorTileMouse();
					MapEditor->MapEditorMenuMouse();

					if (MapEditor->GetSavingMap() == true)
					{
						if (MapEditor->GetMouseOnButton() == 4)
						{
							for (int i = 0; i < (m_iNumberOfMaps+1); i++)
							{
								if (AllTheMaps[i] == Name.c_str())
								{
									MapEditor->SetOverWriteConfirmation(true);
									break;
								}		
								if (MapEditor->GetOverWriteConfirmation() == false && i == (m_iNumberOfMaps))
								{
									MapEditor->SetMouseOnButton(7);
								}
							}
						}
						else if (MapEditor->GetMouseOnButton() == 5)
						{
							Name = "";
							Size = 0;
							MapEditor->SetMouseOnButton(2);
						}
					}
				}
			}

			if (mouseInfo.mLButtonUp) {
				MouseUp = true;
				if (gamemode == MAPEDITOR)
				{
					MapEditor->SetMouseClick(false);
				}
				LevelSelect->SetClick(false);
				for (int i = 0; i < NUMBUTTON; i ++)
					theButton[i].SetState(false);
			}

			break;

		case GLUT_RIGHT_BUTTON:
			break;

		case GLUT_MIDDLE_BUTTON:
			break;
	}
}

bool myApplication::LoadTGA(TextureImage *texture, char *filename)			// Loads A TGA File Into Memory
{    
	GLubyte		TGAheader[12]={0,0,2,0,0,0,0,0,0,0,0,0};	// Uncompressed TGA Header
	GLubyte		TGAcompare[12];								// Used To Compare TGA Header
	GLubyte		header[6];									// First 6 Useful Bytes From The Header
	GLuint		bytesPerPixel;								// Holds Number Of Bytes Per Pixel Used In The TGA File
	GLuint		imageSize;									// Used To Store The Image Size When Setting Aside Ram
	GLuint		temp;										// Temporary Variable
	GLuint		type=GL_RGBA;								// Set The Default GL Mode To RBGA (32 BPP)

	FILE *file = fopen(filename, "rb");						// Open The TGA File

	if(	file==NULL ||										// Does File Even Exist?
		fread(TGAcompare,1,sizeof(TGAcompare),file)!=sizeof(TGAcompare) ||	// Are There 12 Bytes To Read?
		memcmp(TGAheader,TGAcompare,sizeof(TGAheader))!=0				||	// Does The Header Match What We Want?
		fread(header,1,sizeof(header),file)!=sizeof(header))				// If So Read Next 6 Header Bytes
	{
		if (file == NULL)									// Did The File Even Exist? *Added Jim Strong*
			return false;									// Return False
		else
		{
			fclose(file);									// If Anything Failed, Close The File
			return false;									// Return False
		}
	}

	texture->width  = header[1] * 256 + header[0];			// Determine The TGA Width	(highbyte*256+lowbyte)
	texture->height = header[3] * 256 + header[2];			// Determine The TGA Height	(highbyte*256+lowbyte)

	if(	texture->width	<=0	||								// Is The Width Less Than Or Equal To Zero
		texture->height	<=0	||								// Is The Height Less Than Or Equal To Zero
		(header[4]!=24 && header[4]!=32))					// Is The TGA 24 or 32 Bit?
	{
		fclose(file);										// If Anything Failed, Close The File
		return false;										// Return False
	}

	texture->bpp	= header[4];							// Grab The TGA's Bits Per Pixel (24 or 32)
	bytesPerPixel	= texture->bpp/8;						// Divide By 8 To Get The Bytes Per Pixel
	imageSize		= texture->width*texture->height*bytesPerPixel;	// Calculate The Memory Required For The TGA Data

	texture->imageData=(GLubyte *)malloc(imageSize);		// Reserve Memory To Hold The TGA Data

	if(	texture->imageData==NULL ||							// Does The Storage Memory Exist?
		fread(texture->imageData, 1, imageSize, file)!=imageSize)	// Does The Image Size Match The Memory Reserved?
	{
		if(texture->imageData!=NULL)						// Was Image Data Loaded
			free(texture->imageData);						// If So, Release The Image Data

		fclose(file);										// Close The File
		return false;										// Return False
	}

	for(GLuint i=0; i<int(imageSize); i+=bytesPerPixel)		// Loop Through The Image Data
	{														// Swaps The 1st And 3rd Bytes ('R'ed and 'B'lue)
		temp=texture->imageData[i];							// Temporarily Store The Value At Image Data 'i'
		texture->imageData[i] = texture->imageData[i + 2];	// Set The 1st Byte To The Value Of The 3rd Byte
		texture->imageData[i + 2] = temp;					// Set The 3rd Byte To The Value In 'temp' (1st Byte Value)
	}

	fclose (file);											// Close The File

	// Build A Texture From The Data
	glGenTextures(1, &texture[0].texID);					// Generate OpenGL texture IDs

	glBindTexture(GL_TEXTURE_2D, texture[0].texID);			// Bind Our Texture
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);	// Linear Filtered
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);	// Linear Filtered

	if (texture[0].bpp==24)									// Was The TGA 24 Bits
	{
		type=GL_RGB;										// If So Set The 'type' To GL_RGB
	}

	glTexImage2D(GL_TEXTURE_2D, 0, type, texture[0].width, texture[0].height, 0, type, GL_UNSIGNED_BYTE, texture[0].imageData);

	return true;											// Texture Building Went Ok, Return True
}


void myApplication::moveMeUpDown(bool mode, float timeDiff)
{
	// Check if the character is at a ladder. Return if not.
	return;
	if (mode)
	{
		players_.at(0)->SetPos_y( players_.at(0)->GetPos_y() - (int) (5.0f * timeDiff) );
	}
	else
	{
		players_.at(0)->SetPos_y( players_.at(0)->GetPos_y() + (int) (5.0f * timeDiff) );
	}
}

void myApplication::moveMeLeftRight(bool mode, float timeDiff, string player)
{
	int i = 0;
	if (player == "player1")
		i = 0;
	else 
		i = 1;

	players_.at(i)->SetMovingAnimation(players_.at(i)->GetMovingAnimation() + 1);
	if (mode)
	{
		players_.at(i)->SetPos_x( players_.at(i)->GetPos_x() - (int) (5.0f * timeDiff) );

		if (players_.at(i)->GetTextureType() == 0)
		{
			players_.at(i)->SetAnimationInvert( true );
			if (players_.at(i)->GetMovingAnimation() > 7)
			{
				players_.at(i)->SetMovingAnimation(0);
				players_.at(i)->SetAnimationCounter( players_.at(i)->GetAnimationCounter() + 1);
			}
				if (players_.at(i)->GetAnimationCounter()>8)
					players_.at(i)->SetAnimationCounter( 0 );
		}
	}
	else 
	{
		players_.at(i)->SetPos_x( players_.at(i)->GetPos_x() + (int) (5.0f * timeDiff) );
		if (players_.at(i)->GetTextureType() == 0)
		{
			players_.at(i)->SetAnimationInvert( false );
			if (players_.at(i)->GetMovingAnimation() > 7)
			{
				players_.at(i)->SetMovingAnimation(0);
				players_.at(i)->SetAnimationCounter( players_.at(i)->GetAnimationCounter() - 1);
			}
				if (players_.at(i)->GetAnimationCounter()==8)
					players_.at(i)->SetAnimationCounter( 0 );
		}
	}
}

void myApplication::moveMeJump(string player)
{
	int i = 0;
	if (player == "player1")
		i = 0;
	else
		i = 1;

	if (players_.at(i)->isOnGround())
	{
		players_.at(i)->SetToJumpUpwards( true );
	}
}

// Check for collision of hero with obstacles in a certain position
bool myApplication::CheckCollision(int pos_x, int pos_y,
		bool m_bCheckUpwards, bool m_bCheckDownwards,
		bool m_bCheckLeft, bool m_bCheckRight)
{
	// The pos_x and pos_y are the top left corner of the hero, so we find the tile which this position occupies
	int tile_top_y = (int) floor( (float) (pos_y) / TILESIZE);
	int tile_bot_y = (int) floor( (float) (pos_y+24) / TILESIZE);
	int tile_bot2_y = (int) floor( (float) (pos_y+25) / TILESIZE);

	int tile_outerleft_x = (int) floor( (float) (mapOffset_x+pos_x-1) / TILESIZE);
	int tile_innerleft_x = (int) floor( (float) (mapOffset_x+pos_x) / TILESIZE);

	int tile_outerright_x = (int) floor( (float) (mapOffset_x+pos_x+25) / TILESIZE);
	int tile_innerright_x = (int) floor( (float) (mapOffset_x+pos_x+24) / TILESIZE);

	// Collision for tile
	if (m_bCheckLeft)
	{
		if (theMap->theScreenMap[tile_top_y][tile_outerleft_x] == 1 || theMap->theScreenMap[tile_top_y][tile_outerleft_x] == 2)
			return true;
		if (theMap->theScreenMap[tile_bot_y][tile_outerleft_x] == 1 || theMap->theScreenMap[tile_bot_y][tile_outerleft_x] == 2)
			return true;
	}
	if (m_bCheckRight)
	{
 		if (theMap->theScreenMap[tile_top_y][tile_outerright_x] == 1 || theMap->theScreenMap[tile_top_y][tile_outerright_x] == 2)
			return true;
		if (theMap->theScreenMap[tile_bot_y][tile_outerright_x] == 1 || theMap->theScreenMap[tile_bot_y][tile_outerright_x] == 2)
			return true;
	}

	if (m_bCheckUpwards)
	{
		if (theMap->theScreenMap[tile_top_y][tile_innerleft_x] == 1 || theMap->theScreenMap[tile_top_y][tile_innerleft_x] == 2)
			return true;
		if (theMap->theScreenMap[tile_top_y][tile_innerright_x] == 1 || theMap->theScreenMap[tile_top_y][tile_innerright_x] == 2)
			return true;
	}

	if (m_bCheckDownwards)
	{
		if (theMap->theScreenMap[tile_bot2_y][tile_innerleft_x] == 1 || theMap->theScreenMap[tile_bot2_y][tile_innerleft_x] == 2)
			return true;
		if (theMap->theScreenMap[tile_bot2_y][tile_innerright_x] == 1 || theMap->theScreenMap[tile_bot2_y][tile_innerright_x] == 2)
			return true;
	}

	return false;
}

void myApplication::RenderGoodies(void)
{
	glPushMatrix();
		glTranslatef(theArrayOfGoodies->GetPosX() - mapOffset_x, theArrayOfGoodies->GetPosY(), 0);
		glEnable(GL_TEXTURE_2D);
		glEnable(GL_BLEND);
		glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glBindTexture(GL_TEXTURE_2D, theArrayOfGoodies->GoodiesTexture[ 0 ].texID);
		glBegin(GL_QUADS);
			glTexCoord2f(0,1); glVertex2f(0,0);
			glTexCoord2f(0,0); glVertex2f(0,TILE_SIZE);
			glTexCoord2f(1,0); glVertex2f(TILE_SIZE,TILE_SIZE);
			glTexCoord2f(1,1); glVertex2f(TILE_SIZE,0);
		glEnd();
		glDisable(GL_BLEND);
		glDisable(GL_TEXTURE_2D);
		glPopMatrix();
}

// Render some rear wall
void myApplication::RenderRearWalls()
{
	rearWallOffset_x = (int) (mapOffset_x / 2);
	rearWallOffset_y = 0;
	rearWalltileOffset_y = 0;
	rearWalltileOffset_x = (int) (rearWallOffset_x /TILE_SIZE);
	if (rearWalltileOffset_x+theRear->getNumOfTiles_ScreenWidth() > theRear->getNumOfTiles_MapWidth())
		rearWalltileOffset_x = theRear->getNumOfTiles_MapWidth() - theRear->getNumOfTiles_ScreenWidth();
	rearWallfineOffset_x = rearWallOffset_x % TILE_SIZE;

	glPushMatrix();
	for (int i = 0; i < theNumOfTiles_RearHeight; i ++)
	{
		for (int k = 0; k < theNumOfTiles_RearWidth+1; k ++)
		{
			if ((tileOffset_x+k) >= theRear->getNumOfTiles_MapWidth())
				break;
			glPushMatrix();
			glTranslatef(k*TILE_SIZE-rearWallfineOffset_x, i*TILE_SIZE-100, 0);
			glEnable(GL_TEXTURE_2D);
			glEnable(GL_BLEND);
			glColor4f(0.7f, 0.7f, 0.7f, 0.8f);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glBindTexture(GL_TEXTURE_2D, TileMapTexture[theRear->theScreenMap[i][rearWalltileOffset_x+k]].texID);
			glBegin(GL_QUADS);
				glTexCoord2f(0,1); glVertex2f(0,0);
				glTexCoord2f(0,0); glVertex2f(0,TILE_SIZE);
				glTexCoord2f(1,0); glVertex2f(TILE_SIZE,TILE_SIZE);
				glTexCoord2f(1,1); glVertex2f(TILE_SIZE,0);
			glEnd();
			glDisable(GL_BLEND);
			glDisable(GL_TEXTURE_2D);
			glPopMatrix();
		}
	}
	glPopMatrix();
}

// Render some rear wall
void myApplication::RenderMiddleWalls()
{
	MiddleWallOffset_x = (int) (mapOffset_x / 1.5);
	MiddleWallOffset_y = 0;
	MiddleWalltileOffset_y = 0;
	MiddleWalltileOffset_x = (int) (MiddleWallOffset_x /TILE_SIZE);
	if (MiddleWalltileOffset_x+theMiddle->getNumOfTiles_ScreenWidth() > theMiddle->getNumOfTiles_MapWidth())
		MiddleWalltileOffset_x = theMiddle->getNumOfTiles_MapWidth() - theMiddle->getNumOfTiles_ScreenWidth();
	MiddleWallfineOffset_x = MiddleWallOffset_x % TILE_SIZE;

	glPushMatrix();
	for (int i = 0; i < theNumOfTiles_MiddleHeight; i ++)
	{
		for (int k = 0; k < theNumOfTiles_MiddleWidth+1; k ++)
		{
			if ((tileOffset_x+k) >= theMiddle->getNumOfTiles_MapWidth())
				break;
			glPushMatrix();
			glTranslatef(k*TILE_SIZE-MiddleWallfineOffset_x, i*TILE_SIZE-100, 0);
			glEnable(GL_TEXTURE_2D);
			glEnable(GL_BLEND);
			glColor4f(0.7f, 0.7f, 0.7f, 0.8f);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glBindTexture(GL_TEXTURE_2D, TileMapTexture[theMiddle->theScreenMap[i][MiddleWalltileOffset_x+k]].texID);
			glBegin(GL_QUADS);
				glTexCoord2f(0,1); glVertex2f(0,0);
				glTexCoord2f(0,0); glVertex2f(0,TILE_SIZE);
				glTexCoord2f(1,0); glVertex2f(TILE_SIZE,TILE_SIZE);
				glTexCoord2f(1,1); glVertex2f(TILE_SIZE,0);
			glEnd();
			glDisable(GL_BLEND);
			glDisable(GL_TEXTURE_2D);
			glPopMatrix();
		}
	}
	glPopMatrix();
}

/****************************************************************************************************
   Draw the background
 ****************************************************************************************************/
void myApplication::RenderBackground(void) {
	glEnable(GL_TEXTURE_2D);

	// Draw Background image
	glPushMatrix();
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		if (theMap->getmapName() == "level1.csv")
			glBindTexture(GL_TEXTURE_2D, BackgroundTexture[0].texID );
		else if (theMap->getmapName() == "level2.csv")
			glBindTexture(GL_TEXTURE_2D, BackgroundTexture[4].texID );
		else if (theMap->getmapName() == "level3.csv")
			glBindTexture(GL_TEXTURE_2D, BackgroundTexture[5].texID );
		else if (theMap->getmapName() == "level4.csv")
			glBindTexture(GL_TEXTURE_2D, BackgroundTexture[6].texID );
		else
			glBindTexture(GL_TEXTURE_2D, BackgroundTexture[0].texID );
		glPushMatrix();
			glBegin(GL_QUADS);
				int height = 100 * 1.333/1.5;
				if (theMap->getmapName() == "level1.csv")
				{
					glTexCoord2f(0,0); glVertex2f(0 - mapOffset_x,600);
					glTexCoord2f(1,0); glVertex2f(800 - mapOffset_x,600);
					glTexCoord2f(1,1); glVertex2f(800 - mapOffset_x,100);
					glTexCoord2f(0,1); glVertex2f(0 - mapOffset_x,100);		

					glTexCoord2f(1,0); glVertex2f(800- mapOffset_x,600);
					glTexCoord2f(0,0); glVertex2f(1600- mapOffset_x,600);
					glTexCoord2f(0,1); glVertex2f(1600- mapOffset_x,100);
					glTexCoord2f(1,1); glVertex2f(800- mapOffset_x,100);	
				}
				else if (theMap->getmapName() == "level2.csv")
				{
					glTexCoord2f(0,0); glVertex2f(0 - mapOffset_x,500);
					glTexCoord2f(1,0); glVertex2f(600 - mapOffset_x,500);
					glTexCoord2f(1,1); glVertex2f(600 - mapOffset_x,100);
					glTexCoord2f(0,1); glVertex2f(0 - mapOffset_x,100);		

					glTexCoord2f(1,0); glVertex2f(600- mapOffset_x,500);
					glTexCoord2f(0,0); glVertex2f(1050- mapOffset_x,500);
					glTexCoord2f(0,1); glVertex2f(1050- mapOffset_x,100);
					glTexCoord2f(1,1); glVertex2f(600- mapOffset_x,100);	
				}
				else if (theMap->getmapName() == "level3.csv")
				{
					glTexCoord2f(0,0); glVertex2f(125 - mapOffset_x,500);
					glTexCoord2f(1,0); glVertex2f(725 - mapOffset_x,500);
					glTexCoord2f(1,1); glVertex2f(725 - mapOffset_x,150);
					glTexCoord2f(0,1); glVertex2f(125 - mapOffset_x,150);		

					glTexCoord2f(0,0); glVertex2f(725- mapOffset_x,500);
					glTexCoord2f(1,0); glVertex2f(1325- mapOffset_x,500);
					glTexCoord2f(1,1); glVertex2f(1325- mapOffset_x,150);
					glTexCoord2f(0,1); glVertex2f(725- mapOffset_x,150);

				}
				else if (theMap->getmapName() == "level4.csv")
				{
					glTexCoord2f(0,0); glVertex2f(0 - mapOffset_x,500);
					glTexCoord2f(1,0); glVertex2f(575 - mapOffset_x,500);
					glTexCoord2f(1,1); glVertex2f(575 - mapOffset_x,100);
					glTexCoord2f(0,1); glVertex2f(0 - mapOffset_x,100);			
				}
				else
				{
					glTexCoord2f(0,0); glVertex2f(0 - mapOffset_x,600);
					glTexCoord2f(1,0); glVertex2f(800 - mapOffset_x,600);
					glTexCoord2f(1,1); glVertex2f(800 - mapOffset_x,100);
					glTexCoord2f(0,1); glVertex2f(0 - mapOffset_x,100);		

					glTexCoord2f(1,0); glVertex2f(800- mapOffset_x,600);
					glTexCoord2f(0,0); glVertex2f(1600- mapOffset_x,600);
					glTexCoord2f(0,1); glVertex2f(1600- mapOffset_x,100);
					glTexCoord2f(1,1); glVertex2f(800- mapOffset_x,100);	
				}
			glEnd();
		glPopMatrix();
		glDisable(GL_BLEND);
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);

	if (theMap->getmapName() == "level1.csv")
	{
		glEnable(GL_TEXTURE_2D);
		glPushMatrix();
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glBindTexture(GL_TEXTURE_2D, BackgroundTexture[14].texID );
		glPushMatrix();
		glBegin(GL_QUADS);
			glTexCoord2f(0,0); glVertex2f(1400- mapOffset_x,500);
			glTexCoord2f(1,0); glVertex2f(1600- mapOffset_x,500);
			glTexCoord2f(1,1); glVertex2f(1600- mapOffset_x,120);
			glTexCoord2f(0,1); glVertex2f(1400- mapOffset_x,120);
		glEnd();
		glPopMatrix();
		glDisable(GL_BLEND);
		glPopMatrix();
		glDisable(GL_TEXTURE_2D);
	}

	if (theMap->getmapName() == "level2.csv")
	{
		glEnable(GL_TEXTURE_2D);
		glPushMatrix();
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glBindTexture(GL_TEXTURE_2D, BackgroundTexture[6].texID );
		glPushMatrix();
		glBegin(GL_QUADS);
		int height = 100 * 1.333/1.5;
			glTexCoord2f(0,0); glVertex2f(1050- mapOffset_x,500);
			glTexCoord2f(1,0); glVertex2f(1600- mapOffset_x,500);
			glTexCoord2f(1,1); glVertex2f(1600- mapOffset_x,100);
			glTexCoord2f(0,1); glVertex2f(1050- mapOffset_x,100);
		glEnd();
		glPopMatrix();
		glDisable(GL_BLEND);
		glPopMatrix();
		glDisable(GL_TEXTURE_2D);
	}

	if (theMap->getmapName() == "level3.csv")
	{
		glEnable(GL_TEXTURE_2D);
		glPushMatrix();
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
				glBindTexture(GL_TEXTURE_2D, BackgroundTexture[12].texID );
			glPushMatrix();
				glBegin(GL_QUADS);
					int height = 100 * 1.333/1.5;
						glTexCoord2f(0,0); glVertex2f(0 - mapOffset_x,600);
						glTexCoord2f(1,0); glVertex2f(800 - mapOffset_x,600);
						glTexCoord2f(1,1); glVertex2f(800 - mapOffset_x,500);
						glTexCoord2f(0,1); glVertex2f(0 - mapOffset_x,500);		

						glTexCoord2f(1,0); glVertex2f(800- mapOffset_x,600);
						glTexCoord2f(0,0); glVertex2f(1600- mapOffset_x,600);
						glTexCoord2f(0,1); glVertex2f(1600- mapOffset_x,500);
						glTexCoord2f(1,1); glVertex2f(800- mapOffset_x,500);	
				glEnd();
			glPopMatrix();
			glDisable(GL_BLEND);
		glPopMatrix();
		glDisable(GL_TEXTURE_2D);
	}

	if (theMap->getmapName() == "level4.csv")
	{
		glEnable(GL_TEXTURE_2D);
		glPushMatrix();
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glBindTexture(GL_TEXTURE_2D, BackgroundTexture[4].texID );
		glPushMatrix();
		glBegin(GL_QUADS);
			glTexCoord2f(0,0); glVertex2f(575 - mapOffset_x,500);
			glTexCoord2f(1,0); glVertex2f(1175 - mapOffset_x,500);
			glTexCoord2f(1,1); glVertex2f(1175 - mapOffset_x,100);
			glTexCoord2f(0,1); glVertex2f(575 - mapOffset_x,100);

			glTexCoord2f(1,0); glVertex2f(1175 - mapOffset_x,500);
			glTexCoord2f(0,0); glVertex2f(1775 - mapOffset_x,500);
			glTexCoord2f(0,1); glVertex2f(1775 - mapOffset_x,100);
			glTexCoord2f(1,1); glVertex2f(1175 - mapOffset_x,100);
		glEnd();
		glPopMatrix();
		glDisable(GL_BLEND);
		glPopMatrix();
		glDisable(GL_TEXTURE_2D);
	}
						
}

void myApplication::RenderAutosave(void) 
{
	if (!Blinking)
		blink-=0.01f;
	else
		blink+=0.01f;

	if (blink <= 0.0f && !Blinking)
		Blinking = true;
	else if (blink >= 1.0f && Blinking)
		Blinking = false;

	AutoSaveTime += 0.1f;
	if (AutoSaveTime > 50.0f)
		AutoSave = false;
	// Draw Background image
	glEnable(GL_TEXTURE_2D);
	glPushMatrix();
	glTranslatef(55, -45, 0);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glBindTexture(GL_TEXTURE_2D, BackgroundTexture[8].texID );
			glColor4f(1.0f, 1.0f, 1.0f, blink);
		glPushMatrix();
			glBegin(GL_QUADS);		
				glTexCoord2f(0,0); glVertex2f(250,130);
				glTexCoord2f(1,0); glVertex2f(430,130);
				glTexCoord2f(1,1); glVertex2f(430,100);
				glTexCoord2f(0,1); glVertex2f(250,100);	
			glEnd();
		glPopMatrix();
		glDisable(GL_BLEND);
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);
}
/****************************************************************************************************
   Draw the tile map
 ****************************************************************************************************/
void myApplication::RenderTileMap(void) {
	mapFineOffset_x = mapOffset_x % TILE_SIZE;
	glPushMatrix();
	for(int i = 0; i < theNumOfTiles_Height; i ++)
	{
		for(int k = 0; k < theNumOfTiles_Width + 1; k ++)
		{
			if ((tileOffset_x + k) >= theMap->getNumOfTiles_MapWidth())
				break;
			glPushMatrix();
			glTranslatef(k*TILE_SIZE - mapFineOffset_x, i*TILE_SIZE, 0);
			glEnable( GL_TEXTURE_2D );
			glEnable( GL_BLEND );
			glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glBindTexture( GL_TEXTURE_2D, TileMapTexture[theMap->theScreenMap[i][tileOffset_x+k]].texID );
			glBegin(GL_QUADS);
				glTexCoord2f(0,1); glVertex2f(0,0);
				glTexCoord2f(0,0); glVertex2f(0,TILE_SIZE);
				glTexCoord2f(1,0); glVertex2f(TILE_SIZE,TILE_SIZE);
				glTexCoord2f(1,1); glVertex2f(TILE_SIZE,0);
			glEnd();
			glDisable( GL_BLEND );
			glDisable( GL_TEXTURE_2D );
			glPopMatrix();
		}
	}
	glPopMatrix();
}

void myApplication::RenderUI() // render the user interface, game board
{
	printw( 25.0, 75.0, 0, "PLAYER ");
	if (players == TWOPLAYER)
		printw( 25.0, 75.0, 0, "PLAYER1 ");
	if (players == TWOPLAYER)
		printw( 270.0, 75.0, 0, "PLAYER2 ");
	printw( 25.0, 100.0, 0, "ENEMY ");
	if (timer->GetSec() > 9)
		printw( 350.0, 50.0, 0, "TIME   %i:%i", timer->GetMin(), timer->GetSec());
	else
		printw( 350.0, 50.0, 0, "TIME   %i:0%i", timer->GetMin(), timer->GetSec());

	if (theMap->getmapName() == "level1.csv")
		printw( 650.0, 50.0, 0, "STAGE    01");
	else
		printw( 650.0, 50.0, 0, "STAGE    02");

	printw( 615.0, 79.0, 0, "  - %i", coin);

	printw( 600.0, 110.0, 0, "P - ");

	printw (25.0, 50.0, 0, "SCORE - %i", score);

	glPushMatrix();
	glTranslatef(130, 15, 0);
	glPushMatrix();
	glLineWidth(4.5);
	glTranslatef(385,56,0);
	glColor3f(1.0f, 0.0f, 0.0f);
	glBegin(GL_LINES);
	glVertex3f(0.0, 40.0, 0.0);
	glVertex3f(60, 40, 0);
	glEnd();
	glPopMatrix();

	glPushMatrix();
	glLineWidth(4.5);
	glTranslatef(385,7,0);
	glColor3f(1.0f, 0.0f, 0.0f);
	glBegin(GL_LINES);
	glVertex3f(0.0, 40.0, 0.0);
	glVertex3f(60, 40, 0);
	glEnd();
	glPopMatrix();

	glPushMatrix();
	glLineWidth(4.5);
	glTranslatef(425,45,0);
	glRotatef(90,0, 0,1);
	glColor3f(1.0f, 0.0f, 0.0f);
	glBegin(GL_LINES);
	glVertex3f(0.0, 40.0, 0.0);
	glVertex3f(53, 40, 0);
	glEnd();
	glPopMatrix();

	glPushMatrix();
	glLineWidth(4.5);
	glTranslatef(485,45,0);
	glRotatef(90,0, 0,1);
	glColor3f(1.0f, 0.0f, 0.0f);
	glBegin(GL_LINES);
	glVertex3f(0.0, 40.0, 0.0);
	glVertex3f(53, 40, 0);
	glEnd();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(400,56,0);
	glEnable( GL_TEXTURE_2D ); 
	glEnable( GL_BLEND ); 
	glColor4f(1.0f, 1.0f, 1.0f, 1.0f); 
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); 
	if (theMap->getmapName() != "level1.csv")
	{
		glBindTexture( GL_TEXTURE_2D, UITexture[3].texID); 
		glBegin(GL_QUADS); 
		glTexCoord2f(0,1); glVertex2f(0,0);
		glTexCoord2f(0,0); glVertex2f(0,TILE_SIZE); 
		glTexCoord2f(1,0); glVertex2f(TILE_SIZE,TILE_SIZE); 
		glTexCoord2f(1,1); glVertex2f(TILE_SIZE,0); 
		glEnd(); 
	}
	glDisable( GL_BLEND ); 
	glDisable( GL_TEXTURE_2D ); 
	glPopMatrix();
	glPopMatrix();

	glPushMatrix();
	glTranslatef(600,59,0);
	glEnable( GL_TEXTURE_2D ); 
	glEnable( GL_BLEND ); 
	glColor4f(1.0f, 1.0f, 1.0f, 1.0f); 
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); 
	glBindTexture( GL_TEXTURE_2D, UITexture[1].texID); 
	glBegin(GL_QUADS); 
	glTexCoord2f(0,1); glVertex2f(0,0);
	glTexCoord2f(0,0); glVertex2f(0,TILE_SIZE); 
	glTexCoord2f(1,0); glVertex2f(TILE_SIZE,TILE_SIZE); 
	glTexCoord2f(1,1); glVertex2f(TILE_SIZE,0); 
	glEnd(); 
	glDisable( GL_BLEND ); 
	glDisable( GL_TEXTURE_2D ); 
	glPopMatrix();

}


//-------------------------------------------------------------------------
// Calculates the frames per second
//-------------------------------------------------------------------------
void myApplication::calculateFPS()
{
	//  Increase frame count
	frameCount++;

	//  Get the number of milliseconds since glutInit called
	//  (or first call to glutGet(GLUT ELAPSED TIME)).
	currentTime = glutGet(GLUT_ELAPSED_TIME);

	//  Calculate time passed
	int timeInterval = currentTime - previousTime;

	if(timeInterval > 1000)
	{
		//  calculate the number of frames per second
		fps = frameCount / (timeInterval / 2000.0f);

		//  Set time
		previousTime = currentTime;

		//  Reset frame count
		frameCount = 0;
	}
}

//-------------------------------------------------------------------------
//  Draw FPS
//-------------------------------------------------------------------------
void myApplication::drawFPS()
{
	//  Load the identity matrix so that FPS string being drawn
	//  won't get animates
	glLoadIdentity ();

	//  Print the FPS to the window
	glColor3f( 1.0f, 1.0f, 1.0f);
	printw (25.0, 25.0, 0, "FPS: %4.2f", fps);
}

//-------------------------------------------------------------------------
//  Draws a string at the specified coordinates.
//-------------------------------------------------------------------------
void myApplication::printw (float x, float y, float z, char* format, ...)
{
	va_list args;	//  Variable argument list
	int len;		//	String length
	int i;			//  Iterator
	char * text;	//	Text

	//  Initialize a variable argument list
	va_start(args, format);

	//  Return the number of characters in the string referenced the list of arguments.
	//  _vscprintf doesn't count terminating '\0' (that's why +1)
	len = _vscprintf(format, args) + 1; 

	//  Allocate memory for a string of the specified size
	text = (char *)malloc(len * sizeof(char));

	//  Write formatted output using a pointer to the list of arguments
	vsprintf_s(text, len, format, args);

	//  End using variable argument list 
	va_end(args);

	//  Specify the raster position for pixel operations.
	glRasterPos3f (x, y, z);


	//  Draw the characters one by one
	for (i = 0; text[i] != '\0'; i++)
		glutBitmapCharacter(font_style, text[i]);

	//  Free the allocated memory for the string
	free(text);
}


/****************************************************************************************************
   Draw hp
 ****************************************************************************************************/
//Render Health 
void myApplication::Renderhealth(void) 
{ 
	glPushMatrix();
	for (int i=0; i<players_.at(0)->GetHealth(); i++) 
	{
		glPushMatrix();
		glTranslatef(theArrayOfHealth[i]->GetPosX() - 120, theArrayOfHealth[i]->GetPosY() + 55, 0); 
		glTranslatef(100,0,0);
		glEnable( GL_TEXTURE_2D ); 
		glEnable( GL_BLEND ); 
		glColor4f(1.0f, 1.0f, 1.0f, 1.0f); 
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); 
		glBindTexture( GL_TEXTURE_2D, theArrayOfHealth[i]->GoodiesTexture[1].texID ); 
		glBegin(GL_QUADS); 
		glTexCoord2f(0,1); glVertex2f(0,0);
		glTexCoord2f(0,0); glVertex2f(0,TILE_SIZE); 
		glTexCoord2f(1,0); glVertex2f(TILE_SIZE,TILE_SIZE); 
		glTexCoord2f(1,1); glVertex2f(TILE_SIZE,0); 
		glEnd(); 
		glDisable( GL_BLEND ); 
		glDisable( GL_TEXTURE_2D ); 
		glPopMatrix(); 
	}
	if (players == TWOPLAYER)
	{
		for (int i=0; i<players_.at(1)->GetHealth(); i++) 
		{
			glPushMatrix();
			glTranslatef(theArrayOfHealth[i]->GetPosX() + 125, theArrayOfHealth[i]->GetPosY() + 55, 0); 
			glTranslatef(100,0,0);
			glEnable( GL_TEXTURE_2D ); 
			glEnable( GL_BLEND ); 
			glColor4f(1.0f, 1.0f, 1.0f, 1.0f); 
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); 
			glBindTexture( GL_TEXTURE_2D, theArrayOfHealth[i]->GoodiesTexture[1].texID ); 
			glBegin(GL_QUADS); 
			glTexCoord2f(0,1); glVertex2f(0,0);
			glTexCoord2f(0,0); glVertex2f(0,TILE_SIZE); 
			glTexCoord2f(1,0); glVertex2f(TILE_SIZE,TILE_SIZE); 
			glTexCoord2f(1,1); glVertex2f(TILE_SIZE,0); 
			glEnd(); 
			glDisable( GL_BLEND ); 
			glDisable( GL_TEXTURE_2D ); 
			glPopMatrix(); 
		}
	}

	for (std::vector<Enemies*>::iterator enemy = enemies_.begin();
		enemy != enemies_.end(); enemy++)
	{
		if ((*enemy)->GetID() == 3 || (*enemy)->GetID() == 4)
		{
			for (int i=0; i<(*enemy)->GetHealth(); i++) 
			{
				glPushMatrix();
					glTranslatef(theArrayOfHealth[i]->GetPosX() - 120, theArrayOfHealth[i]->GetPosY() + 55, 0); 
					glTranslatef(100,27,0);
					glEnable( GL_TEXTURE_2D ); 
					glEnable( GL_BLEND ); 
						glColor4f(0.65f, 0.46f, 0.12f, 1.0f); 
						glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); 
						glBindTexture( GL_TEXTURE_2D, theArrayOfHealth[i]->GoodiesTexture[1].texID ); 
						glBegin(GL_QUADS); 
						glTexCoord2f(0,1); glVertex2f(0,0);
						glTexCoord2f(0,0); glVertex2f(0,TILE_SIZE); 
						glTexCoord2f(1,0); glVertex2f(TILE_SIZE,TILE_SIZE); 
						glTexCoord2f(1,1); glVertex2f(TILE_SIZE,0); 
					glEnd(); 
					glDisable( GL_BLEND ); 
					glDisable( GL_TEXTURE_2D ); 
				glPopMatrix(); 
			}
		}
	}
	glColor3f(1,1,1);
	glPopMatrix();
}

/****************************************************************************************************
   Draw potion
 ****************************************************************************************************/
void myApplication::Drawpotion(void)
{
	glLoadIdentity();
	glPushAttrib(GL_ALL_ATTRIB_BITS);
	glColor3f(1.0f, 1.0f, 1.0f);
	printw(55.0f, 160.0f, 0.0f, ":%i", potion);
	glPopAttrib();
}

/****************************************************************************************************
   Draw highscore
 ****************************************************************************************************/
void myApplication::RenderHighscore()
{
	glEnable(GL_TEXTURE_2D);

		// Draw Background image
		glPushMatrix();
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glBindTexture(GL_TEXTURE_2D, BackgroundTexture[3].texID );
			glPushMatrix();
				glBegin(GL_QUADS);
					glTexCoord2f(0,0); glVertex2f(0,600);
					glTexCoord2f(1,0); glVertex2f(800,600);
					glTexCoord2f(1,1); glVertex2f(800,0);
					glTexCoord2f(0,1); glVertex2f(0,0);				
				glEnd();
			glPopMatrix();
			glDisable(GL_BLEND);
		glPopMatrix();
		glDisable(GL_TEXTURE_2D);


}
/****************************************************************************************************
   Draw control
 ****************************************************************************************************/
void myApplication::RenderControl()
{
	glEnable(GL_TEXTURE_2D);

		// Draw Background image
		glPushMatrix();
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glBindTexture(GL_TEXTURE_2D, BackgroundTexture[7].texID );
			glPushMatrix();
				glBegin(GL_QUADS);
					glTexCoord2f(0,0); glVertex2f(0,600);
					glTexCoord2f(1,0); glVertex2f(800,600);
					glTexCoord2f(1,1); glVertex2f(800,0);
					glTexCoord2f(0,1); glVertex2f(0,0);				
				glEnd();
			glPopMatrix();
			glDisable(GL_BLEND);
		glPopMatrix();
		glDisable(GL_TEXTURE_2D);
}

/****************************************************************************************************
   Draw key
 ****************************************************************************************************/
void myApplication::Drawkey(void)
{
glLoadIdentity();
glPushAttrib(GL_ALL_ATTRIB_BITS);
glColor3f(1.0f, 1.0f, 1.0f);
printw(55.0f, 190.0f, 0.0f, ":%i", key);
glPopAttrib();
}

/****************************************************************************************************
   Draw smoke
 ****************************************************************************************************/
void myApplication::Rendersmoke(void)
{
	glEnable(GL_TEXTURE_2D);
			glPushMatrix();
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glBindTexture(GL_TEXTURE_2D, smokeTexture[0].texID );
			glPushMatrix();
				glTranslatef(278.0f, 450.0f - Scount, 0.0f);
				glScalef(Scount, Scount, 0.0f);
				glBegin(GL_QUADS);
					glTexCoord2f(0,0); glVertex2f(0,25);
					glTexCoord2f(1,0); glVertex2f(25,25);
					glTexCoord2f(1,1); glVertex2f(25,0);
					glTexCoord2f(0,1); glVertex2f(0,0);				
				glEnd();
			glPopMatrix();
			glDisable(GL_BLEND);
		glPopMatrix();
		glDisable(GL_TEXTURE_2D);
	
}
/****************************************************************************************************
   Enter Name
 ****************************************************************************************************/
void myApplication::EnterName()
{
	// Get letter input from user
	for(int i = 0; i <= 126; i ++)
	{
		if (myKeys[i] == true && GetOut == false)
		{
			if (i == 8)
			{
				if (Size != 0)
				{
					Size -= 1;
					SName[Size] = ' ';
				}
			}
			else
			{
				CName = char(i);
				SName[Size] = CName;
				// Make sure size doesn't go out of array
				if (Size > 20)
					Size = 20;
				else
					Size ++;
			}

			stringstream ss;
			
			for (int i = 0; i < Size; i ++)
			{
				// stringstream each letter pressed
				if (SName[i] != -1)
					ss << SName[i];
			}

			// Set char array into string
			Name = ss.str();

			// Player cant press and hold letter to input
			GetOut = true;
		}
	}
	
	// Print out array of name
	string msg = Name;
	PrintName = const_cast<char*> ( msg.c_str() );

	for(int i = 0; i < 20; i ++)
	{
		if (SName[i] != -1)
			printw (400.0, 365.0, 0, "%s", PrintName);
		else
			printw (400.0, 365.0, 0, " ");
	}
}

void myApplication::SpawnEnemies(int num, int type, int sec)
{
	if (spawntimer.GetSec() < 1)
	{
		Spawn = true;
		Counter = 0;
		Delay = 0;
		spawntimer.Init(0, sec);
	}

	if (Spawn)
	{
		Delay ++;
		if (Delay == 40 && Counter < num)
		{
			Counter++;
			Delay = 0;
			enemies_.push_back(new Enemies(-15 + mapOffset_x, 450, 1, type, 2, Enemies::ATTACK));
			enemies_.push_back(new Enemies(800 + mapOffset_x, 450, 1, type, 1, Enemies::ATTACK));
		}

		if (Counter == 3)
			Spawn = false;
	}
}

void myApplication::AnimationAttack()
{
	// Animation
	if (myKeys['q'] == true && !players_.at(0)->GetKeyDown('q') || myKeys['e'] == true && !players_.at(0)->GetKeyDown('e'))
	{
		players_.at(0)->SetAnimationStop(false);
		players_.at(0)->SetCounter(0);
		players_.at(0)->SetAnimationCounter(0);
		players_.at(0)->SetMovingAnimation(0);
		if (myKeys['q'] == true)
			players_.at(0)->SetKeyDown('q', true);
		else if (myKeys['e'] == true)
			players_.at(0)->SetKeyDown('e', true);
	}

	if (players_.at(0)->GetKeyDown('q') || players_.at(0)->GetKeyDown('e'))
	{
		if (!players_.at(0)->GetAnimationStop())
		{	
			players_.at(0)->SetCounter(players_.at(0)->GetCounter() + 1);
			if (players_.at(0)->GetKeyDown('q'))
				players_.at(0)->Animate(WHIP);
			else if (players_.at(0)->GetKeyDown('e'))
			{
				if (theMap->getmapName() != "level1.csv")
					players_.at(0)->Animate(KNIFE);
			}
		}
		else
		{
			players_.at(0)->SetCounter(0);
			if (players_.at(0)->GetKeyDown('q'))
				players_.at(0)->SetKeyDown('q', false);
			else if (players_.at(0)->GetKeyDown('e'))
				players_.at(0)->SetKeyDown('e', false);
		}
	}

	// Attack Part
	if (players_.at(0)->GetCounter() > 30)
	{
		players_.at(0)->SetCounter(0);
		if (players_.at(0)->GetKeyDown('q'))
		{
			Whip->WhipAttack(players_.at(0), enemies_, 1, mapOffset_x, score);
			players_.at(0)->ResetAnimation();
			players_.at(0)->SetKeyDown('q', false);
			Audio->PlaySFX(0);
		}
		else if (players_.at(0)->GetKeyDown('e'))
		{
			if (theMap->getmapName() != "level1.csv")
			{
				knife_.push_back(new Weapon(players_.at(0), 2));
				players_.at(0)->ResetAnimation();
				if (players_.at(0)->GetKeyDown('e'))
					players_.at(0)->SetKeyDown('e', false);
			}
		}
	}
}

void myApplication::AnimationAttack2()
{
	// Animation
	if (myKeys['p'] == true && !players_.at(1)->GetKeyDown('p') || myKeys['u'] == true && !players_.at(1)->GetKeyDown('u'))
	{
		players_.at(1)->SetAnimationStop(false);
		players_.at(1)->SetCounter(0);
		players_.at(1)->SetAnimationCounter(0);
		players_.at(1)->SetMovingAnimation(0);
		if (myKeys['p'] == true)
			players_.at(1)->SetKeyDown('p', true);
		else if (myKeys['u'] == true)
			players_.at(1)->SetKeyDown('u', true);
	}

	if (players_.at(1)->GetKeyDown('q') || players_.at(1)->GetKeyDown('e') ||
		players_.at(1)->GetKeyDown('u') || players_.at(1)->GetKeyDown('p'))
	{
		if (!players_.at(1)->GetAnimationStop())
		{	
			players_.at(1)->SetCounter(players_.at(1)->GetCounter() + 1);
			if (players_.at(1)->GetKeyDown('p'))
				players_.at(1)->Animate(WHIP);
			else if (players_.at(1)->GetKeyDown('u'))
			{
				if (theMap->getmapName() != "level1.csv")
					players_.at(1)->Animate(KNIFE);
			}
		}
		else
		{
			players_.at(1)->SetCounter(0);
			if (players_.at(1)->GetKeyDown('u'))
				players_.at(1)->SetKeyDown('u', false);
			else if (players_.at(1)->GetKeyDown('p'))
				players_.at(1)->SetKeyDown('p', false);
		}
	}

	// Attack Part
	if (players_.at(1)->GetCounter() > 30)
	{
		players_.at(1)->SetCounter(0);
		if (players_.at(1)->GetKeyDown('p'))
		{
			Whip->WhipAttack(players_.at(1), enemies_, 1, mapOffset_x, score);
			players_.at(1)->ResetAnimation();
			players_.at(1)->SetKeyDown('p', false);
			Audio->PlaySFX(0);
		}
		else if (players_.at(1)->GetKeyDown('u'))
		{
			if (theMap->getmapName() != "level1.csv")
			{
				knife_.push_back(new Weapon(players_.at(1), 2));
				players_.at(1)->ResetAnimation();
				players_.at(1)->SetKeyDown('u', false);
			}
		}
	}
}

void myApplication::BallAnimation()
{
	BallMovingAnimation += 1;
	if (BallMovingAnimation > 17)
	{
		BallMovingAnimation = 0;
		BallAnimationCounter += 1;
	}

	if (BallAnimationCounter > 3)
		BallAnimationCounter = 0;

	if (KnifeY < 450)
		KnifeY+=5.0f;

	glEnable(GL_TEXTURE_2D);
	glPushMatrix();
	glTranslatef(KnifeX, KnifeY, 0);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glBindTexture(GL_TEXTURE_2D, UITexture[3].texID );
			glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		glPushMatrix();
			glBegin(GL_QUADS);		
				glTexCoord2f(0,1); glVertex2f(0,0);
				glTexCoord2f(0,0); glVertex2f(0,TILE_SIZE); 
				glTexCoord2f(1,0); glVertex2f(TILE_SIZE,TILE_SIZE); 
				glTexCoord2f(1,1); glVertex2f(TILE_SIZE,0); 
			glEnd();
		glPopMatrix();
		glDisable(GL_BLEND);
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);

	glEnable(GL_TEXTURE_2D);
	glPushMatrix();
	glTranslatef(370, 150, 0);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glBindTexture(GL_TEXTURE_2D, BackgroundTexture[9].texID );
			glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		glPushMatrix();
			glBegin(GL_QUADS);		
				glTexCoord2f(0.35 * BallAnimationCounter,1); glVertex2f(0,75);
				glTexCoord2f(0.35 * BallAnimationCounter,0); glVertex2f(75,75);
				glTexCoord2f(0.35 * BallAnimationCounter + 0.3,0); glVertex2f(75,0);
				glTexCoord2f(0.35 * BallAnimationCounter + 0.3,1); glVertex2f(0,0);	
			glEnd();
		glPopMatrix();
		glDisable(GL_BLEND);
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);
}

void myApplication::RenderDoor()
{
	glEnable(GL_TEXTURE_2D);
	glPushMatrix();
	if (theMap->getmapName() == "level2.csv")
		glTranslatef(1000-mapOffset_x, 174, 0);
	else
		glTranslatef(560-mapOffset_x, 197, 0);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glBindTexture(GL_TEXTURE_2D, BackgroundTexture[DoorTexture].texID );
			glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
		glPushMatrix();
			glBegin(GL_QUADS);	
			if (DoorTexture == 10)
			{
				glTexCoord2f(0,1); glVertex2f(0,0);
				glTexCoord2f(0,0); glVertex2f(0,80); 
				glTexCoord2f(1,0); glVertex2f(20,80); 
				glTexCoord2f(1,1); glVertex2f(20,0); 
			}
			else
			{
				glTexCoord2f(0,1); glVertex2f(10,0);
				glTexCoord2f(0,0); glVertex2f(10,80); 
				glTexCoord2f(1,0); glVertex2f(65,80); 
				glTexCoord2f(1,1); glVertex2f(65,0); 
			}
			glEnd();
		glPopMatrix();
		glDisable(GL_BLEND);
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);
}

void myApplication::Reset()
{
	if (!Audio->theSoundEngine->isCurrentlyPlaying("MusicNSounds/VampireKiller.mp3"))
		Audio->PlayBGM(0);

	PlayerList::iterator Players;
	for (Players = players_.begin(); Players != players_.end(); Players++)
	{
		if ((*Players)->GetID() == 1)
		{
			(*Players)->SetPos_x(100);
			(*Players)->SetHealth(5);
			(*Players)->SetInvulnerable(false);
		}
		if ((*Players)->GetID() == 2)
		{
			delete *Players;
			players_.erase(Players);
			break;
		}
	}
	
	for (std::vector<Enemies*>::iterator enemy = enemies_.begin();
		enemy != enemies_.end(); enemy++)
	{
		delete *enemy;
		enemies_.erase(enemy);
		break;
	}

	players = ONEPLAYER;
	SetPlayer = false;
	Name = "";
	PrintName = const_cast<char*> ("");
	for (int i = 0; i < 20; i ++)
		SName[i] = -1;
	spawntimer.Init(0, 1);
	theMap->setmapName("level1.csv");
	theMap->LoadMap(theMap->getmapName());
	mapOffset_x = 0;
	coin = 0;
	score = 0;
	timer->Init(5, 0);
	timer->StartTimer();
	StageClear = false;
}

void myApplication::RenderEnding()
{
	if (scroll > -2900)
		scroll -= 0.05f;
	glEnable(GL_TEXTURE_2D);
	glPushMatrix();
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glBindTexture(GL_TEXTURE_2D, BackgroundTexture[13].texID );
		glPushMatrix();
			glBegin(GL_QUADS);
				glTexCoord2f(0,0); glVertex2f(0,3500 + scroll);
				glTexCoord2f(1,0); glVertex2f(800,3500 + scroll);
				glTexCoord2f(1,1); glVertex2f(800,0 + scroll);
				glTexCoord2f(0,1); glVertex2f(0,0 + scroll);	
			glEnd();
		glPopMatrix();
		glDisable(GL_BLEND);
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);
}

void myApplication::RenderExitWindow()
{
	glPushMatrix();
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glColor4f(0, 0, 0, 0.5);
		glPushMatrix();
			glBegin(GL_QUADS);
				glVertex2f(0,0);
				glVertex2f(800,0);
				glVertex2f(800,600);
				glVertex2f(0,600);		
			glEnd();
		glPopMatrix();
		glDisable(GL_BLEND);
	glPopMatrix();

	glPushMatrix();
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glColor4f(0.7, 0.7, 0.7, 0.9);
		glPushMatrix();
			glBegin(GL_QUADS);
				glVertex2f(150,450);
				glVertex2f(650,450);
				glVertex2f(650,150);
				glVertex2f(150,150);		
			glEnd();
		glPopMatrix();
		glDisable(GL_BLEND);
	glPopMatrix();

	glColor3f(1,1,1);
	printw(250, 250, 0, "Are you sure you want to exit?");
}