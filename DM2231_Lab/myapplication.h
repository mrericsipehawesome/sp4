#include <stdio.h>
#include <conio.h>
#include <GL/glut.h>
#include "vector3D.h"
#include <iostream>
#include <time.h>
#include <ctime>
#include "Camera.h"
#include "Map.h"
#include "PlayerInfo.h"
#include "TextureImage.h"
#include <irrKlang.h>

#include "GoodiesFactory.h"
#include "Goodies.h"

// New SP4
#include "SaveLoad.h"
#include "Weapon.h"
#include "Enemy.h"
#include "mainmenu.h"
#include "Timer.h"
#include "Highscore.h"
#include <vector>

#include "Audio.h"
#include "MapEditor.h"
#include "LevelSelect.h"
#include "Button.h"

//using std::vector;

#define TILE_SIZE 25
#define MAX_TILE 17										// Structure Name

#define F5_Key 0x3f00
#define F6_Key 0x4000

#define WHIP 1
#define KNIFE 2

#define NUMBUTTON 5

using namespace irrklang;

class RakPeerInterface;

//Mouse Info
typedef struct {
	bool mLButtonUp;
	bool mRButtonUp;
	bool middleButtonUp;
	int  lastX, lastY;
} theMouseInfo;

class myApplication 
{
public:
	// this is a constructor, it is implemented in myapplication.cpp
	myApplication();
	// this is a destructor, it is implemented in myapplication.cpp
	~myApplication();

	typedef std::vector<CPlayerInfo*> PlayerList;
	PlayerList players_;

//	bool Start( int argc, char **argv );

	bool Init();

	void renderScene(void);

	void changeSize(int w, int h);

	void inputKey(int key, int x, int y);
	void KeyboardDown(unsigned char key, int x, int y);
	void KeyboardUp(unsigned char key, int x, int y);
	void gamepad (unsigned int buttonID, int x, int y, int z);
	void MouseMove (int x, int y);
	void MouseClick(int button, int state, int x, int y);

	static myApplication* getInstance();

	int score, coin;

private:
	
	static myApplication *s_pInstance;

	// Camera
	Vector3D theCameraPosition;
	Vector3D theCameraDirection;
	float angle;

	bool Update(void);
	void UpdateMapEditor(void);
	bool LoadTGA(TextureImage *texture, char *filename);			// Loads A TGA File Into Memory

	void moveMeUpDown(bool mode, float timeDiff);
	void moveMeLeftRight(bool mode, float timeDiff, string player);
	void moveMeJump(string player);

	void RenderBackground(void);
	void RenderEnding(void);
	void RenderExitWindow(void);
	void RenderAutosave(void);
	void RenderTileMap(void);

	bool turn;
	//  The number of frames
	int frameCount;
	//  Number of frames per second
	float fps;
	//  currentTime - previousTime is the time elapsed
	//  between every call of the Idle function
	int currentTime, previousTime;
	//  Pointer to a font style..
	//  Fonts supported by GLUT are: GLUT_BITMAP_8_BY_13, 
	//  GLUT_BITMAP_9_BY_15, GLUT_BITMAP_TIMES_ROMAN_10, 
	//  GLUT_BITMAP_TIMES_ROMAN_24, GLUT_BITMAP_HELVETICA_10,
	//  GLUT_BITMAP_HELVETICA_12, and GLUT_BITMAP_HELVETICA_18.
	//GLvoid *font_style;
	void *font_style;
	// Calculate the FPS
	void calculateFPS();
	// Display FPS
	void drawFPS();
	//  Draws a string at the specified coordinates.
	void printw (float x, float y, float z, char* format, ...);

	// realtime loop control
	long timelastcall;
	// realtime loop control - frequency
	float frequency;

	bool myKeys[255];
	theMouseInfo mouseInfo;

	// The camera
	Camera* theCamera;

	// Background
	TextureImage BackgroundTexture[20];
	int mapOffset_x, mapOffset_y;
	int tileOffset_x, tileOffset_y;
	int mapFineOffset_x, mapFineOffset_y;

	// Tile mapping
	CMap* theMap;
	CMap* theRear;
	CMap* theMiddle;
	CMap* theNewMap;
	CMap* theGrid;
	TextureImage TileMapTexture[34];
	int theNumOfTiles_Height;
	int theNumOfTiles_Width;
	int theNumOfTiles_RearHeight;
	int theNumOfTiles_RearWidth;
	int theNumOfTiles_MiddleHeight;
	int theNumOfTiles_MiddleWidth;

	// Render some rear walls to show parallax scrolling
	void RenderRearWalls();
	int rearWallOffset_x, rearWallOffset_y;
	int rearWalltileOffset_x, rearWalltileOffset_y;
	int rearWallfineOffset_x, rearWallfineOffset_y;

	// Render some Middle walls to show parallax scrolling
	void RenderMiddleWalls();
	int MiddleWallOffset_x, MiddleWallOffset_y;
	int MiddleWalltileOffset_x, MiddleWalltileOffset_y;
	int MiddleWallfineOffset_x, MiddleWallfineOffset_y;

	bool CheckCollision(int pos_x, int pos_y,
		bool m_bCheckUpwards, bool m_bCheckDownwards,
		bool m_bCheckLeft, bool m_bCheckRight);

	// Goodies and Goodies Factory
	CGoodiesFactory theGoodiesFactory;
	CGoodies* theArrayOfGoodies;
	CGoodies* KeyGoodies;
	CGoodies** theArrayOfHealth;
	CGoodies** theArrayOfEnemy;

	void RenderGoodies(void);
	void RenderKey();
	void Renderhealth(void);

	CPlayerInfo* theHero;
	mainmenu*Mainmenu;

	void drawDeathScreen();

	CAudio* Audio;
	CMapEditor* MapEditor;
	CLevelSelect*  LevelSelect;

	//bool follow;
	bool coindisappear;
	bool disappear;
	bool stop;
	float Origin;
	bool NextLevel;
	bool shootfire;
	bool firepos;
	bool showkey;

	// New SP3
	SaveLoad Save_Load;
	Vector3D theBulletPosition;
	Vector3D theTargetPosition;
	Vector3D DirectionVector;

	Vector3D theBulletPosition2;
	Vector3D theTargetPosition2;
	Vector3D DirectionVector2;
	bool holdkey;

	void collisioncircle();
	void CreateCircle(int radius);
	void bulletfunc();
	void bulletfunc2();
	GLuint circle;

	/*Weapon weapon;
	Weapon Pistol;
	Weapon Shotgun;
	Weapon Whip;
	Weapon Knife;*/

	void Weapons();
	void RenderGun();

	//Health
	int hp;
	int potion;
	void Drawpotion(void);

	//Enemy
	void RenderEnemies();
	int life;

	// Print Highscore
	Highscore highscore;
	void RenderHighscore();

	//Print control
	void RenderControl();

	//Key function
	int key;
	void Drawkey(void);

	//sniper bullet
	float array_x[10];
	float array_y[10];
	int index;

	//render UIS
	void drawbullets(void);
	TextureImage UITexture[10];
	void drawpotions(void);
	void drawkeys(void);
	void drawgun1(void);
	void drawgun2(void);
	void RenderUI(void);

	//Smoke for bomb explosion
	void Rendersmoke(void);
	TextureImage smokeTexture[1];
	float Scount; //Counter for smoke animation
	bool smoke;

	// Enter name for highscore
	void EnterName();

	int keys, Size;
	bool GetOut, ComeIn;

	string Name;
	char SName[20];
	char CName;
	char* PrintName;

	// Timer
	void drawTimer(void);
	bool TimeStart;

	float CopMove;
	bool move;

	int Skip;

	void EnemyPos();

	// NEW
	unsigned int timer_;
	bool SendInitialPosition();
	void UpdateEnemy(float x, float y, int id);

	typedef std::vector<Enemies*> EnemyList;  
	EnemyList enemies_;
	
	typedef std::vector<Weapon*> KnifeList; 
	KnifeList knife_;

	Weapon* Whip;

	void SpawnEnemies(int num, int type, int sec);
	void AnimationAttack();
	void AnimationAttack2();

	void BallAnimation();
	void RenderDoor();
	void Reset();

	CTimer* timer;
	CTimer spawntimer;
	int Delay, Counter;
	bool Spawn;

	int counter;
	bool keydown_q, keydown_p;
	bool keydown_e, keydown_u;
	int playerno;
	bool BossSpawn;
	bool Animation;
	int players;
	bool SetPlayer;
	bool MouseUp;
	int Distance;
	bool Load;
	float blink;
	bool Blinking;
	bool AutoSave;
	float AutoSaveTime;
	bool StageClear;
	float KnifeX, KnifeY;
	int BallMovingAnimation, BallAnimationCounter;
	bool Loaded;
	bool DoorMove;
	int DoorTexture;
	float DoorWait;
	int TILESIZE;

	bool keypressed;
	int timeSinceStart;
    int deltaTime;
	int oldTimeSinceStart;
	float scroll;
	bool startpressed;
	
	string AllTheMaps[100];

	int m_iNumberOfMaps;
	Button theButton[NUMBUTTON];
};