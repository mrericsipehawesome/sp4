#include "Goodies.h"

CGoodies::CGoodies(void)
{
}

CGoodies::~CGoodies(void)
{
}

void CGoodies::SetPos(int PosX, int PosY)
{
	this->PosX = PosX;
	this->PosY = PosY;
}

int CGoodies::GetPosX()
{
	return PosX;
}

int CGoodies::GetPosY()
{
	return PosY;
}

void CGoodies::setcoindisappear(bool disappear)
{
	coindisappear = disappear;
}

bool CGoodies::getcoindisappear()
{
	return coindisappear;
}