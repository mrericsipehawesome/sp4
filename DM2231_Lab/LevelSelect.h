/**
 * @file
 * @author Wisnton Yeu
 * @version 1.0
 */

#pragma once
#include "TextureImage.h"
#include <string>
#include <fstream>
#include <windows.h>
#include <stdio.h>
#include <strstream>
#include <iostream>
#include <GL/glut.h>
#include "Map.h"
#include "Button.h"
#include "Map.h"

using namespace std;

/**
 * The Level Select class allows player to select level to play or edit.
 */

class CLevelSelect
{
private:
	bool LoadDone;
	int arraynum;
	string LevelName[100];
	string SelectedLevel;

	int MouseX, MouseY;
	bool Clicked;
	bool Once;
	int pagenum;

	TextureImage TileTexture[32];
	TextureImage BackgroundTexture[4];
	TextureImage levelselect[100];

	void *font_style;
public:
	/**
	* Constructor
	*/
	CLevelSelect(void);

	/**
	* Destructor
	*/
	~CLevelSelect(void);

	/**
	* Function to load all levels into string array.
	*/
	void LoadLevels();

	/**
	* Function to render background, button & level from array.
	*/
	void RenderLevelSelect();

	/**
	* Function to set bool when clicked.
	* @param Clicked bool to check if clicked is true or false
	*/
	void SetClick(bool Clicked);

	/**
	* Function to get mouse click bool.
	*/
	bool GetClick();
	
	/**
	* Function to return level selected.
	*/
	string GetLevel();

	/**
	* Function to move mouse & choose level.
	*/
	void Selection();


	void Reset();

	/**
	* Function to render background.
	*/
	void RenderBackground();

	/**
	* Function to get mouse pos x & y.
	* @param MouseX Pos of x
	* @param MouseY Pos of y
	*/
	void Update (int MouseX, int MouseY);

	/**
	* Function to render map preview.
	* @param TileTexture[] Array of all tiles
	* @param mapFineOffset_x mapFineOffset_x
	* @param mapOffset_x mapOffset_x
	*/
	void RenderTileMap(TextureImage TileTexture[], int mapFineOffset_x, int mapOffset_x);

	/**
	* Function to load edited level.
	*/
	void LoadEditedLevel();

	bool GetButton();

	void printw (float x, float y, float z, char* format, ...);
	bool LoadTGA(TextureImage *texture, char *filename);

	Button theButton[2];
	CMap* theMap;
};
