/**
 * @file
 * @author Sherman Lim
 * @version 1.0
 */

/**
 * The main menu class allows main menu to be in the game
 */

#pragma once

#include <iostream>
#include <string>
#include "gl/freeglut.h"
#include "TextureImage.h"

using namespace std;



class mainmenu
{
public:
	/**
	* Constructor
	*/
	mainmenu();

	/**
	* Destructor
	*/
	~mainmenu();

	/**
	* Function to Set Orthogonal projection
	*/
	void orthogonalStart ();

	/**
	* Function to End Orthogonal projection
	*/
	void orthogonalEnd ();

	/**
	* Function to Changes Integers to String.
	*/
	string intToString (int);

	/**
	* Function to draw the menu logo
	*/
	void menulogo();

	/**
	* Function to draw the start button
	*/
	void menustart();

	/**
	* Function to draw the score button
	*/
	void menuscore();

	/**
	* Function to draw the singleplayer button
	*/
	void singleplayer();

	/**
	* Function to draw the multiplayer button
	*/
	void multiplayer();

	/**
	* Function to draw the load button
	*/
	void load();

	/**
	* Function to draw the mapeditor button
	*/
	void mapeditor();
	
	float blink;
	bool Blinking;
	int timeSinceStart;
	int deltaTime;
	int oldTimeSinceStart;
	TextureImage menu[20];
};
