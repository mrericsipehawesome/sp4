#include "SaveLoad.h"

SaveLoad::SaveLoad(void)
{
	font_style = GLUT_BITMAP_TIMES_ROMAN_24;
	LoadingLine = 0;
	LoadingNameLine = 0;
	test = 0;
	// Assign default values to array elements
	for (int i = 0; i < 10; i ++)
	{
		Name[i] = " ";
		Highscore[i] = -1;
	}
}

SaveLoad::~SaveLoad(void)
{
}

void SaveLoad::Saving(CPlayerInfo* player, CMap* theMap, int score, int mapOffset_x, CTimer* timer)
{
	ofstream myfile;
	myfile.open ("save.txt");
	
	myfile << "// Map Name" << endl;
	myfile << theMap->getmapName() << endl;
	
	myfile << "//Player Pos X" << endl;
	myfile << player->GetPos_x() << endl;
	
	myfile << "//Player Pos Y" << endl;
	myfile << player->GetPos_y() << endl;

	myfile << "//Player Health" << endl;
	myfile << player->GetHealth() << endl;

	myfile << "//Score" << endl;
	myfile << score << endl;

	myfile << "//Mapoffset_x" << endl;
	myfile << mapOffset_x << endl;

	myfile << "//Timer Min" << endl;
	myfile << timer->GetMin() << endl;

	myfile << "//Timer Sec" << endl;
	myfile << timer->GetSec() << endl;

	myfile.close();
}

void SaveLoad::Loading(CPlayerInfo* player, CMap* theMap, int* score, int* mapOffset_x, CTimer* timer)
{
	LoadingLine = 0;
	string line;
	int Min, Sec;
	ifstream myfile ("save.txt");
	if (myfile.is_open())
	{
		while ( myfile.good() )
		{
		  getline (myfile,line);
		  int value = atoi(line.c_str());

		  // Ignore whitespaces and comments
		  if (line.find("//") == NULL || line.find(" ") == NULL)
			  continue;

		  // Loading line to go through each line int the txt file
		  if (LoadingLine == 0)
		  {
			  theMap->setmapName(line);
			  theMap->LoadMap(line);
		  }
		  else if (LoadingLine == 1)
			  player->SetPos_x(value);
		  else if (LoadingLine == 2)
			  player->SetPos_y(value);
		  else if (LoadingLine == 3)
			  player->SetHealth(value);
		  else if (LoadingLine == 4)
			  *score = value;
		  else if (LoadingLine == 5)
			  *mapOffset_x = value;
		  else if (LoadingLine == 6)
				Min = value;
		  else if (LoadingLine == 7)
				Sec = value;

		  LoadingLine ++;
		}
		timer->Init(Min, Sec);
		LoadingLine = 0;
		myfile.close();
	}

	else cout << "Unable to open file"; 
}

void SaveLoad::printw (float x, float y, float z, char* format, ...)
{
	va_list args;	//  Variable argument list
	int len;		//	String length
	int i;			//  Iterator
	char * text;	//	Text

	//  Initialize a variable argument list
	va_start(args, format);

	//  Return the number of characters in the string referenced the list of arguments.
	//  _vscprintf doesn't count terminating '\0' (that's why +1)
	len = _vscprintf(format, args) + 1; 

	//  Allocate memory for a string of the specified size
	text = (char *)malloc(len * sizeof(char));

	//  Write formatted output using a pointer to the list of arguments
	vsprintf_s(text, len, format, args);

	//  End using variable argument list 
	va_end(args);

	//  Specify the raster position for pixel operations.
	glRasterPos3f (x, y, z);


	//  Draw the characters one by one
	for (i = 0; text[i] != '\0'; i++)
		glutBitmapCharacter(font_style, text[i]);

	//  Free the allocated memory for the string
	free(text);
}