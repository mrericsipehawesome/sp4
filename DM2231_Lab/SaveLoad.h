/**
 * @file
 * @author Winston Yeu
 * @version 1.0
 */

#pragma once
#include <iostream>
#include <string>
#include <fstream>
#include <strstream>
#include "PlayerInfo.h"
#include "Map.h"
#include "Weapon.h"
#include "Enemy.h"
#include "Timer.h"

using namespace std;

/**
 * The SaveLoad class allows user to save, load to the last game into a text file.
 */
class SaveLoad
{
private:
	int LoadingLine, LoadingNameLine;
	int test;
	void *font_style;
	
public:

	/**
	* Constructor
	*/
	SaveLoad(void);

	/**
	* Destructor
	*/
	~SaveLoad(void);

	int Highscore[10];
	string Name[10];

	CMap theMap;


	/**
	* Function to save game info into text file
	* @param player Save player info (E.g. position, health)
	* @param theMap Save map info
	* @param score Save score
	* @param mapOffset_x Save map offset
	* @param timer Save time
	*/
	void Saving(CPlayerInfo* player, CMap* theMap, int score, int mapOffset_x, CTimer* timer);
	
	/**
	* Function to load game info from text file
	* @param player Load player info (E.g. position, health)
	* @param theMap Load map info
	* @param score Load score
	* @param mapOffset_x Load map offset
	* @param timer Load time
	*/
	void Loading(CPlayerInfo* player, CMap* theMap, int* score, int* mapOffset_x, CTimer* timer);

	/**
	* Function to print words
	*/
	void printw (float x, float y, float z, char* format, ...);
};
