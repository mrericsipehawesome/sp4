#include "LevelSelect.h"

CLevelSelect::CLevelSelect(void)
: LoadDone(false)
, arraynum(0)
, Clicked(false)
, SelectedLevel("level1.csv")
, Once(false)
, pagenum(0)
{
	LoadTGA( &(levelselect[0]), "images/levelselect.tga");
	LoadTGA( &BackgroundTexture[0], "images/level1.tga");
	LoadTGA( &BackgroundTexture[1], "images/level2.tga");
	LoadTGA( &BackgroundTexture[2], "images/level3.tga");
	LoadTGA( &BackgroundTexture[3], "images/level4.tga");

	font_style = GLUT_BITMAP_HELVETICA_18;
	for (int i = 0; i < 20; i ++)
		LevelName[i] = "";

	theButton[0].SetButtonPos(137.5, 400);
	theButton[0].SetButtonColor(0.8f, 0.8f);
	theButton[0].SetTextPos(27, 38);
	theButton[0].SetTitle("<--");

	theButton[1].SetButtonPos(572.5, 400);
	theButton[1].SetButtonColor(0.8f, 0.8f);
	theButton[1].SetTextPos(27, 38);
	theButton[1].SetTitle("-->");

	theMap = new CMap(); 
	theMap->Init( 600, 800, 600, 1600, 25 );
	theMap->LoadMap( SelectedLevel );
	theMap->setmapName( SelectedLevel );
}

CLevelSelect::~CLevelSelect(void)
{
}

void CLevelSelect::RenderLevelSelect()
{
	glEnable(GL_TEXTURE_2D);
	// Draw Background image
	glPushMatrix();
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glBindTexture(GL_TEXTURE_2D, levelselect[0].texID );
		glPushMatrix();
			glBegin(GL_QUADS);
				glTexCoord2f(0,0); glVertex2f(0,600);
				glTexCoord2f(1,0); glVertex2f(800,600);
				glTexCoord2f(1,1); glVertex2f(800,0);
				glTexCoord2f(0,1); glVertex2f(0,0);				
			glEnd();
		glPopMatrix();
		glDisable(GL_BLEND);
	glPopMatrix();
	glDisable(GL_TEXTURE_2D);

	for (int i = (20*pagenum) ; i < arraynum; i ++)
	{
		if ( i < (10 + (20*pagenum)))
		{
			if (SelectedLevel != LevelName[i] + ".csv")
				printw(50.0 + i*70 - (700*(pagenum*2)), 520.0, 0, "%s", LevelName[i].c_str());
			else
			{
				glColor3f(0.5, 0.5, 0.5);
				printw(50.0 + i*70 - (700*(pagenum*2)), 520.0, 0, "%s", LevelName[i].c_str());
				glColor3f(1, 1, 1);
			}
		}
		else if ( i < (20 + (20*pagenum)))
		{
			if (SelectedLevel != LevelName[i] + ".csv")
				printw(50.0 + i*70 - (700) - (700*(pagenum*2)), 570.0, 0, "%s", LevelName[i].c_str());
			else
			{
				glColor3f(0.4, 0.4, 0.4);
				printw(50.0 + i*70 - (700) - (700*(pagenum*2)), 570.0, 0, "%s", LevelName[i].c_str());
				glColor3f(1, 1, 1);
			}
		}
	}

	theButton[0].drawButton();
	theButton[1].drawButton();

	RenderBackground();
}

void CLevelSelect::SetClick(bool Clicked)
{
	this->Clicked = Clicked;
}

bool CLevelSelect::GetClick()
{
	return Clicked;
}

void CLevelSelect::LoadLevels()
{
	if (!LoadDone)
	{
		string line;
		ifstream myfile ("levelselect.txt");
		if (myfile.is_open())
		{
			while ( myfile.good() )
			{
				getline (myfile,line);
				LevelName[arraynum] = line;
				arraynum++;
			}
			LoadDone = true;
			myfile.close();
		}

		else cout << "Unable to open file"; 
	}
}

void CLevelSelect::RenderBackground()
{
	glPushMatrix();
		glEnable(GL_BLEND);
		glColor4f(0.9,0.9,0.9,0.3);
		glPushMatrix();
			glBegin(GL_QUADS);
				glVertex2f(0,600);
				glVertex2f(800,600);
				glVertex2f(800,470);
				glVertex2f(0,470);		
			glEnd();
		glPopMatrix();
		glDisable(GL_BLEND);
	glPopMatrix();
	glColor3f(1,1,1);

	if (SelectedLevel != "")
	{
		glEnable(GL_TEXTURE_2D);
		// Draw Background image
		glPushMatrix();
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			if (SelectedLevel == "level1.csv")
				glBindTexture(GL_TEXTURE_2D, BackgroundTexture[0].texID );
			else if (SelectedLevel == "level2.csv")
				glBindTexture(GL_TEXTURE_2D, BackgroundTexture[1].texID );
			else if (SelectedLevel == "level3.csv")
				glBindTexture(GL_TEXTURE_2D, BackgroundTexture[2].texID );
			else if (SelectedLevel == "level4.csv")
				glBindTexture(GL_TEXTURE_2D, BackgroundTexture[3].texID );
			else
				glBindTexture(GL_TEXTURE_2D, BackgroundTexture[0].texID );
				
			glPushMatrix();
				glBegin(GL_QUADS);
						glTexCoord2f(0,0); glVertex2f(30,370);
						glTexCoord2f(1,0); glVertex2f(775,370);
						glTexCoord2f(1,1); glVertex2f(775,0);
						glTexCoord2f(0,1); glVertex2f(30,0);		
				glEnd();
			glPopMatrix();
			glDisable(GL_BLEND);
		glPopMatrix();
		glDisable(GL_TEXTURE_2D);
	}
}

void CLevelSelect::Update(int MouseX, int MouseY)
{
	this->MouseX = MouseX;
	this->MouseY = MouseY;
	theButton[0].SetMouse(MouseX, MouseY);
	theButton[1].SetMouse(MouseX, MouseY);
}

string CLevelSelect::GetLevel()
{
	return SelectedLevel;
}

void CLevelSelect::Selection()
{
	cout << pagenum << endl;
	for (int i = (20*pagenum) ; i < arraynum; i ++)
	{
		if (i < (10 + (20*pagenum)))
		{
			if (MouseX > 50.0 + i*70 - (700*(pagenum*2)) && MouseX < 50.0 + (i+1)*70 - (700*(pagenum*2)))
			{
				if (MouseY > 500 && MouseY < 520)
				{
 					if (GetClick() && !Once)
					{
						Once = true;
						SelectedLevel = LevelName[i] + ".csv";
						theMap->LoadMap( SelectedLevel );
						theMap->setmapName( SelectedLevel );
					}
				}
			}
		}
		else if (i < (20 + (20*pagenum)))
		{
			if (MouseY > 550 && MouseY < 570)
			{
				if (MouseX > 50.0 + (i-10)*70 - (700*(pagenum*2)) && MouseX < 50.0 + (i-9)*70 - (700*(pagenum*2)))
				{
					if (GetClick() && !Once)
					{
						Once = true;
						SelectedLevel = LevelName[i] + ".csv";
						theMap->LoadMap( SelectedLevel );
						theMap->setmapName( SelectedLevel );
					}
				}
			}
		}
	}

	theButton[0].SetState(Clicked);
	theButton[1].SetState(Clicked);

	if (!GetClick())
		Once = false;
}

void CLevelSelect::Reset()
{
	LoadDone = false;
	arraynum = 0;
}

bool CLevelSelect::GetButton()
{
	if (pagenum > 0)
	{
		if (theButton[0].Update() && !Once)
		{
			Once = true;
			pagenum--;
		}
	}
	else
		theButton[0].SetButtonColor(0.8f, 0.8f);

	if (arraynum - 1 > (pagenum + 1) * 20)
	{
		if (theButton[1].Update() && !Once)
		{
			Once = true;
			pagenum++;
		}
	}
	else
		theButton[1].SetButtonColor(0.8f, 0.8f);



	return false;
}

void CLevelSelect::RenderTileMap(TextureImage TileTexture[], int mapTileOffSet_x, int mapOffset_x)
{
	int mapFineOffset_x = mapOffset_x % 25;
	glPushMatrix();
	for(int i = 0; i < 24; i ++)
	{
		for(int k = 0; k < 32 + 1; k ++)
		{
			if ((mapTileOffSet_x + k) >= 64)
				break;
			glPushMatrix();
			glTranslatef((k*25*0.9) - mapFineOffset_x + 30, (i*25*0.9-80), 0);
			glEnable( GL_TEXTURE_2D );
			glEnable( GL_BLEND );
			glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glBindTexture( GL_TEXTURE_2D, TileTexture[theMap->theScreenMap[i][mapTileOffSet_x+k]].texID );
			glBegin(GL_QUADS);
				glTexCoord2f(0,1); glVertex2f(0,0);
				glTexCoord2f(0,0); glVertex2f(0,25);
				glTexCoord2f(1,0); glVertex2f(25,25);
				glTexCoord2f(1,1); glVertex2f(25,0);
			glEnd();
			glDisable( GL_BLEND );
			glDisable( GL_TEXTURE_2D );
			glPopMatrix();
		}
	}
	glPopMatrix();
}

void CLevelSelect::LoadEditedLevel()
{
	theMap->LoadMap( SelectedLevel );
	theMap->setmapName( SelectedLevel );
}

void CLevelSelect::printw (float x, float y, float z, char* format, ...)
{
	va_list args;	//  Variable argument list
	int len;		//	String length
	int i;			//  Iterator
	char * text;	//	Text

	//  Initialize a variable argument list
	va_start(args, format);

	//  Return the number of characters in the string referenced the list of arguments.
	//  _vscprintf doesn't count terminating '\0' (that's why +1)
	len = _vscprintf(format, args) + 1; 

	//  Allocate memory for a string of the specified size
	text = (char *)malloc(len * sizeof(char));

	//  Write formatted output using a pointer to the list of arguments
	vsprintf_s(text, len, format, args);

	//  End using variable argument list 
	va_end(args);

	//  Specify the raster position for pixel operations.
	glRasterPos3f (x, y, z);


	//  Draw the characters one by one
	for (i = 0; text[i] != '\0'; i++)
		glutBitmapCharacter(font_style, text[i]);

	//  Free the allocated memory for the string
	free(text);
}


bool CLevelSelect::LoadTGA(TextureImage *texture, char *filename)			// Loads A TGA File Into Memory
{    
	GLubyte		TGAheader[12]={0,0,2,0,0,0,0,0,0,0,0,0};	// Uncompressed TGA Header
	GLubyte		TGAcompare[12];								// Used To Compare TGA Header
	GLubyte		header[6];									// First 6 Useful Bytes From The Header
	GLuint		bytesPerPixel;								// Holds Number Of Bytes Per Pixel Used In The TGA File
	GLuint		imageSize;									// Used To Store The Image Size When Setting Aside Ram
	GLuint		temp;										// Temporary Variable
	GLuint		type=GL_RGBA;								// Set The Default GL Mode To RBGA (32 BPP)

	FILE *file = fopen(filename, "rb");						// Open The TGA File

	if(	file==NULL ||										// Does File Even Exist?
		fread(TGAcompare,1,sizeof(TGAcompare),file)!=sizeof(TGAcompare) ||	// Are There 12 Bytes To Read?
		memcmp(TGAheader,TGAcompare,sizeof(TGAheader))!=0				||	// Does The Header Match What We Want?
		fread(header,1,sizeof(header),file)!=sizeof(header))				// If So Read Next 6 Header Bytes
	{
		if (file == NULL)									// Did The File Even Exist? *Added Jim Strong*
			return false;									// Return False
		else
		{
			fclose(file);									// If pagenum Failed, Close The File
			return false;									// Return False
		}
	}

	texture->width  = header[1] * 256 + header[0];			// Determine The TGA Width	(highbyte*256+lowbyte)
	texture->height = header[3] * 256 + header[2];			// Determine The TGA Height	(highbyte*256+lowbyte)

	if(	texture->width	<=0	||								// Is The Width Less Than Or Equal To Zero
		texture->height	<=0	||								// Is The Height Less Than Or Equal To Zero
		(header[4]!=24 && header[4]!=32))					// Is The TGA 24 or 32 Bit?
	{
		fclose(file);										// If pagenum Failed, Close The File
		return false;										// Return False
	}

	texture->bpp	= header[4];							// Grab The TGA's Bits Per Pixel (24 or 32)
	bytesPerPixel	= texture->bpp/8;						// Divide By 8 To Get The Bytes Per Pixel
	imageSize		= texture->width*texture->height*bytesPerPixel;	// Calculate The Memory Required For The TGA Data

	texture->imageData=(GLubyte *)malloc(imageSize);		// Reserve Memory To Hold The TGA Data

	if(	texture->imageData==NULL ||							// Does The Storage Memory Exist?
		fread(texture->imageData, 1, imageSize, file)!=imageSize)	// Does The Image Size Match The Memory Reserved?
	{
		if(texture->imageData!=NULL)						// Was Image Data Loaded
			free(texture->imageData);						// If So, Release The Image Data

		fclose(file);										// Close The File
		return false;										// Return False
	}

	for(GLuint i=0; i<int(imageSize); i+=bytesPerPixel)		// Loop Through The Image Data
	{														// Swaps The 1st And 3rd Bytes ('R'ed and 'B'lue)
		temp=texture->imageData[i];							// Temporarily Store The Value At Image Data 'i'
		texture->imageData[i] = texture->imageData[i + 2];	// Set The 1st Byte To The Value Of The 3rd Byte
		texture->imageData[i + 2] = temp;					// Set The 3rd Byte To The Value In 'temp' (1st Byte Value)
	}

	fclose (file);											// Close The File

	// Build A Texture From The Data
	glGenTextures(1, &texture[0].texID);					// Generate OpenGL texture IDs

	glBindTexture(GL_TEXTURE_2D, texture[0].texID);			// Bind Our Texture
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);	// Linear Filtered
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);	// Linear Filtered

	if (texture[0].bpp==24)									// Was The TGA 24 Bits
	{
		type=GL_RGB;										// If So Set The 'type' To GL_RGB
	}

	glTexImage2D(GL_TEXTURE_2D, 0, type, texture[0].width, texture[0].height, 0, type, GL_UNSIGNED_BYTE, texture[0].imageData);

	return true;											// Texture Building Went Ok, Return True
}