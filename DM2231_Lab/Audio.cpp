#include "Audio.h"

CAudio * CAudio::s_pInstance = NULL;

CAudio::CAudio(void)
: theSoundEngine(NULL)
, Sound_BGM(NULL)
, Sound_SFX(NULL)
{
	
}

CAudio::~CAudio(void)
{
	if (theSoundEngine != NULL)
	{
		theSoundEngine->drop();
	}
}

CAudio* CAudio::getInstance()
{
	if(s_pInstance == NULL)
	{
		s_pInstance = new CAudio();
	}

	return s_pInstance;
}

void CAudio::StartAudio (void)
{
	Sound_BGM->setIsPaused(false);
}

void CAudio::PauseAudio (void)
{
	Sound_BGM->setIsPaused(true);
}

void CAudio::StopAudio (void)
{
	theSoundEngine->stopAllSounds();
}

bool CAudio::Init (void)
{
	BGM[0] = "MusicNSounds/VampireKiller.mp3";	//Normal BGM
	BGM[1] = "MusicNSounds/PoisonMind.mp3";		//Boss BGM
	BGM[2] = "MusicNSounds/StageClear.mp3";		//Stage Clear BGM
	BGM[3] = "MusicNSounds/Death.mp3";			//Death BGM
	BGM[4] = "MusicNSounds/End.mp3";			//End BGM
		
	SFX[0] = "MusicNSounds/07.wav";				//Whip Nothing
	SFX[1] = "MusicNSounds/06.wav";				//Whip Something
	SFX[2] = "MusicNSounds/12.wav";				//Pick up Heart
	SFX[3] = "MusicNSounds/25.wav";				//Pick Up Item
	SFX[4] = "MusicNSounds/17.wav";				//Pick Up Money
	SFX[5] = "MusicNSounds/33.wav";				//Water monster

	theSoundEngine = createIrrKlangDevice();
	if (!theSoundEngine)
		return false;
}

void CAudio::PlayBGM (int WhatBGMToPlay)
{
	StopAudio ();
	switch (WhatBGMToPlay)
	{
		//Normal BGM
		case 0:
		//Boss BGM
		case 1:
			Sound_BGM = theSoundEngine->play2D(BGM[WhatBGMToPlay].c_str(), true, false);
			break;

		//Stage Clear BGM
		case 2:
			Sound_BGM = theSoundEngine->play2D(BGM[WhatBGMToPlay].c_str(), false, false);
			break;

		//Death BGM
		case 3:
			Sound_BGM = theSoundEngine->play2D(BGM[WhatBGMToPlay].c_str(), false, false);
			break;

		//End BGM
		case 4:
			Sound_BGM = theSoundEngine->play2D(BGM[WhatBGMToPlay].c_str(), false, false);
			break;
	}
}

void CAudio::PlaySFX (int WhatSFXToPlay)
{
	Sound_SFX = theSoundEngine->play2D(SFX[WhatSFXToPlay].c_str(), false, false);
}