#pragma once
#include "Goodies.h"
#include "Coin.h"
#include "Health.h"

typedef int GoodiesID;
#define COIN 1
#define HEALTH 0

class CGoodiesFactory
{
public:
	CGoodiesFactory(void);
	~CGoodiesFactory(void);

	CGoodies* Create(GoodiesID);
};
