#pragma once
#include "TextureImage.h"
#include "vector3D.h"
/**
 * @file
 * @author Eddie Oh
 * @version 1.4
 */

#pragma once
#include "PlayerInfo.h"
#include <iostream>
#include "Timer.h"

using namespace std;





/**
 * The definition of the different types of enemies
 */

#define TILE_SIZE 25
#define GHOST 1
#define PANTHER 2
#define BOSSa 3
#define BOSSb 4

/**
 * The Enemy class helps to control the actions of the enemies.
 */

class Enemies
{
private:
	//Position X of the enemy
	float Enemy_x;
	//Position Y of the enemy
	float Enemy_y;
	//Position X of the fireball
	float fireballX;
	//Position Y of the fireball
	float fireballY;
	//The current state 
	int CurentState;
	Vector3D Destination, Destination2;
	int moveX;
	int moveY;
	int texturetype, type, speedCounter, direction, rest;
	int health;
	float speed;
	int hit;
	bool Dead, Hit;
	bool rage;
	bool roam, attack, Fire;
	int MobCounter;
	float jumpHeight;

	unsigned int id;
public:
	enum FINITESTATE
	{
		ATTACK,
		REST,
		FIRE,
		ROAM,
		IDLE
	};
	FINITESTATE curState;

	/**
	* Constructor
	*/
	Enemies(float locx_, float locy_, int id, int Type, int direction, FINITESTATE state);
	
	/**
	* Destructor
	*/
	~Enemies(void);

	/**
	* Texture for enemy
	*/
	TextureImage EnemyTexture[10];

	/**
	* Upate the enemy according to the player coord
	* @param id gets the player ID
	* @param player1 means checking the distance between player 1 and the enemy
	* @param player2 means checking the distance between player 2 and the enemy
	* @param mapOffset_x sets the mapOffset X
	*/
	void Update(CPlayerInfo* player, int id, long timelastcall, int mapOffset_x, int player1, int player2);
	
	/**
	*Calculate the distance between the hero and enemy
	* @param hero_x gets the X position for the character
	* @param hero_y gets the Y position for the character
	* @param mapOffset_x sets the mapOffset X
	*/
	int CalculateDistance(const float hero_x, const float hero_y, const int mapOffset_x);
	
	/**
	* Calculate the distance between the hero and fireball
	* @param hero_x gets the X position for the character
	* @param hero_y gets the Y position for the character
	* @param mapOffset_x sets the mapOffset X
	*/
	int CalculateDistance2(const float hero_x, const float hero_y, const int mapOffset_x);

	/**
	* Set pos X of enemy
	* @param Enemy_x sets enemy position x
	*/
	void SetPos_x(const float Enemy_x);
	
	/**
	* Set pos Y of enemy
	* @param Enemy_y sets enemy position y
	*/
	void SetPos_y(const float Enemy_y);
	
	/**
	* Funtion to get pos X of enemy
	*/
	float GetPos_x(void);

	/**
	* Function to get pos Y of enemy
	*/
	float GetPos_y(void);
	
	/**
	* Funtion to get pos X of fireball
	*/
	//Get pos X of fireball
	float GetFireBallPos_x(void);
	
	/**
	* Function to get pos Y of fireball
	*/
	float GetFireBallPos_y(void);

	unsigned int GetID() { return id; }

	bool render;

	CMap* theMap;
	CTimer timer;

	/**
	* Function to constrain the enemy position
	* @param leftBorder checks the left side of the screen
	* @param rightBorder checks the right side of the screen
	* @param topBorder checks the top of the screen
	* @param bottomBorder checks the bottom of the screen
	* @param mapOffset_x gets the mapOffset X
	* @param mapOffset_y gets the mapOffset Y
	*/
	void ConstrainEnemy( const int leftBorder, const int rightBorder,
								const int topBorder, const int bottomBorder,
								float timeDiff,
								int& mapOffset_x, int& mapOffset_y);


	bool LoadTGA(TextureImage *texture, char *filename);
	
	/**
	* Function to Render enemies
	* @param mapOffset_x Gets the mapOffset X of the enemies
	*/
	void RenderEnemies(int mapOffset_x);
	
	/**
	* Function for enemy's fireball
	*/
	void Renderfire();

	/**
	* Boolean for Enemy animation
	*/
	bool EnemyAnimationInvert;

	int EnemyAnimationCounter;

	/**
	* Function to set the moving animation
	* @param Animation sets the counter for animation
	*/
	void SetMovingAnimation(int Animation) { MovingAnimation = Animation; }

	/**
	* Function to get MovingAnimation
	* @param MovingAnimation Gets MovingAnimation
	*/
	int GetMovingAnimation() { return MovingAnimation; }

	/**
	* Function to Set health
	* @param health gets the number of health points
	*/
	void SetHealth(int health) { this->health = health; }
	
	/**
	* Function to get health
	* @param health gets the number of health points left
	*/
	int GetHealth() { return health; }

	/**
	* Function to set hit
	* @param hit gets the number of hit
	*/
	void SetHit(int hit) { this->hit = hit; }

	/** Function to get number of hits
	* @param hit Gets the number of hits
	*/
	int GetHit() { return hit; }

	/**
	* Function to check if hit is done
	*/
	void SetHitSomething(bool Hit) { this->Hit = Hit; }
	
	/**
	* Function to check if hit is done
	* @param Hit returns the Hit number for checking
	*/
	bool GetHitSomething() { return Hit; }

	/**
	* Function to set Death
	* @param Dead Checks whether dead is true or false
	*/
	void SetDeath(bool Dead) { this->Dead = Dead; }
	
	/** 
	* Function to check whether death is true
	* @param Dead Returns and check whether enemy is dead or alive
	*/
	bool GetDeath() { return Dead; }

	int mapOffset_x;
	int MovingAnimation;
	float TexCoordX, CounterX;
	int Tile_SizeX, Tile_SizeY;
	bool TimerStart;
};
