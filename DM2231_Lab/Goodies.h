#pragma once
#include "TextureImage.h"

class CGoodies
{
private:
	int PosX, PosY;
	bool coindisappear;
public:
	CGoodies(void);
	~CGoodies(void);

	void SetPos(int PosX, int PosY);
	int GetPosX();
	int GetPosY();
	void setcoindisappear(bool disappear);
	bool getcoindisappear();

	TextureImage GoodiesTexture[2];
};
