#include "Highscore.h"

Highscore::Highscore(void)
:	Textline(0)
,	arraypos(0)
{
	font_style = GLUT_BITMAP_TIMES_ROMAN_24;
	for (int i = 0; i < 10; i ++)
	{
		score[0].name = "";
		score[0].score = 0;
	}
}

Highscore::~Highscore(void)
{
}

void Highscore::Save(string name, int score)
{
	ofstream myfile ("Score.txt", ios::app);
	if (myfile.is_open())
	{
	    myfile << name << endl;
	    myfile << score << endl;

		myfile.close();
	}
	else cout << "Unable to open file";
}

void Highscore::Change()
{
	ofstream myfile ("Score.txt");
	if (myfile.is_open())
	{
		for (int i = 0; i < arraypos; i ++)
		{
			myfile << score[i].name << endl;
			myfile << score[i].score << endl;
		}

		myfile.close();
	}
	else cout << "Unable to open file";
}

void Highscore::Load()
{
	string line;
	arraypos = 0;
	ifstream myfile ("Score.txt");
    if (myfile.is_open())
    {
		while ( getline (myfile,line) )
		{
			if (Textline == 0)
			{
				Textline=1;
			    score[arraypos].name = line;
			}
		    else if (Textline == 1)
		    {
				Textline=0;
				score[arraypos].score = atoi(line.c_str());
				arraypos++;
		    }
		}
	  
		myfile.close();
    }
    else cout << "Unable to open file"; 
	if (arraypos == 6)
		arraypos = 5;
	Sort(arraypos);
	Change();
}

void Highscore::Sort(int arraypos)
{
	for(int iter = 1; iter < arraypos; iter++)
	{
		for(int index = 0; index < arraypos - iter; index++)
		{
			if(score[index].score < score[index+1].score)
			{
				//swap around
				Score temp = score[index];
				score[index]= score[index+1];
				score[index+1] = temp;
			}
		}
	}
}

void Highscore::Render()
{
	for (int i = 0; i < arraypos; i ++)
	{
		printw(480.0, 130.0 + i*40, 0, "%i.\t\t\t%s\t\t\t\t\t\t", i+1, score[i].name.c_str());
		printw(680.0, 130.0 + i*40, 0, "%i\t\t\t\t\t\t\t\t\t\t", score[i].score);
	}
}

void Highscore::printw (float x, float y, float z, char* format, ...)
{
	va_list args;	//  Variable argument list
	int len;		//	String length
	int i;			//  Iterator
	char * text;	//	Text

	//  Initialize a variable argument list
	va_start(args, format);

	//  Return the number of characters in the string referenced the list of arguments.
	//  _vscprintf doesn't count terminating '\0' (that's why +1)
	len = _vscprintf(format, args) + 1; 

	//  Allocate memory for a string of the specified size
	text = (char *)malloc(len * sizeof(char));

	//  Write formatted output using a pointer to the list of arguments
	vsprintf_s(text, len, format, args);

	//  End using variable argument list 
	va_end(args);

	//  Specify the raster position for pixel operations.
	glRasterPos3f (x, y, z);


	//  Draw the characters one by one
	for (i = 0; text[i] != '\0'; i++)
		glutBitmapCharacter(font_style, text[i]);

	//  Free the allocated memory for the string
	free(text);
}
